﻿namespace App
{
    using Microsoft.AspNet.Builder;
    using Microsoft.AspNet.Diagnostics;
    using Microsoft.AspNet.Diagnostics.Entity;
    using Microsoft.AspNet.Hosting;
    using Microsoft.AspNet.Mvc;
    using Microsoft.Framework.ConfigurationModel;
    using Microsoft.Framework.DependencyInjection;
    using Microsoft.Framework.Logging;
    using MSharp.Framework.AspNet.Mvc;
    using MSharp.Framework.Data;
    using System;
    using System.Diagnostics;

    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            // Setup configuration sources.
            Configuration = new Configuration()
                .AddJsonFile("config.json")
                .AddEnvironmentVariables();

            Console.WriteLine("PID: {0}", Process.GetCurrentProcess().Id);
            Console.WriteLine("EnvironmentName: {0}", env.EnvironmentName);
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(s => Configuration);

            //// Add EF services to the services container.
            //services.AddEntityFramework(Configuration)
            //    .AddSqlServer()
            //    .AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(
            //        Configuration.Get("Data:DefaultConnection:ConnectionString"))); // shouldn't be needed

            //// Add Identity services to the services container.
            //services.AddIdentity<ApplicationUser, IdentityRole>(Configuration)
            //    .AddEntityFrameworkStores<ApplicationDbContext>();

            // Add MVC services to the services container.
            services.AddMvc();

            // Uncomment the following line to add Web API servcies which makes it easier to port Web API 2 controllers.
            // You need to add Microsoft.AspNet.Mvc.WebApiCompatShim package to project.json
            services.AddWebApiConventions();

            // Register MSharp.Web dependencies
            services.AddMSharpWeb(Configuration)
                .Configure<MSharpOptions>(options =>
                    {
                        //options.ElementRolesServices.Clear();
                        options.ElementRolesServices.AddType(typeof(JsonElementRoleService));
                    });

            // Add MSharp data service + provider
            services.AddMSharpData(Configuration)
                .AddDataProvider<AppData.AdoDotNetDataProviderFactory>();

            // Experimental:
            services.AddSingleton<IStringifier, CommandInfoStringifier>();
            services.AddSingleton<IModulePathHelper, DefaultModulePathHelper>();
            services.AddScoped<StateHelperService>();
            services.AddSingleton<ControllerActionHelper>();
            services.AddTransient<ModuleInfo>();
        }

        // Configure is called after ConfigureServices is called.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerfactory)
        {
            // Configure the HTTP request pipeline.
            // Add the console logger.
            loggerfactory.AddConsole();

            // Add the following to the request pipeline only in development environment.
            if (string.Equals(env.EnvironmentName, "Development", StringComparison.OrdinalIgnoreCase))
            {
                //app.UseBrowserLink();
                app.UseErrorPage(ErrorPageOptions.ShowAll);
                app.UseDatabaseErrorPage(DatabaseErrorPageOptions.ShowAll);
            }
            else
            {
                // Add Error handling middleware which catches all application specific errors and
                // send the request to the following path or controller action.
                app.UseErrorHandler("/Default/Error");
            }

            // Add static files to the request pipeline.
            app.UseStaticFiles();

            // Add cookie-based authentication to the request pipeline.
            app.UseIdentity();

            // Add MVC to the request pipeline.
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Default", action = "Home" }
                    );

                routes.MapRoute(
                    name: "404",
                    template: "{*anything}",
                    defaults: new { controller = "Default", action = "NotFound" }
                    );
            });

            // Add test data to DB
            //App.TestData.InitialiseDatabaseWithTestData();
        }
    }
}
