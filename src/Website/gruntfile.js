// This file in the main entry point for defining grunt tasks and using grunt plugins.
// Click here to learn more. http://gruntjs.com/api/grunt.file

module.exports = function (grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        bower: {
            install: {
                options: {
                    targetDir: "wwwroot/lib",
                    layout: "byComponent",
                    cleanTargetDir: false,
                }
            }
        },
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2,
                    sourceMap: false,
                    //relativeUrls: true,
                    paths: [
                        "wwwroot/css",
                        "wwwroot/lib/bootstrap/less",
                        "wwwroot/lib/fontawesome/less"
                    ]
                },
                files: {
                    //"wwwroot/css/main.css": "wwwroot/css/main.less", // destination file and source file
                    "wwwroot/css/site.css": "wwwroot/css/site.less"
                }
            }
        },
        watch: {
            styles: {
                files: ['wwwroot/css/**/*.less'], // which files to watch
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    // This command registers the default task which will install bower packages into wwwroot/lib
    grunt.registerTask("default", ['bower:install']);
    grunt.registerTask("less-main", ['less']);
    grunt.registerTask("less-watch", ['watch']);

    // The following line loads the grunt plugins.
    // This line needs to be at the end of this this file.
    grunt.loadNpmTasks("grunt-bower-task");
};