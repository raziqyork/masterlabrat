﻿namespace App.Components.Modules.Menus
{
	using System.Collections.Generic;
	using System.Threading.Tasks;
	using Microsoft.AspNet.Mvc;
	using Models;

	//[ViewComponent(Name = "MainMenu")] /*optional*/
	public class MenuTestbed : ViewComponent
	{
		//private readonly ApplicationDbContext db;
		//public MainMenu(ApplicationDbContext context)
		//{
		//	db = context;
		//}

		public IViewComponentResult Invoke(int maxPriority)
		{
			return View(GetMenuData());
		}

		public async Task<IViewComponentResult> InvokeAsync(int total)
		{
			return View(await Task.FromResult(GetMenuData()));
		}

		private MenuViewModel GetMenuData()
		{
			var menu = new MenuViewModel { Items = new List<MenuItemViewModel>() };
			menu.Items.Add(new MenuItemViewModel { Name = "SubItem1", Text = "SubItem1", Action = "Default", Area = "PagesAndMenus", Controller = "SubItem1", });
			menu.Items.Add(new MenuItemViewModel { Name = "NoOptions", Text = "NoOptions", Action = "Default", Area = "PagesAndMenus/DynamicItems", Controller = "NoOptions", });
			menu.Items.Add(new MenuItemViewModel { Name = "NoOptions", Text = "NoOptions", Action = "Default", Area = "PagesAndMenus/DynamicItems", Controller = "NoOptions", });
			menu.Items.Add(new MenuItemViewModel { Type = MenuItemType.Divider, });
			menu.Items.Add(new MenuItemViewModel { Type = MenuItemType.HttpAction, Name = "Postback", Text = "Postback", Action = "ItemClicked", Controller = "MenuActions", });
			menu.Items.Add(new MenuItemViewModel { Type = MenuItemType.AjaxAction, Name = "DoSomething", Text = "Do Something", Action = "DoSomethingOnTheServer", });

			return menu;
			//return Task.FromResult(menu)
			//return Task.Run(() => menu);
		}

		public void DoSomethingOnTheServer()
		{
			/* This should really be in a separate controller class */
		}
	}
}