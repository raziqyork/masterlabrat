namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;

    [ModelBinder(Name = "page")]
    public class PageState
    {
        public string PageActionName { get; set; }
    }
}