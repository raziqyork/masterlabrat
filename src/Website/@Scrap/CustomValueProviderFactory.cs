﻿//namespace App
//{
//    using System;
//    using System.Globalization;
//    using System.Threading.Tasks;
//    using Microsoft.AspNet.Mvc.ModelBinding;

//    public class CustomValueProviderFactory : IValueProviderFactory
//    {
//        public IValueProvider GetValueProvider(ValueProviderFactoryContext context)
//        {
//            return new CustomValueProvider();
//        }

//        private class CustomValueProvider : IValueProvider
//        {
//            public Task<bool> ContainsPrefixAsync(string prefix)
//            {
//                var result = String.Equals(prefix, "test", StringComparison.OrdinalIgnoreCase);
//                return Task.FromResult(result);
//            }

//            public Task<ValueProviderResult> GetValueAsync(string key)
//            {
//                var value = "custom-value-provider-value";
//                var result = new ValueProviderResult(value, value, CultureInfo.CurrentCulture);
//                return Task.FromResult(result);
//            }
//        }
//    }
//}