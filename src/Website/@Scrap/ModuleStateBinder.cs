﻿namespace App
{
    using Microsoft.AspNet.Mvc.ModelBinding;
    using MSharp.Framework.AspNet.Mvc;
    using System;
    using System.Threading.Tasks;

    public class ModuleStateBinder : IModelBinder
    {
        public async Task<ModelBindingResult> BindModelAsync(ModelBindingContext bindingContext)
        {
            if (!await bindingContext.ValueProvider.ContainsPrefixAsync(bindingContext.ModelName))
                return null;

            var state = Activator.CreateInstance(bindingContext.ModelType) as ModuleState;

            var keyFor = new Func<string, string>(s => bindingContext.ModelName + "." + s);

            //var value = await bindingContext.ValueProvider.GetValueAsync(keyFor("SomeIdentity"));
            //state.SomeIdentity = ParseId(value?.AttemptedValue);

            //value = await bindingContext.ValueProvider.GetValueAsync(keyFor("SomeInteger"));
            //state.SomeInteger = TryParseInt(value?.AttemptedValue);

            //value = await bindingContext.ValueProvider.GetValueAsync(keyFor("CurrentAction"));
            //state.CurrentAction = value?.AttemptedValue;

            // todo: Commands

            return new ModelBindingResult(state, bindingContext.ModelName, true);

            //var dtoBinder = new ComplexModelDtoModelBinder();
            //var model2 = _activator.CreateInstance(_serviceProvider, bindingContext.ModelType);

            //var key = String.IsNullOrEmpty(bindingContext.ModelName)
            //    ? "productId"
            //    : bindingContext.ModelName + "." + "___Commands";

            //var value0 = await bindingContext.ValueProvider.GetValueAsync(key);
            //var value1 = await bindingContext.ValueProvider.GetValueAsync("___Commands");
            //var value2 = await bindingContext.ValueProvider.GetValueAsync("___Commands[556903be-b2c3-4543-b2ab-d0aafa4683f7].Name");
            //var value3 = await bindingContext.ValueProvider.GetValueAsync(bindingContext.ModelName);
            //var xxxxx = bindingContext.ModelState.Values;
            //var zzzzz = (bindingContext.ValueProvider as IEnumerable<IValueProvider>)?
            //    .OfType<EnhancedReadableStringCollectionValueProvider>()
            //    .FirstOrDefault();

            //bindingContext.ValueProvider.ContainsPrefixAsync()
            //bindingContext.ModelState.SetModelValue("", new ValueProviderResult());

            //return new ModelBindingResult(state, bindingContext.ModelName, true);
        }

        private static object ParseId(string value)
        {
            var intId = TryParseInt(value);
            if (intId.HasValue) return intId.Value;

            Guid guidId;
            if (Guid.TryParse(value, out guidId)) return guidId;

            return value;
        }

        private static int? TryParseInt(string value)
        {
            int intId;
            if (Int32.TryParse(value, out intId)) return intId;
            return null;
        }

        //private readonly ITypeActivator _activator;
        //private readonly IServiceProvider _serviceProvider;
        //public ModuleStateBinder(IServiceProvider serviceProvider, ITypeActivator activator)
        //{
        //    _serviceProvider = serviceProvider;
        //    _activator = activator;
        //}
    }
}