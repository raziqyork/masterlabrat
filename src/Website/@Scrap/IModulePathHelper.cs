﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;

    public interface IModulePathHelper
    {
        string GetViewPath(string module);
        string GetApiPath(string module);
        //string GetViewPath(Type moduleType);
        //string GetApiPath(Type moduleType);
    }
}