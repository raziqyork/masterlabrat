﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;

    public class DefaultModulePathHelper : IModulePathHelper
    {
        public string GetApiPath(string module)
        {
            if (module == null)
                throw new ArgumentNullException(nameof(module));

            return $"~/module/{module}/";
        }

        public string GetViewPath(string module)
        {
            if (module == null)
                throw new ArgumentNullException(nameof(module));

            return $"Views/_Modules/{module}/Default";
        }
    }
}