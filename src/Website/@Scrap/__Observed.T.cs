﻿namespace App
{
    using Microsoft.AspNet.Mvc;

    [ModelBinder(BinderType = typeof(ModuleStateBinder))]
    public class Observed<T> where T : class
    {
        public T Obj { get; set; }
    }
}