namespace MSharp.Framework.AspNet.Mvc
{
    using App;

    public class CommandCollection
    {
        public CommandInfo this[object key]
        {
            get
            {
                return null;
            }
            set
            {
                if (value != null)
                {
                    value.Id = key;
                }
            }
        }
    }
}