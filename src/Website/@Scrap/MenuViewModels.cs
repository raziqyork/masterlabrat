﻿namespace App.Models
{
    using System;
    using System.Collections.Generic;

    public class MenuViewModel
    {
        public IList<MenuItemViewModel> Items { get; set; }
    }

    public class MenuItemViewModel
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Area { get; set; }
        public object RouteParams { get; set; }
        public MenuItemType Type { get; set; }
        public IEnumerable<MenuItemViewModel> Items { get; set; }
        public bool? Selected { get; set; }
        public bool? Disabled { get; set; }
    }

    public enum MenuItemType
    {
        /// <summary> Basic navigation to controller action </summary>
        Navigation = 0,
        /// <summary> Server action invoked by form post back </summary>
        HttpAction,
        /// <summary> Server action invoked by ajax </summary>
        AjaxAction,
        /// <summary> AngularJS/Knockout action </summary>
        [Obsolete]
        ClientAction,
        /// <summary> Embedded javascript method </summary>
        JavaScript,
        /// <summary> Sub-menu parent </summary>
        ParentItem,
        /// <summary> Menu Separator </summary>
        Divider
    }
}