﻿namespace App.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class _MyFormModuleViewModel : IEntityViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Item title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Item date")]
        public DateTime Date { get; set; }

        [Display(Name = "Is currently active?")]
        public bool IsActive { get; set; }

        object IEntityViewModel.Id => Id;
    }

    public class _MyListModuleViewModel : ListsViewModel<App.TertiaryRecord, _MyListModuleViewModel.Filters>
    {
        //public class Item : MyFormModuleViewModel { }

        public class Filters
        {
            public string Query { get; set; }
        }
    }
}