﻿/// <reference path="../_references.js" />
'use strict';

define(function (require) {
    var angular = require('angular');
    require('services/AjaxService');
    require('services/WebApiService');

    var webUx = angular.module('webUx', ['sharedServices']);

    webUx.init = function () {
        angular.bootstrap(document, ['webUx']);
    };

    /**
     * Decorate form element with data-ajax-form attribute to make an Ajax Form.
     */
    webUx.directive('ajaxForm', ['AjaxService', function (ajax) {

        return function (scope, element, attr) {
            if (!element.is("form"))
                throw "ajaxForm directive: directive can only be used on a form element";

            var module = element.closest("[data-module]");

            if (module.length == 0)
                throw "ajaxForm directive: parent module-container not detected";

            ajax.initModuleForm(element, module.attr("data-module"));

            element.submit(function ($event) {
                $event.preventDefault();
                ajax.submitAjaxForm(scope, element, module);
            });
        };

    }]);

    //$httpProvider.defaults.headers.get = { 'My-Header': 'value' }.
    //webUx.run(function () {
    //    $http.defaults.headers.common.Authorization = 'Basic YmVlcDpib29w';
    //});
    //
    //webUx.constant('magicNumber', 42);
    //
    //webUx.controller('ModuleCtrl', ['$scope', function ($scope) {
    //    $scope.submit = function (form) {
    //        //debugger;
    //        //return confirm('woah mumma! are you surely sure?');
    //        return true;
    //    };
    //}]);
    //
    //webUx.controller('__ModuleCtrl',
    //    ['$scope', '$q', '$http', 'WebApiService', 'AjaxService',
    //    function ($scope, $q, $http, webApi, ajax) {
    //        //var result = webApi.lorem();
    //        //var result2 = ajax.load();
    //
    //        $scope.submit = function (form, $event) {
    //            //debugger;
    //            //return confirm('woah mumma! are you surely sure?');
    //            $event.preventDefault();
    //        };
    //    }]);

    return webUx;
});