﻿'use strict';

require.config({
    baseUrl: '/',
    paths: {
        'app': 'js/app',
        'angular': 'lib/angular/angular',
        'bootstrap': 'lib/bootstrap/js/bootstrap',
        'jquery': 'lib/jquery/jquery',
        'lodash': 'lib/lodash/lodash',
        'services': 'lib/msharp/services'
    },
    shim: {
        lodash: {
            exports: '_'
        },
        angular: {
            deps: ['lodash', 'jquery'],
            exports: 'angular'
        },
        bootstrap: {
            deps: ['jquery']
        }
    }
    //, urlArgs: 'v=1.0'
    , out: 'js/main-built.js'
});

define('sharedServices', ['angular', 'lodash', 'jquery'], function (angular, lodash, jquery) {

    return angular.module('sharedServices', [])

        .service('Config', ['$compile', function ($compile) {
            this.pathAjaxLoader = '/Ajax/Load/{module}';
            this.ajaxIndicator = $compile("<div class='ajax-indicator row'> <div class='col-md-2 col-md-offset-5 alert alert-info text-center' role='alert'><strong>Loading... <i class='fa fa-cog fa-spin'></i></strong></div> </div>");
        }])

        .factory('_', function () { return lodash; })
        .factory('$', function () { return jquery; })
    ;
});

require(['app'], function (app) {
    app.init();
});