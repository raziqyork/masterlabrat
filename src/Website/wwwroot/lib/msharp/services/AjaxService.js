﻿/// <reference path="../_references.js" />
"use strict";

define(['sharedServices', 'services/WebApiService'], function (sharedServices) {

    sharedServices.service("AjaxService",
        ["WebApiService", "$http", "$compile", "$timeout", "$", "Config", function (webApi, $http, $compile, $timeout, $, Config) {

            /**
             * Invoke before calling submitAjaxForm.
             */
            this.initModuleForm = function (form, moduleName) {
                var action = this.toAjaxAction(form.attr("action"), moduleName);
                form.data("ajaxform-action", action)

                form.find(":submit").click(function () {
                    form.data("submit-button", this.name);
                    form.data("submit-button-value", this.value);
                });
            };

            this.submitAjaxForm = function (scope, form, container) {

                var req = {
                    url: form.data("ajaxform-action"),
                    method: (form.attr("method") || "POST").toUpperCase(),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
                    },
                    data: form.serialize() +
                        "&" + encodeURI(form.data("submit-button")) +
                        "=" + encodeURI(form.data("submit-button-value")),
                    timeout: 10000
                };

                this.showIndicator(scope, container);

                $http(req)
                    .success(function (data, status, headers, config) {
                        var content = $compile(data)(scope); // AngularJS compile response html
                        container.replaceWith(content);
                    })
                    .error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                        this.hideIndicator(content);
                    });
            };

            this.hideIndicator = function (container) {
                container.toggleClass('ajax-posting', false);
            };

            this.showIndicator = function (scope, container) {
                container.toggleClass('ajax-posting', true);
                var indicator = Config.ajaxIndicator(scope).hide();
                container.prepend(indicator);
                $timeout(function () { indicator.show(); }, 250);
            };

            this.toAjaxAction = function (action, module) {
                var uri = $("<a></a>").attr("href", action)[0]
                uri.pathname = Config.pathAjaxLoader.replace(/{module}/gi, module);
                return uri.pathname + uri.search + uri.hash;
            };

        }]);

});