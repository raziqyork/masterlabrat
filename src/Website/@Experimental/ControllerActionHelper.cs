﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ControllerActionHelper
    {
        private readonly IActionInvokerFactory actionInvokerFactory;
        private readonly IReadOnlyList<ControllerActionDescriptor> controllerActionDescriptor;
        private readonly IControllerActivator controllerActivator;

        public ControllerActionHelper(
            IActionDescriptorsCollectionProvider actionDescriptorsCollectionProvider,
            IActionInvokerFactory actionInvokerFactory,
            IControllerActivator controllerActivator)
        {
            this.controllerActionDescriptor = actionDescriptorsCollectionProvider
                .ActionDescriptors.Items
                .OfType<ControllerActionDescriptor>()
                .ToList().AsReadOnly();

            this.actionInvokerFactory = actionInvokerFactory;

            this.controllerActivator = controllerActivator;
        }

        private ControllerActionDescriptor GetActionDescriptor(string controller, string action)
        {
            var actionDescriptor = controllerActionDescriptor
                .FirstOrDefault(d => d.ControllerName == controller && d.Name == action);

            if (actionDescriptor == null)
                throw new InvalidOperationException($"No ActionDescriptor found for controller {controller} and action {action}.");

            return actionDescriptor;
        }

        public object InvokeAction(ActionContext actionContext, string controller, string action, params object[] args)
        {
            var actionDescriptor = GetActionDescriptor(controller, action);

            var controllerType = actionDescriptor.ControllerTypeInfo;

            var controllerObj = controllerActivator.Create(actionContext, controllerType);

            var actionResult = actionDescriptor.MethodInfo.Invoke(controllerObj, args);

            return actionResult;
        }

        public IViewComponentResult Create(string controller, string action)
        {
            var actionDescriptor = GetActionDescriptor(controller, action);

            return new ControllerActionViewComponentResult(actionDescriptor, actionInvokerFactory);
        }

        public Task<IViewComponentResult> CreateAsync(string controller, string action)
        {
            return Task.FromResult(Create(controller, action));
        }
    }
}