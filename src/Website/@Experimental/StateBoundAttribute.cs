﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.Framework.DependencyInjection;

    public class StateBoundAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            var controller = context.Controller as Controller;
            if (controller == null) return;

            var moduleState = new ModuleState();

            if (context.HttpContext.Request.Method != "GET")
            {
                var binder = context.HttpContext.RequestServices.GetRequiredService<EasyBinder>();

                moduleState = binder.BindModel<ModuleState>("module");
            }

            controller.ViewData[nameof(ModuleState)] = moduleState;

            // because we cannot get ViewData in StateHelperService
            context.HttpContext.Items[typeof(ModuleState)] = moduleState;
        }
    }
}