﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.Razor;
    using Microsoft.Framework.DependencyInjection;
    using System.Text.RegularExpressions;

    public class ModuleInfo
    {
        private RazorPage view;

        public ModuleInfo(
            IElementIdentityGenerator idGenerator,
            StateHelperService state,
            IScopedInstance<ActionContext> currentContext)
        {
            var context = currentContext.Value;

            if (context.HttpContext.Request.Method == "GET")
            {
                this.Id = idGenerator.NextModuleId();
            }
            else
            {
                var postBackModuleId = state.Module?.ModuleId;
                if (postBackModuleId != null)
                {
                    this.Id = postBackModuleId;
                }
                else
                {
                    this.Id = idGenerator.NextSafeModuleId();
                }
            }
        }

        public string Id { get; }

        public string Name => GetViewInfo(ExtractModuleName);

        public object Init(RazorPage view)
        {
            this.view = view;
            return null; // return null to get the more compact Razor syntax of: @Module.Init(this)
        }

        private T GetViewInfo<T>(Func<RazorPage, T> accessor)
        {
            if (view == null)
                throw new InvalidOperationException("ModuleInfo not initialised. Please invoke Init(RazorPage) in your current view.");

            return accessor(view);
        }

        private static Regex ModuleNamePattern = new Regex(@"/([^/]+)/[\w-\.\$@]+.cshtml$",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

        private static string ExtractModuleName(RazorPage view)
        {
            var viewPath = view.ViewContext.View.Path;

            var match = ModuleNamePattern.Match(viewPath);
            if (match.Success)
                return match.Groups[1].Value;

            throw new InvalidOperationException($"Cannot determine module name from view path: {viewPath}.");
        }
    }
}