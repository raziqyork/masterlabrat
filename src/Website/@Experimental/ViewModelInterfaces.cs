﻿using System;

namespace App
{
    public interface IEntityViewModel
    {
        object Id { get; }
    }

    public interface IListsViewModel
    {
        Type ItemType { get; }
        Type FilterType { get; }
        int ListPageIndex { get; set; }
        string ListSortOrder { get; set; }
    }

}