﻿namespace App.Components
{
    using Microsoft.AspNet.Mvc;
    using MSharp.Framework.AspNet;
    using System.Linq;
    using System.Threading.Tasks;

    public abstract class ListModuleBase<T> : ModuleBase where T : IListsViewModel
    {
        public ListViewOptions ListViewOptions { get; private set; }

        public abstract T GetListViewModel();

        public virtual IViewComponentResult PopulateListView()
        {
            return View(GetListViewModel());
        }

        public virtual IViewComponentResult Sort(T model, string args)
        {
            var newOrder = (args ?? "").Trim().Split(' ');

            if (newOrder.Length > 0)
            {
                var direction = newOrder.ElementAtOrDefault(1) ?? "asc";

                ListViewOptions.Sort = newOrder[0] + " " + direction;
            }

            return PopulateListView();
        }

        public virtual async Task<IViewComponentResult> InvokeAsync(ListViewOptions defaultOptions = null)
        {
            this.ListViewOptions = defaultOptions;

            if (IsPostBack)
            {
                var result = await InvokePostbackAsync();
                if (result != null)
                {
                    return result;
                }
            }

            return await Task.FromResult(PopulateListView());
        }
    }
}