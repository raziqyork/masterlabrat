﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class ListsViewModel<TItem> : IEnumerable<TItem>, IListsViewModel where TItem : class
    {
        public int ListPageIndex { get; set; }
        public string ListSortOrder { get; set; }
        public List<TItem> Items { get; set; }

        Type IListsViewModel.ItemType => typeof(TItem);
        Type IListsViewModel.FilterType => null;
        IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator() => Items.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();
    }

    public class ListsViewModel<TItem, TFilter> : ListsViewModel<TItem>, IListsViewModel where TItem : class
    {
        public TFilter Filter { get; set; }
        Type IListsViewModel.FilterType => typeof(TFilter);
    }
}