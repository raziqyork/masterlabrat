﻿namespace App.Components
{
    using Microsoft.AspNet.Mvc;
    using MSharp.Framework;
    using System;
    using System.Threading.Tasks;

    public abstract class FormModuleBase<T> : ModuleBase where T : class //, IEntityViewModel
    {
        public object ItemId { get; set; }

        public virtual T GetViewModel()
        {
            var record = (T)Database.GetOrDefault(ItemId, typeof(T));
            return record;
        }

        public abstract void BindModelToEntity(T model, object entity);

        public virtual IViewComponentResult PopulateFormView()
        {
            return View(GetViewModel());
        }

        public virtual async Task<IViewComponentResult> InvokeAsync(string id)
        {
            ItemId = id;

            if (IsPostBack)
            {
                var result = await InvokePostbackAsync();
                if (result != null)
                {
                    return result;
                }
            }

            return await Task.FromResult(PopulateFormView());
        }
    }
}