namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;

    [ModelBinder(Name = "module" /*, BinderType = typeof(ModuleStateBinder)*/)]
    public class ModuleState
    {
        public string ModuleId { get; set; }

        public string ModelType { get; set; }

        public CommandInfo Command { get; set; }

        //public object SomeIdentity { get; set; }
        //public int? SomeInteger { get; set; }

        //public List<ModuleInfo> Modules { get; set; }

        //public string CurrentAction { get; set; }
        //public CommandCollection Commands { get; set; }

        ////[ModelBinder(BinderType = typeof(DictionaryModelBinder<int, string>))]
        //public Dictionary<int, string> ___Commands { get; set; }
        //public List<CommandInfo> ___Commands { get; set; }
        //public CommandInfo ___Command { get; set; }
    }

    //public class ModuleInfo
    //{
    //    public string ModuleId { get; set; }
    //    public Type ModuleModelType { get; set; }
    //}
}