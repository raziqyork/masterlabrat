﻿namespace App.Components
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.Framework.DependencyInjection;
    using MSharp.Framework.AspNet.Mvc;
    using System;
    using System.Threading.Tasks;

    public abstract class ModuleBase : ViewComponent
    {
        public bool IsPostBack => Request.Method != "GET";
        //public PageState Page => ViewBag.PageState;
        public ModuleState Module => ViewBag.ModuleState;
        public string ModuleId => Module?.ModuleId;

        protected virtual object BindModel(object model) => model;

        public virtual async Task<IViewComponentResult> InvokePostbackAsync()
        {
            // todo: check command originated from this module
            if (Module?.Command?.Name == null) return null;

            var method = GetType().GetMethod(Module.Command.Name);

            if (method == null || method.ReturnType != typeof(IViewComponentResult))
                return null;//   throw new InvalidOperationException($"Action '{Module.Command.Name}' not found in {GetType()}.");

            var parameters = method.GetParameters();
            var args = new object[parameters.Length];
            var binder = Context.RequestServices.GetRequiredService<EasyBinder>();
            for (int i = 0; i < parameters.Length && i < 2; i++)
            {
                var param = parameters[i];

                if (param.ParameterType == typeof(string))
                {
                    args[i] = Module.Command.Args;
                }
                else if (param.ParameterType.IsValueType)
                {
                    args[i] = Convert.ChangeType(Module.Command.Args, Type.GetTypeCode(param.ParameterType));
                }
                else
                {
                    args[i] = BindModel(binder.BindModel(param.ParameterType, propertyName: param.Name));
                }
            }

            return await Task.FromResult((IViewComponentResult)method.Invoke(this, args));
        }
    }
}