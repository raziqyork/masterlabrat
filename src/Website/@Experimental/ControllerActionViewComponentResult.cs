﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Routing;
    using System.Threading.Tasks;

    public class ControllerActionViewComponentResult : IViewComponentResult
    {
        private readonly ActionDescriptor actionDescriptor;
        private readonly IActionInvokerFactory actionInvokerFactory;

        public ControllerActionViewComponentResult(
            ActionDescriptor actionDescriptor,
            IActionInvokerFactory actionInvokerFactory)
        {
            this.actionDescriptor = actionDescriptor;
            this.actionInvokerFactory = actionInvokerFactory;
        }

        public async Task ExecuteAsync(ViewComponentContext context)
        {
            var actionContext = context.ViewContext;

            // merge default RouteData in actionDescriptor with RouteData from current context
            var newRouteData = new RouteData(actionContext.RouteData);
            if (actionDescriptor.RouteValueDefaults != null)
            {
                foreach (var kvp in actionDescriptor.RouteValueDefaults)
                {
                    newRouteData.Values[kvp.Key] = kvp.Value;
                }
            }

            var newActionContext = new ActionContext(actionContext.HttpContext, newRouteData, actionDescriptor, actionContext.ModelState);

            var invoker = actionInvokerFactory.CreateInvoker(newActionContext);

            ////// var writer = context.Writer;
            ////var sb = new StringBuilder();
            ////using (var writer = new StringWriter(sb))
            ////{
            ////    var viewContext = new ViewContext(newActionContext, null, actionContext.ViewData, writer);
            ////    var invoker = actionInvokerFactory.CreateInvoker(viewContext);
            ////    await invoker.InvokeAsync();
            ////}
            //////invoker.InvokeAsync().Wait();
            ////await Task.Delay(0);
            // await actionContext.HttpContext.Response.Body.FlushAsync();

            //var stream = actionContext.HttpContext.Response.Body;
            //actionContext.HttpContext.Response.Body = null;
            await invoker.InvokeAsync();
            //actionContext.HttpContext.Response.Body = stream;

            //
            // This seems to be writing out the response output stream 
            // before the page begins to render??
            //
        }

        public void Execute(ViewComponentContext context)
        {
            ExecuteAsync(context).Wait();
        }
    }
}