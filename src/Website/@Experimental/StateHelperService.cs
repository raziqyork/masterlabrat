﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.Framework.DependencyInjection;

    public class StateHelperService
    {
        private readonly ActionContext contextAccessor;

        public StateHelperService(IScopedInstance<ActionContext> contextAccessor)
        {
            this.contextAccessor = contextAccessor.Value;
        }

        //public PageState Page => contextAccessor.HttpContext.Items[typeof(PageState)] as PageState;

        public ModuleState Module => contextAccessor.HttpContext.Items[typeof(ModuleState)] as ModuleState;
    }
}