﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.Rendering;
    using Microsoft.Framework.DependencyInjection;
    using System.Threading.Tasks;

    public static class ViewComponentHelperExtensions
    {
        private static object[] MergeModuleArgs(string name, params object[] args)
        {
            var args2 = new object[args.Length + 1];
            args2[0] = name;
            args.CopyTo(args2, 1);
            return args2;
        }

        public static HtmlString Module(this IViewComponentHelper helper, string name, params object[] args)
        {
            return helper.Invoke("ModuleHost", MergeModuleArgs(name, args));
        }

        public static Task<HtmlString> ModuleAsync(this IViewComponentHelper helper, string name, params object[] args)
        {
            return helper.InvokeAsync("ModuleHost", MergeModuleArgs(name, args));
        }

        public static void ModuleRender(this IViewComponentHelper helper, string name, params object[] args)
        {
            helper.RenderInvoke("ModuleHost", MergeModuleArgs(name, args));
        }

        public static Task ModuleRenderAsync(this IViewComponentHelper helper, string name, params object[] args)
        {
            return helper.RenderInvokeAsync("ModuleHost", MergeModuleArgs(name, args));
        }

        public static HtmlString RenderModule(this IHtmlHelper html, string module, object args = null)
        {
            var viewContext = html.ViewContext;

            var factory = viewContext.HttpContext.ApplicationServices.GetRequiredService<ControllerActionHelper>();

            var result = factory.Create(module, "ModuleUi");

            var vcx = new ViewComponentContext(null, viewContext, null);
            result.Execute(vcx);

            return HtmlString.Empty;
        }
    }
}