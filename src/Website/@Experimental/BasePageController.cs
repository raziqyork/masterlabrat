﻿namespace App
{
    using Microsoft.AspNet.Mvc;
    using MSharp.Framework.AspNet.Mvc;

    [StateBound]
    public abstract partial class BasePageController : Controller
    {
        public virtual IActionResult Default()
        {
            return View();
        }

        //[HttpPost]
        //public virtual IActionResult DefaultPostback()
        //{
        //    return View("~/Views/_Modules/MyListModule/Default.cshtml");
        //}
    }
}