﻿using System.Collections.Generic;

namespace App
{
    public class AjaxModuleInfo
    {
        public string ModuleName { get; set; }

        public readonly List<object> Arguments = new List<object>();
    }
}