﻿namespace App.UX.ElementRoles
{
    using System.Collections.Generic;
    using Microsoft.AspNet.Mvc;
    using Microsoft.Framework.DependencyInjection;
    using MSharp.Framework.AspNet.Mvc;
    using Microsoft.AspNet.Http;

    [ElementRole("//module-form")]
    public class ModuleFormRole : ElementRole
    {
        private readonly ActionContext context;

        public override string Css => "hello-kitty";

        public override void Config(IDictionary<string, string> attributes)
        {
            attributes["action"] = context.HttpContext.Request.Url().PathAndQuery;
        }

        public ModuleFormRole(IScopedInstance<ActionContext> contextAccessor)
        {
            this.context = contextAccessor.Value;
        }
    }

    //[ElementRole("grid-wrapper")]
    //public class GridContainerRole : ElementRole
    //{
    //    public override string Css => "table-responsive";
    //}

    //[ElementRole("grid")]
    //public class GridRole : ElementRole
    //{
    //    public override string Css => "table table-condensed table-striped table-bordered";
    //}

    //[ElementRole("form-label-wrapper")]
    //public class FormLabelContainerRole : ElementRole.Hidden { }
}
