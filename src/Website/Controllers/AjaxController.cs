﻿namespace App.Controllers
{
    using Microsoft.AspNet.Mvc;
    using MSharp.Framework.AspNet;
    using MSharp.Framework.AspNet.Mvc;

    [StateBound]
    [Route("Ajax")]
    public class AjaxController : Controller
    {
        [Route("Load/{module}")]
        public IActionResult LoadModule(string module)
        {
            var info = new AjaxModuleInfo { ModuleName = module };

            // Todo: this needs to be passed from the posting form.
            info.Arguments.Add(new ListViewOptions { PageSize = 3, Sort = "DateField" });

            return View(info);
        }
    }
}