namespace App.Controllers.PagesAndMenus
{
    using Microsoft.AspNet.Mvc;
    using MSharp.Framework.AspNet.Mvc;

    [Area("PagesAndMenus")]
    [Route("Pages-and-Menus/SubItem1")]
    public class SubItem1Controller : App.BasePageController
    {
        //[Activate]
        public LinkHelperService Link { get; set; }

        public SubItem1Controller(LinkHelperService link)
        {
            Link = link;
        }

        [Route("")]
        public override IActionResult Default()
        {
            return RedirectPermanent(Link.To("Default", "SideItem1", "PagesAndMenus/SubItem1").BuildUrl());
            //return View();
            //return View($"~/Views/_Pages/PagesAndMenus/SubItem1/Default.cshtml");
            //return View($"~/Views/_Pages{Request.Path.Value}.cshtml");
        }
    }
}