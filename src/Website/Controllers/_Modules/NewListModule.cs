﻿namespace App.Controllers
{
    using Microsoft.AspNet.Mvc;
    using System;
    using System.Collections.Generic;

    public partial class NewListModule : AjaxListModuleBase<ListsViewModel<TertiaryRecord>>
    {
        public override ListsViewModel<TertiaryRecord> EmptyModel()
        {
            return new ListsViewModel<TertiaryRecord>
            {
                Items = new List<TertiaryRecord>(),
                ListPageIndex = 0,
                ListSortOrder = "TextField desc",
            };
        }

        public override void PopulateModel(ListsViewModel<TertiaryRecord> model)
        {
            model.Items.AddRange(new[] {
                new TertiaryRecord { TextField = "Abc123", DateField = DateTime.Now },
                new TertiaryRecord { TextField = "Zyw987", DateField = DateTime.Now },
            });
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
