﻿using Microsoft.AspNet.Mvc;

namespace App.Controllers
{
    [Route("module/[controller]")]
    public abstract partial class AjaxModuleBase<T> : Controller, IModuleBase where T : class, new()
    {
        [Route("ui")]
        public virtual IActionResult ModuleUi()
        {
            return View(GetData(null));
        }

        [Route("data"), HttpGet]
        public virtual T GetData(object properties)
        {
            var model = EmptyModel();
            PopulateModel(model);
            return model;
        }

        object IModuleBase.GetData(object properties) => GetData(properties);

        public virtual T EmptyModel()
        {
            var model = new T();
            // this is where default search filters and sort directives
            return model;
        }

        public abstract void PopulateModel(T model);
    }
}