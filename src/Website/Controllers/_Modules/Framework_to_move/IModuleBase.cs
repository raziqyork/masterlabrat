﻿namespace App.Controllers
{
    public interface IModuleBase
    {
        object GetData(object properties);
    }
}