﻿using Microsoft.AspNet.Mvc;

namespace App.Controllers
{
    public abstract partial class AjaxListModuleBase<T> : AjaxModuleBase<T> where T : class, new()
    {
        // POST module/[controller]/sort
        [HttpPost]
        [Route("sort")]
        public virtual IActionResult SortUi(string name, bool descending)
        {
            // sort

            return ModuleUi();
            //return View(GetData(null));
        }
    }
}