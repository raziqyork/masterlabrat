﻿namespace App.Controllers
{
    using Microsoft.AspNet.Mvc;

    //[Authorize]
    public class DefaultController : Controller
    {
        public IActionResult Home() => RedirectPermanent("~/Pages-and-Menus");

        public IActionResult NotFound()
        {
            // Some browsers always look for /favicon.ico
            if (Request.Path.Value == "/favicon.ico")
                return RedirectPermanent("~/Images/favicon.ico");

            //return new HttpNotFoundResult();
            //throw new HttpResponseException(HttpStatusCode.NotFound);
            throw new System.Web.HttpException(404, $"wtf brah!? '{Request.Path.Value}' is 404");
        }

        public IActionResult Error() => View("~/Views/Shared/Error.cshtml");

        ////[Route("pages-and-menus/dynamic-items/all-options")]
        ////[Route("pages-and-menus/dynamic-items/no-options")]
        ////[Route("pages-and-menus/dynamic-items/option")]
        ////[HttpGet][AllowAnonymous][ValidateAntiForgeryToken]
        //public IActionResult EmptyPage() => View($"~/Views{Request.Path.Value}.cshtml");
    }
}