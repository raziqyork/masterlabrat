﻿namespace App.Components
{
    using Microsoft.AspNet.Mvc;
    using NuGet;
    using System;

    [ViewComponent(Name = "Module")]
    public class CustomModuleComponent : ModuleBase
    {
        private string GetView(string moduleName)
        {
            if (moduleName == null)
                throw new ArgumentNullException(nameof(moduleName));

            return $"/Views/_Modules/{moduleName}/Default";
        }

        public IViewComponentResult Invoke(string moduleName)
        {
            return View(GetView(moduleName));
        }

        public IViewComponentResult Invoke(string moduleName, object properties)
        {
            var view = View(GetView(moduleName));
            view.ViewData.AddRange(properties.ToPropertyDictionary()); /* Much Wow!!! This sets properties in RazorPage.ViewBag */
            return view;
        }
    }
}