﻿namespace App.Components
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.Razor;
    using MSharp.Framework.AspNet.Mvc;
    using System;
    using System.Threading.Tasks;

    [ViewComponent(Name = "ModuleState")]
    public class ModuleStateComponent : ModuleBase
    {
        public IViewComponentResult Invoke(RazorPage moduleView, ModuleInfo moduleInfo)
        {
            var moduleState = Module ?? new ModuleState();

            if (!IsPostBack)
            {
                moduleState.ModuleId = moduleInfo.Id;
                moduleState.ModelType = DetectViewModelType(moduleView)?.ToString();
            }

            return View(moduleState);
        }

        private static Type DetectViewModelType(RazorPage view)
        {
            var type = view.GetType();

            while (type.BaseType != typeof(RazorPage))
            {
                type = type.BaseType;
            }

            if (type.BaseType == typeof(RazorPage))
            {
                return type.GenericTypeArguments[0];
            }

            return null;
        }

        public override Task<IViewComponentResult> InvokePostbackAsync() => null;
    }
}