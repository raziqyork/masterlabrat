﻿namespace App.Components.Modules.Custom
{
	using Microsoft.AspNet.Mvc;

	public class Footer : ViewComponent
	{
		public IViewComponentResult Invoke()
		{
			return View();
		}

		public IViewComponentResult Invoke(object config)
		{
			return View();
		}
	}
}