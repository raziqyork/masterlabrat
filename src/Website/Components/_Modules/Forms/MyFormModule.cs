﻿namespace App.Components
{
    using Microsoft.AspNet.Mvc;
    using MSharp.Framework;
    using System;

    public class MyFormModule : FormModuleBase<TertiaryRecord>
    {
        protected override object BindModel(object obj)
        {
            if (obj == null) return null;

            var model = (TertiaryRecord)obj;

            var command = Module?.Command;
            if (command != null)
            {
                model.ID = (Guid)Module.Command.Id;
            }

            return model;
        }

        public override void BindModelToEntity(TertiaryRecord model, object entityObj)
        {
            var entity = (TertiaryRecord)entityObj;

            entity.TextField = model.TextField;
            entity.DateField = model.DateField;
        }

        public IViewComponentResult SaveItem(TertiaryRecord model)
        {
            var item = Database.GetOrDefault<TertiaryRecord>(model.GetId())?.Clone();
            if (item != null)
            {
                BindModelToEntity(model, item);
                Database.Save(item);
            }

            return PopulateFormView();
        }
    }
}
