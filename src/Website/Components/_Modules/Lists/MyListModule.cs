﻿namespace App.Components
{
    using Microsoft.AspNet.Mvc;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    public class MyListModule : ListModuleBase<ListsViewModel<TertiaryRecord>>
    {
        public override ListsViewModel<TertiaryRecord> GetListViewModel()
        {
            var options = new List<QueryOption>();

            if (ListViewOptions?.IsComplexSort != true)
            {
                options.Add(QueryOption.Paging(ListViewOptions.Sort, ListViewOptions.PageIndex, ListViewOptions.PageSize));
            }

            var items = Database.GetList<TertiaryRecord>(options.ToArray());

            return new ListsViewModel<TertiaryRecord>
            {
                Items = items.ToList(),
                ListPageIndex = ListViewOptions?.PageIndex ?? 0,
                ListSortOrder = ListViewOptions?.Sort,
            };
        }

        public IViewComponentResult DeleteRowItem(ListsViewModel<TertiaryRecord> model)
        {
            Thread.Sleep(1000);

            var item = Database.GetOrDefault<TertiaryRecord>(Module.Command.Id);
            if (item != null)
            {
                Database.Delete(item);
            }

            return PopulateListView();
        }
    }
}
