﻿namespace App.Components
{
    using Microsoft.AspNet.Mvc;
    using MSharp.Framework.AspNet.Mvc;
    using System.Threading.Tasks;

    [ViewComponent(Name = "ModuleHost")]
    public class ModuleHostComponent : ViewComponent
    {
        [Activate]
        protected internal ControllerActionHelper ControllerAction { get; set; }

        public IViewComponentResult Invoke(string moduleName, object properties)
        {
            var data = ControllerAction.InvokeAction(ViewContext, moduleName, "GetData", properties);

            return View($"../../../_Modules/{moduleName}/ModuleUi", data);

            //var view =(ICompositeViewEngine) CompViewEngine.FindPartialView(ViewContext, "ModuleUi").EnsureSuccessful().View;
            ////var sb = new StringBuilder();
            ////using (var writer = new StringWriter(sb))
            ////{
            ////    var vc = new ViewContext(this.ViewContext, null, ViewData, writer);
            ////    var a = (ViewResult)ControllerAction.InvokeAction(vc, moduleName, "ModuleUi");
            ////    //a.ViewData = ViewData;
            ////    a.ViewEngine = ViewEngine;
            ////    a.ExecuteResultAsync(vc).Wait();
            ////}
            ////return Content($"[{moduleName}]");
            //return ControllerAction.Create(moduleName, "ModuleUi");
        }

        public IViewComponentResult Invoke(string moduleName)
        {
            return Invoke(moduleName, null);
        }

        public Task<IViewComponentResult> InvokeAsync(string moduleName, object properties)
        {
            return Task.FromResult(Invoke(moduleName, properties));
            //return ControllerAction.CreateAsync(moduleName, "ModuleUi");
        }

        public Task<IViewComponentResult> InvokeAsync(string moduleName)
        {
            return InvokeAsync(moduleName, null);
        }
    }
}