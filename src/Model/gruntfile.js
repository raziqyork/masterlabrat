/// <binding BeforeBuild='copySource' />
/*
This file in the main entry point for defining grunt tasks and using grunt plugins.
Click here to learn more. http://gruntjs.com/api/grunt.file
*/
module.exports = function (grunt) {
    grunt.file.defaultEncoding = 'utf8';

    grunt.initConfig({
        bower: {
            install: {
                options: {
                    targetDir: "wwwroot/lib",
                    layout: "byComponent",
                    cleanTargetDir: false
                }
            }
        },
        copySource: {
            options: {
                sourceDir: "C:\\Projects\\MLR.Data\\Model",
                targetDir: "Imported"
            }
        }
    });

    grunt.registerTask("default", ["bower:install"]);

    grunt.registerTask("copySource", "Copy all cs files from M# Model project.", function () {
        var options = this.options();
        grunt.log.writeln("Copying source from ", options.sourceDir);

        grunt.file.delete(options.targetDir, { force: true });
        grunt.file.mkdir(options.targetDir);

        var i = 0;
        grunt.file.recurse(options.sourceDir, function callback(abspath, rootdir, subdir, filename) {
            if (!grunt.file.isMatch("*.cs", filename) || grunt.file.isMatch("TemporaryGeneratedFile*", filename) || grunt.file.isMatch("Assembly.cs", filename)) return;
            grunt.log.writeln(subdir + "/" + filename);
            grunt.file.copy(abspath, options.targetDir + "/" + subdir + "/" + filename);
            //grunt.file.copy(abspath, options.targetDir + "/" + subdir + "/" + "f" + (i++) + ".cs");
        });
    });

    grunt.loadNpmTasks("grunt-bower-task");
};