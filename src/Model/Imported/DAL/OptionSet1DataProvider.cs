﻿namespace AppData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// Provides data-access facilities for Option set 1s.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class OptionSet1DataProvider : SqlDataProvider
    {
        #region SQL Commands
        
        /// <summary>
        /// Gets a SQL command text to query a single Option set 1 record
        /// </summary>
        static string SELECT_COMMAND { get { return "SELECT {0} FROM [OptionSet1s] AS O WHERE O.[ID] = @Id".FormatWith(COLUMNS_LIST); } }
        
        /// <summary>
        /// Gets the list of columns to use for loading Option set 1s.
        /// </summary>
        static string COLUMNS_LIST = @"O.[ID] AS [OptionSet1s_Id],
        O.[Name] AS [OptionSet1s_Name],
        O.[Order] AS [OptionSet1s_Order]";
        
        /// <summary>
        /// Gets a SQL command text to insert a record into OptionSet1s table.
        /// </summary>
        static string INSERT_COMMAND = @"INSERT INTO [OptionSet1s]
        ([ID], [Name], [Order])
        VALUES
        (@ID, @Name, @Order)";
        
        /// <summary>
        /// Gets a SQL command text to update a record in OptionSet1s table.
        /// </summary>
        static string UPDATE_COMMAND = @"UPDATE [OptionSet1s] SET
        [ID] = @ID,
        [Name] = @Name,
        [Order] = @Order
        WHERE [Id] = @OriginalId";
        
        /// <summary>
        /// Gets a SQL command text to delete a record from OptionSet1s table.
        /// </summary>
        static string DELETE_COMMAND = @"DELETE FROM [OptionSet1s] WHERE [ID] = @Id";
        
        #endregion
        
        #region Property-Column Mappings
        
        /// <summary>
        /// Gets the mapping between property names and database column names.
        /// </summary>
        internal static Dictionary<string, string> PropertyColumnMappings = new Dictionary<string, string> {
        { "ID", "O.[ID]" },
        { "Name", "O.[Name]" },
        { "Order", "O.[Order]" }};
        
        #endregion
        
        /// <summary>
        /// Gets the specified Option set 1 instance from the database.
        /// </summary>
        public override IEntity Get(Type type, object objectID)
        {
            using (var reader = ExecuteReader(SELECT_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", objectID)))
            {
                if (reader.Read())
                {
                    return Parse(reader);
                }
                else
                {
                    throw new DataException("There is no Option set 1 record with the ID of '{0}'.".FormatWith(objectID));
                }
            }
        }
        
        /// <summary>
        /// Gets the list of Option set 1s that match the specified criteria.
        /// </summary>
        public override IEnumerable<IEntity> GetList(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.OptionSet1>(criteria, options, PropertyColumnMappings);
            
            using (var reader = ExecuteReader(queryBuilder.GenerateQuery(COLUMNS_LIST, "[OptionSet1s] AS O"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters)))
            {
                var result = new List<App.OptionSet1>();
                while (reader.Read())
                {
                    result.Add(Parse(reader));
                }
                
                return result;
            }
        }
        
        /// <summary>
        /// Gets a count of the Option set 1s that match the specified criteria.
        /// </summary>
        public override int Count(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.OptionSet1>(criteria, options, PropertyColumnMappings);
            
            return (int)ExecuteScalar(queryBuilder.GenerateCountQuery("[OptionSet1s] AS O"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters));
        }
        
        /// <summary>
        /// Lazy-loads the data for the specified many-to-many relation on the specified Option set 1 instance from the database.
        /// </summary>
        public override IEnumerable<string> ReadManyToManyRelation(IEntity instance, string property)
        {
            throw new ArgumentException("The property '{0}' is not supported for the instance of '{1}'".FormatWith(property, instance.GetType().ToString()));
        }
        
        /// <summary>
        /// Extracts the Option set 1 instance from the current record of the specified data reader.
        /// </summary>
        internal static App.OptionSet1 Parse(IDataReader reader)
        {
            var result = new App.OptionSet1();
            FillData(reader, result);
            EntityManager.SetSaved(result, reader.GetGuid(0));
            return result;
        }
        
        /// <summary>
        /// Loads the data from the specified data reader on the specified Option set 1 instance.
        /// </summary>
        internal static void FillData(IDataReader reader, App.OptionSet1 entity)
        {
            var values = new object[reader.FieldCount];
            reader.GetValues(values);
            
            entity.Name = (string)values[1];
            
            entity.Order = (int)values[2];
        }
        
        /// <summary>
        /// Saves the specified Option set 1 instance in the database.
        /// </summary>
        public override void Save(IEntity record)
        {
            var item = record as App.OptionSet1;
            
            if (record.IsNew)
            {
                Insert(item);
            }
            else
            {
                Update(item);
            }
        }
        
        /// <summary>
        /// Inserts the specified new Option set 1 instance into the database.
        /// </summary>
        void Insert(App.OptionSet1 item)
        {
            ExecuteNonQuery(INSERT_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Bulk inserts a number of specified Option set 1s into the database.
        /// </summary>
        public override void BulkInsert(IEntity[] entities, int batchSize)
        {
            var commands = new List<KeyValuePair<string, IDataParameter[]>>();
            
            foreach (var item in entities.Cast<App.OptionSet1>())
            {
                commands.Add(INSERT_COMMAND, CreateParameters(item));
            }
            
            ExecuteNonQuery(CommandType.Text, commands);
        }
        
        /// <summary>
        /// Updates the specified existing Option set 1 instance in the database.
        /// </summary>
        void Update(App.OptionSet1 item)
        {
            ExecuteNonQuery(UPDATE_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Creates parameters for Inserting or Updating Option set 1 records
        /// </summary>
        IDataParameter[] CreateParameters(App.OptionSet1 item)
        {
            var result = new List<IDataParameter>();
            
            result.Add(CreateParameter("OriginalId", item.OriginalId));
            result.Add(CreateParameter("Id", item.GetId()));
            result.Add(CreateParameter("Name", item.Name));
            result.Add(CreateParameter("Order", item.Order));
            
            return result.ToArray();
        }
        
        /// <summary>
        /// Deletes the specified Option set 1 instance from the database.
        /// </summary>
        public override void Delete(IEntity record)
        {
            ExecuteNonQuery(DELETE_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", record.GetId()));
        }
    }
}