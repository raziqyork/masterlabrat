﻿namespace AppData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// Provides data-access facilities for Application events.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class ApplicationEventDataProvider : SqlDataProvider
    {
        #region SQL Commands
        
        /// <summary>
        /// Gets a SQL command text to query a single Application event record
        /// </summary>
        static string SELECT_COMMAND { get { return "SELECT {0} FROM [ApplicationEvents] AS A WHERE A.[ID] = @Id".FormatWith(COLUMNS_LIST); } }
        
        /// <summary>
        /// Gets the list of columns to use for loading Application events.
        /// </summary>
        static string COLUMNS_LIST = @"A.[ID] AS [ApplicationEvents_Id],
        A.[UserId] AS [ApplicationEvents_UserId],
        A.[Date] AS [ApplicationEvents_Date],
        A.[Event] AS [ApplicationEvents_Event],
        A.[ItemType] AS [ApplicationEvents_ItemType],
        A.[ItemKey] AS [ApplicationEvents_ItemKey],
        A.[Data] AS [ApplicationEvents_Data],
        A.[IP] AS [ApplicationEvents_IP]";
        
        /// <summary>
        /// Gets a SQL command text to insert a record into ApplicationEvents table.
        /// </summary>
        static string INSERT_COMMAND = @"INSERT INTO [ApplicationEvents]
        ([ID], [UserId], [Date], [Event], [ItemType], [ItemKey], [Data], [IP])
        VALUES
        (@ID, @UserId, @Date, @Event, @ItemType, @ItemKey, @Data, @IP)";
        
        /// <summary>
        /// Gets a SQL command text to update a record in ApplicationEvents table.
        /// </summary>
        static string UPDATE_COMMAND = @"UPDATE [ApplicationEvents] SET
        [ID] = @ID,
        [UserId] = @UserId,
        [Date] = @Date,
        [Event] = @Event,
        [ItemType] = @ItemType,
        [ItemKey] = @ItemKey,
        [Data] = @Data,
        [IP] = @IP
        WHERE [Id] = @OriginalId";
        
        /// <summary>
        /// Gets a SQL command text to delete a record from ApplicationEvents table.
        /// </summary>
        static string DELETE_COMMAND = @"DELETE FROM [ApplicationEvents] WHERE [ID] = @Id";
        
        #endregion
        
        #region Property-Column Mappings
        
        /// <summary>
        /// Gets the mapping between property names and database column names.
        /// </summary>
        internal static Dictionary<string, string> PropertyColumnMappings = new Dictionary<string, string> {
        { "ID", "A.[ID]" },
        { "UserId", "A.[UserId]" },
        { "Date", "A.[Date]" },
        { "Event", "A.[Event]" },
        { "ItemType", "A.[ItemType]" },
        { "ItemKey", "A.[ItemKey]" },
        { "Data", "A.[Data]" },
        { "IP", "A.[IP]" }};
        
        #endregion
        
        /// <summary>
        /// Gets the specified Application event instance from the database.
        /// </summary>
        public override IEntity Get(Type type, object objectID)
        {
            using (var reader = ExecuteReader(SELECT_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", objectID)))
            {
                if (reader.Read())
                {
                    return Parse(reader);
                }
                else
                {
                    throw new DataException("There is no Application event record with the ID of '{0}'.".FormatWith(objectID));
                }
            }
        }
        
        /// <summary>
        /// Gets the list of Application events that match the specified criteria.
        /// </summary>
        public override IEnumerable<IEntity> GetList(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.ApplicationEvent>(criteria, options, PropertyColumnMappings);
            
            using (var reader = ExecuteReader(queryBuilder.GenerateQuery(COLUMNS_LIST, "[ApplicationEvents] AS A"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters)))
            {
                var result = new List<App.ApplicationEvent>();
                while (reader.Read())
                {
                    result.Add(Parse(reader));
                }
                
                return result;
            }
        }
        
        /// <summary>
        /// Gets a count of the Application events that match the specified criteria.
        /// </summary>
        public override int Count(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.ApplicationEvent>(criteria, options, PropertyColumnMappings);
            
            return (int)ExecuteScalar(queryBuilder.GenerateCountQuery("[ApplicationEvents] AS A"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters));
        }
        
        /// <summary>
        /// Lazy-loads the data for the specified many-to-many relation on the specified Application event instance from the database.
        /// </summary>
        public override IEnumerable<string> ReadManyToManyRelation(IEntity instance, string property)
        {
            throw new ArgumentException("The property '{0}' is not supported for the instance of '{1}'".FormatWith(property, instance.GetType().ToString()));
        }
        
        /// <summary>
        /// Extracts the Application event instance from the current record of the specified data reader.
        /// </summary>
        internal static App.ApplicationEvent Parse(IDataReader reader)
        {
            var result = new App.ApplicationEvent();
            FillData(reader, result);
            EntityManager.SetSaved(result, reader.GetGuid(0));
            return result;
        }
        
        /// <summary>
        /// Loads the data from the specified data reader on the specified Application event instance.
        /// </summary>
        internal static void FillData(IDataReader reader, App.ApplicationEvent entity)
        {
            var values = new object[reader.FieldCount];
            reader.GetValues(values);
            
            if (values[1] != DBNull.Value) entity.UserId = (string)values[1];
            
            entity.Date = (DateTime)values[2];
            
            entity.Event = (string)values[3];
            
            entity.ItemType = (string)values[4];
            
            if (values[5] != DBNull.Value) entity.ItemKey = (string)values[5];
            
            if (values[6] != DBNull.Value) entity.Data = (string)values[6];
            
            if (values[7] != DBNull.Value) entity.IP = (string)values[7];
        }
        
        /// <summary>
        /// Saves the specified Application event instance in the database.
        /// </summary>
        public override void Save(IEntity record)
        {
            var item = record as App.ApplicationEvent;
            
            if (record.IsNew)
            {
                Insert(item);
            }
            else
            {
                Update(item);
            }
        }
        
        /// <summary>
        /// Inserts the specified new Application event instance into the database.
        /// </summary>
        void Insert(App.ApplicationEvent item)
        {
            ExecuteNonQuery(INSERT_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Bulk inserts a number of specified Application events into the database.
        /// </summary>
        public override void BulkInsert(IEntity[] entities, int batchSize)
        {
            var commands = new List<KeyValuePair<string, IDataParameter[]>>();
            
            foreach (var item in entities.Cast<App.ApplicationEvent>())
            {
                commands.Add(INSERT_COMMAND, CreateParameters(item));
            }
            
            ExecuteNonQuery(CommandType.Text, commands);
        }
        
        /// <summary>
        /// Updates the specified existing Application event instance in the database.
        /// </summary>
        void Update(App.ApplicationEvent item)
        {
            ExecuteNonQuery(UPDATE_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Creates parameters for Inserting or Updating Application event records
        /// </summary>
        IDataParameter[] CreateParameters(App.ApplicationEvent item)
        {
            var result = new List<IDataParameter>();
            
            result.Add(CreateParameter("OriginalId", item.OriginalId));
            result.Add(CreateParameter("Id", item.GetId()));
            result.Add(CreateParameter("UserId", item.UserId));
            result.Add(CreateParameter("Date", item.Date));
            result.Add(CreateParameter("Event", item.Event));
            result.Add(CreateParameter("ItemType", item.ItemType));
            result.Add(CreateParameter("ItemKey", item.ItemKey));
            result.Add(CreateParameter("Data", item.Data));
            result.Add(CreateParameter("IP", item.IP));
            
            return result.ToArray();
        }
        
        /// <summary>
        /// Deletes the specified Application event instance from the database.
        /// </summary>
        public override void Delete(IEntity record)
        {
            ExecuteNonQuery(DELETE_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", record.GetId()));
        }
    }
}