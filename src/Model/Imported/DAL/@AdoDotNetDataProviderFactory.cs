﻿namespace AppData
{
    using System;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// A factory that can instantiate Data Provider objects for MLR.Data
    /// </summary>
    public class AdoDotNetDataProviderFactory : IDataProviderFactory
    {
        string ConnectionString;
        string ConnectionStringKey;
        
        /// <summary>
        /// Initializes a new instance of AdoDotNetDataProviderFactory.
        /// </summary>
        public AdoDotNetDataProviderFactory(DataProviderFactoryInfo factoryInfo)
        {
            this.ConnectionString = factoryInfo.ConnectionString;
            this.ConnectionStringKey = factoryInfo.ConnectionStringKey;
        }
        
        /// <summary>
        /// Gets a data provider instance for the specified entity type.
        /// </summary>
        public virtual IDataProvider GetProvider(Type type)
        {
            IDataProvider result = null;
            
            if (type == typeof(App.ApplicationEvent)) result = new ApplicationEventDataProvider();
            else if (type == typeof(App.ChildRecord)) result = new ChildRecordDataProvider();
            else if (type == typeof(App.EmailQueueItem)) result = new EmailQueueItemDataProvider();
            else if (type == typeof(App.MainRecord)) result = new MainRecordDataProvider();
            else if (type == typeof(App.OptionSet1)) result = new OptionSet1DataProvider();
            else if (type == typeof(App.OptionSet2)) result = new OptionSet2DataProvider();
            else if (type == typeof(App.OptionSet3)) result = new OptionSet3DataProvider();
            else if (type == typeof(App.SecondaryRecord)) result = new SecondaryRecordDataProvider();
            else if (type == typeof(App.TertiaryRecord)) result = new TertiaryRecordDataProvider();
            else if (type == typeof(App.User)) result = new UserDataProvider();
            else if (type.IsInterface) result = new InterfaceDataProvider<SqlDataProvider>();
            
            if (result == null)
            {
                throw new NotSupportedException(type + " is not a data-supported type.");
            }
            else if (this.ConnectionString.HasValue())
            {
                result.ConnectionString = this.ConnectionString;
            }
            else if (this.ConnectionStringKey.HasValue())
            {
                result.ConnectionStringKey = this.ConnectionStringKey;
            }
            
            return result;
        }
        
        /// <summary>
        /// Determines whether this data provider factory handles interface data queries.
        /// </summary>
        public virtual bool SupportsPolymorphism()
        {
            return true;
        }
    }
}