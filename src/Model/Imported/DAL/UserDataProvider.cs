﻿namespace AppData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// Provides data-access facilities for Users.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class UserDataProvider : SqlDataProvider
    {
        #region SQL Commands
        
        /// <summary>
        /// Gets a SQL command text to query a single User record
        /// </summary>
        static string SELECT_COMMAND { get { return "SELECT {0} FROM [Users] AS U WHERE U.[ID] = @Id".FormatWith(COLUMNS_LIST); } }
        
        /// <summary>
        /// Gets the list of columns to use for loading Users.
        /// </summary>
        static string COLUMNS_LIST = @"U.[ID] AS [Users_Id],
        U.[Email] AS [Users_Email],
        U.[Password] AS [Users_Password]";
        
        /// <summary>
        /// Gets a SQL command text to insert a record into Users table.
        /// </summary>
        static string INSERT_COMMAND = @"INSERT INTO [Users]
        ([ID], [Email], [Password])
        VALUES
        (@ID, @Email, @Password)";
        
        /// <summary>
        /// Gets a SQL command text to update a record in Users table.
        /// </summary>
        static string UPDATE_COMMAND = @"UPDATE [Users] SET
        [ID] = @ID,
        [Email] = @Email,
        [Password] = @Password
        WHERE [Id] = @OriginalId";
        
        /// <summary>
        /// Gets a SQL command text to delete a record from Users table.
        /// </summary>
        static string DELETE_COMMAND = @"DELETE FROM [Users] WHERE [ID] = @Id";
        
        #endregion
        
        #region Property-Column Mappings
        
        /// <summary>
        /// Gets the mapping between property names and database column names.
        /// </summary>
        internal static Dictionary<string, string> PropertyColumnMappings = new Dictionary<string, string> {
        { "ID", "U.[ID]" },
        { "Email", "U.[Email]" },
        { "Password", "U.[Password]" }};
        
        #endregion
        
        /// <summary>
        /// Gets the specified User instance from the database.
        /// </summary>
        public override IEntity Get(Type type, object objectID)
        {
            using (var reader = ExecuteReader(SELECT_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", objectID)))
            {
                if (reader.Read())
                {
                    return Parse(reader);
                }
                else
                {
                    throw new DataException("There is no User record with the ID of '{0}'.".FormatWith(objectID));
                }
            }
        }
        
        /// <summary>
        /// Gets the list of Users that match the specified criteria.
        /// </summary>
        public override IEnumerable<IEntity> GetList(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.User>(criteria, options, PropertyColumnMappings);
            
            using (var reader = ExecuteReader(queryBuilder.GenerateQuery(COLUMNS_LIST, "[Users] AS U"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters)))
            {
                var result = new List<App.User>();
                while (reader.Read())
                {
                    result.Add(Parse(reader));
                }
                
                return result;
            }
        }
        
        /// <summary>
        /// Gets a count of the Users that match the specified criteria.
        /// </summary>
        public override int Count(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.User>(criteria, options, PropertyColumnMappings);
            
            return (int)ExecuteScalar(queryBuilder.GenerateCountQuery("[Users] AS U"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters));
        }
        
        /// <summary>
        /// Lazy-loads the data for the specified many-to-many relation on the specified User instance from the database.
        /// </summary>
        public override IEnumerable<string> ReadManyToManyRelation(IEntity instance, string property)
        {
            throw new ArgumentException("The property '{0}' is not supported for the instance of '{1}'".FormatWith(property, instance.GetType().ToString()));
        }
        
        /// <summary>
        /// Extracts the User instance from the current record of the specified data reader.
        /// </summary>
        internal static App.User Parse(IDataReader reader)
        {
            var result = new App.User();
            FillData(reader, result);
            EntityManager.SetSaved(result, reader.GetGuid(0));
            return result;
        }
        
        /// <summary>
        /// Loads the data from the specified data reader on the specified User instance.
        /// </summary>
        internal static void FillData(IDataReader reader, App.User entity)
        {
            var values = new object[reader.FieldCount];
            reader.GetValues(values);
            
            entity.Email = (string)values[1];
            
            entity.Password = (string)values[2];
        }
        
        /// <summary>
        /// Saves the specified User instance in the database.
        /// </summary>
        public override void Save(IEntity record)
        {
            var item = record as App.User;
            
            if (record.IsNew)
            {
                Insert(item);
            }
            else
            {
                Update(item);
            }
        }
        
        /// <summary>
        /// Inserts the specified new User instance into the database.
        /// </summary>
        void Insert(App.User item)
        {
            ExecuteNonQuery(INSERT_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Bulk inserts a number of specified Users into the database.
        /// </summary>
        public override void BulkInsert(IEntity[] entities, int batchSize)
        {
            var commands = new List<KeyValuePair<string, IDataParameter[]>>();
            
            foreach (var item in entities.Cast<App.User>())
            {
                commands.Add(INSERT_COMMAND, CreateParameters(item));
            }
            
            ExecuteNonQuery(CommandType.Text, commands);
        }
        
        /// <summary>
        /// Updates the specified existing User instance in the database.
        /// </summary>
        void Update(App.User item)
        {
            ExecuteNonQuery(UPDATE_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Creates parameters for Inserting or Updating User records
        /// </summary>
        IDataParameter[] CreateParameters(App.User item)
        {
            var result = new List<IDataParameter>();
            
            result.Add(CreateParameter("OriginalId", item.OriginalId));
            result.Add(CreateParameter("Id", item.GetId()));
            result.Add(CreateParameter("Email", item.Email));
            result.Add(CreateParameter("Password", item.Password));
            
            return result.ToArray();
        }
        
        /// <summary>
        /// Deletes the specified User instance from the database.
        /// </summary>
        public override void Delete(IEntity record)
        {
            ExecuteNonQuery(DELETE_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", record.GetId()));
        }
    }
}