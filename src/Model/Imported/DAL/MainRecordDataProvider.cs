﻿namespace AppData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Transactions;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// Provides data-access facilities for Main records.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class MainRecordDataProvider : SqlDataProvider
    {
        #region SQL Commands
        
        /// <summary>
        /// Gets a SQL command text to query a single Main record record
        /// </summary>
        static string SELECT_COMMAND { get { return "SELECT {0} FROM [MainRecords] AS M WHERE M.[ID] = @Id".FormatWith(COLUMNS_LIST); } }
        
        /// <summary>
        /// Gets the list of columns to use for loading Main records.
        /// </summary>
        static string COLUMNS_LIST = @"M.[ID] AS [MainRecords_Id],
        M.[TextField] AS [MainRecords_TextField],
        M.[DateCreated] AS [MainRecords_DateCreated],
        M.[NumberField] AS [MainRecords_NumberField],
        M.[PatternField] AS [MainRecords_PatternField],
        M.[DisabledField] AS [MainRecords_DisabledField],
        M.[SliderField] AS [MainRecords_SliderField],
        M.[RichTextField] AS [MainRecords_RichTextField],
        M.[TextareaField] AS [MainRecords_TextareaField],
        M.[DateTimeField] AS [MainRecords_DateTimeField],
        M.[DateField] AS [MainRecords_DateField],
        M.[TimeField] AS [MainRecords_TimeField],
        M.[CheckboxField] AS [MainRecords_CheckboxField],
        M.[DisabledCheckboxField] AS [MainRecords_DisabledCheckboxField],
        (SELECT [MainRecords_CheckboxListField].[OptionSet1Id] AS [Id] FROM [MainRecords_CheckboxListField] WHERE [MainRecords_CheckboxListField].[MainRecordId] = M.[ID] FOR XML PATH('')) AS [MainRecords_CheckboxListField],
        M.[RadiobuttonField] AS [MainRecords_RadiobuttonField],
        M.[DocumentField_FileName] AS [MainRecords_DocumentField_FileName]";
        
        /// <summary>
        /// Gets a SQL command text to insert a record into MainRecords table.
        /// </summary>
        static string INSERT_COMMAND = @"INSERT INTO [MainRecords]
        ([ID], [TextField], [DateCreated], [NumberField], [PatternField], [DisabledField], [SliderField], [RichTextField], [TextareaField], [DateTimeField], [DateField], [TimeField], [CheckboxField], [DisabledCheckboxField], [RadiobuttonField], [DocumentField_FileName])
        VALUES
        (@ID, @TextField, @DateCreated, @NumberField, @PatternField, @DisabledField, @SliderField, @RichTextField, @TextareaField, @DateTimeField, @DateField, @TimeField, @CheckboxField, @DisabledCheckboxField, @RadiobuttonField, @DocumentField_FileName)";
        
        /// <summary>
        /// Gets a SQL command text to update a record in MainRecords table.
        /// </summary>
        static string UPDATE_COMMAND = @"UPDATE [MainRecords] SET
        [ID] = @ID,
        [TextField] = @TextField,
        [DateCreated] = @DateCreated,
        [NumberField] = @NumberField,
        [PatternField] = @PatternField,
        [DisabledField] = @DisabledField,
        [SliderField] = @SliderField,
        [RichTextField] = @RichTextField,
        [TextareaField] = @TextareaField,
        [DateTimeField] = @DateTimeField,
        [DateField] = @DateField,
        [TimeField] = @TimeField,
        [CheckboxField] = @CheckboxField,
        [DisabledCheckboxField] = @DisabledCheckboxField,
        [RadiobuttonField] = @RadiobuttonField,
        [DocumentField_FileName] = @DocumentField_FileName
        WHERE [Id] = @OriginalId";
        
        /// <summary>
        /// Gets a SQL command text to delete a record from MainRecords table.
        /// </summary>
        static string DELETE_COMMAND = @"DELETE FROM [MainRecords] WHERE [ID] = @Id";
        
        #endregion
        
        #region Property-Column Mappings
        
        /// <summary>
        /// Gets the mapping between property names and database column names.
        /// </summary>
        internal static Dictionary<string, string> PropertyColumnMappings = new Dictionary<string, string> {
        { "ID", "M.[ID]" },
        { "TextField", "M.[TextField]" },
        { "DateCreated", "M.[DateCreated]" },
        { "NumberField", "M.[NumberField]" },
        { "PatternField", "M.[PatternField]" },
        { "DisabledField", "M.[DisabledField]" },
        { "SliderField", "M.[SliderField]" },
        { "RichTextField", "M.[RichTextField]" },
        { "TextareaField", "M.[TextareaField]" },
        { "DateTimeField", "M.[DateTimeField]" },
        { "DateField", "M.[DateField]" },
        { "TimeField", "M.[TimeField]" },
        { "CheckboxField", "M.[CheckboxField]" },
        { "DisabledCheckboxField", "M.[DisabledCheckboxField]" },
        { "RadiobuttonField", "M.[RadiobuttonField]" },
        { "DocumentField", "M.[DocumentField_FileName]" }};
        
        #endregion
        
        #region Subquery Mappings
        
        /// <summary>
        /// Gets the mapping between association details and required subqueries.
        /// This enables database-level execution of indirect property criteria.
        /// </summary>
        static IEnumerable<PropertySubqueryMapping> GetSubQueryMappings()
        {
            yield return new PropertySubqueryMapping("RadiobuttonField.*", "MainRecords_RadiobuttonField_", OptionSet2DataProvider.PropertyColumnMappings)
            {
                Subquery = "SELECT [MainRecords_RadiobuttonField_O].[ID] FROM [OptionSet2s] AS MainRecords_RadiobuttonField_O WHERE [MainRecords_RadiobuttonField_O].[ID] = M.[RadiobuttonField]"
            };
        }
        
        #endregion
        
        /// <summary>
        /// Gets the specified Main record instance from the database.
        /// </summary>
        public override IEntity Get(Type type, object objectID)
        {
            using (var reader = ExecuteReader(SELECT_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", objectID)))
            {
                if (reader.Read())
                {
                    return Parse(reader);
                }
                else
                {
                    throw new DataException("There is no Main record record with the ID of '{0}'.".FormatWith(objectID));
                }
            }
        }
        
        /// <summary>
        /// Gets the list of Main records that match the specified criteria.
        /// </summary>
        public override IEnumerable<IEntity> GetList(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.MainRecord>(criteria, options, PropertyColumnMappings, GetSubQueryMappings);
            
            using (var reader = ExecuteReader(queryBuilder.GenerateQuery(COLUMNS_LIST, "[MainRecords] AS M"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters)))
            {
                var result = new List<App.MainRecord>();
                while (reader.Read())
                {
                    result.Add(Parse(reader));
                }
                
                return result;
            }
        }
        
        /// <summary>
        /// Gets a count of the Main records that match the specified criteria.
        /// </summary>
        public override int Count(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.MainRecord>(criteria, options, PropertyColumnMappings, GetSubQueryMappings);
            
            return (int)ExecuteScalar(queryBuilder.GenerateCountQuery("[MainRecords] AS M"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters));
        }
        
        /// <summary>
        /// Lazy-loads the data for the specified many-to-many relation on the specified Main record instance from the database.
        /// </summary>
        public override IEnumerable<string> ReadManyToManyRelation(IEntity instance, string property)
        {
            throw new ArgumentException("The property '{0}' is not supported for the instance of '{1}'".FormatWith(property, instance.GetType().ToString()));
        }
        
        /// <summary>
        /// Extracts the Main record instance from the current record of the specified data reader.
        /// </summary>
        internal static App.MainRecord Parse(IDataReader reader)
        {
            var result = new App.MainRecord();
            FillData(reader, result);
            EntityManager.SetSaved(result, reader.GetGuid(0));
            return result;
        }
        
        /// <summary>
        /// Loads the data from the specified data reader on the specified Main record instance.
        /// </summary>
        internal static void FillData(IDataReader reader, App.MainRecord entity)
        {
            var values = new object[reader.FieldCount];
            reader.GetValues(values);
            
            entity.TextField = (string)values[1];
            
            entity.DateCreated = (DateTime)values[2];
            
            if (values[3] != DBNull.Value) entity.NumberField = (decimal)values[3];
            
            if (values[4] != DBNull.Value) entity.PatternField = (string)values[4];
            
            if (values[5] != DBNull.Value) entity.DisabledField = (string)values[5];
            
            entity.SliderField = (int)values[6];
            
            if (values[7] != DBNull.Value) entity.RichTextField = (string)values[7];
            
            entity.TextareaField = (string)values[8];
            
            if (values[9] != DBNull.Value) entity.DateTimeField = (DateTime)values[9];
            
            if (values[10] != DBNull.Value) entity.DateField = ((DateTime)values[10]).Date;
            
            if (values[11] != DBNull.Value) entity.TimeField = (DateTime)values[11];
            
            entity.CheckboxField = (bool)values[12];
            
            entity.DisabledCheckboxField = (bool)values[13];
            
            if (values[14] != DBNull.Value) entity.CheckboxListFieldIds = ExtractIds((string)values[14]).Select(id => id.To<Guid>()).ToList();
            
            entity.RadiobuttonFieldId = (Guid)values[15];
            
            if (values[16] != DBNull.Value) entity.DocumentField = new Document { FileName = (string)values[16] };
        }
        
        /// <summary>
        /// Saves the specified Main record instance in the database.
        /// </summary>
        public override void Save(IEntity record)
        {
            var item = record as App.MainRecord;
            
            using (var scope = Database.CreateTransactionScope())
            {
                if (record.IsNew)
                {
                    Insert(item);
                }
                else
                {
                    Update(item);
                }
                
                SaveManyToManyRelation(item);
                
                scope.Complete();
            }
        }
        
        /// <summary>
        /// Inserts the specified new Main record instance into the database.
        /// </summary>
        void Insert(App.MainRecord item)
        {
            ExecuteNonQuery(INSERT_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Bulk inserts a number of specified Main records into the database.
        /// </summary>
        public override void BulkInsert(IEntity[] entities, int batchSize)
        {
            var commands = new List<KeyValuePair<string, IDataParameter[]>>();
            
            foreach (var item in entities.Cast<App.MainRecord>())
            {
                commands.Add(INSERT_COMMAND, CreateParameters(item));
            }
            
            ExecuteNonQuery(CommandType.Text, commands);
            
            foreach (var item in entities.Cast<App.MainRecord>())
                SaveManyToManyRelation(item);
        }
        
        /// <summary>
        /// Updates the specified existing Main record instance in the database.
        /// </summary>
        void Update(App.MainRecord item)
        {
            ExecuteNonQuery(UPDATE_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Creates parameters for Inserting or Updating Main record records
        /// </summary>
        IDataParameter[] CreateParameters(App.MainRecord item)
        {
            var result = new List<IDataParameter>();
            
            result.Add(CreateParameter("OriginalId", item.OriginalId));
            result.Add(CreateParameter("Id", item.GetId()));
            result.Add(CreateParameter("TextField", item.TextField));
            result.Add(CreateParameter("DateCreated", item.DateCreated));
            result.Add(CreateParameter("NumberField", item.NumberField));
            result.Add(CreateParameter("PatternField", item.PatternField));
            result.Add(CreateParameter("DisabledField", item.DisabledField));
            result.Add(CreateParameter("SliderField", item.SliderField));
            result.Add(CreateParameter("RichTextField", item.RichTextField));
            result.Add(CreateParameter("TextareaField", item.TextareaField));
            result.Add(CreateParameter("DateTimeField", item.DateTimeField));
            result.Add(CreateParameter("DateField", item.DateField));
            result.Add(CreateParameter("TimeField", item.TimeField));
            result.Add(CreateParameter("CheckboxField", item.CheckboxField));
            result.Add(CreateParameter("DisabledCheckboxField", item.DisabledCheckboxField));
            result.Add(CreateParameter("RadiobuttonField", item.RadiobuttonFieldId));
            result.Add(CreateParameter("DocumentField_FileName", item.DocumentField.FileName));
            
            return result.ToArray();
        }
        
        /// <summary>
        /// Saves the many to many relationship data for the specified Main record instance.
        /// </summary>
        void SaveManyToManyRelation(App.MainRecord item)
        {
            // Checkbox list field
            {
                var commands = new List<string> { "DELETE FROM [MainRecords_CheckboxListField] WHERE [MainRecordId] = @MainRecordId" };
                var parameters = new List<IDataParameter>() { CreateParameter("MainRecordId", item.GetId()) };
                
                for (int i = 0; i < item.CheckboxListFieldIds.Count; i++)
                {
                    commands.Add("INSERT INTO [MainRecords_CheckboxListField] ([MainRecordId], [OptionSet1Id]) VALUES (@MainRecordId, @OptionSet1Id{0})".FormatWith(i));
                    parameters.Add(CreateParameter("OptionSet1Id" + i, item.CheckboxListFieldIds[i]));
                }
                
                ExecuteNonQuery(commands.ToString(";"), CommandType.Text, parameters.ToArray());
            }
        }
        
        /// <summary>
        /// Deletes the specified Main record instance from the database.
        /// </summary>
        public override void Delete(IEntity record)
        {
            ExecuteNonQuery(DELETE_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", record.GetId()));
        }
    }
}