﻿namespace AppData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// Provides data-access facilities for Secondary records.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class SecondaryRecordDataProvider : SqlDataProvider
    {
        #region SQL Commands
        
        /// <summary>
        /// Gets a SQL command text to query a single Secondary record record
        /// </summary>
        static string SELECT_COMMAND { get { return "SELECT {0} FROM [SecondaryRecords] AS S WHERE S.[ID] = @Id".FormatWith(COLUMNS_LIST); } }
        
        /// <summary>
        /// Gets the list of columns to use for loading Secondary records.
        /// </summary>
        static string COLUMNS_LIST = @"S.[ID] AS [SecondaryRecords_Id],
        S.[TertiaryRecord] AS [SecondaryRecords_TertiaryRecord],
        S.[TextField] AS [SecondaryRecords_TextField],
        S.[DropdownField] AS [SecondaryRecords_DropdownField]";
        
        /// <summary>
        /// Gets a SQL command text to insert a record into SecondaryRecords table.
        /// </summary>
        static string INSERT_COMMAND = @"INSERT INTO [SecondaryRecords]
        ([ID], [TertiaryRecord], [TextField], [DropdownField])
        VALUES
        (@ID, @TertiaryRecord, @TextField, @DropdownField)";
        
        /// <summary>
        /// Gets a SQL command text to update a record in SecondaryRecords table.
        /// </summary>
        static string UPDATE_COMMAND = @"UPDATE [SecondaryRecords] SET
        [ID] = @ID,
        [TertiaryRecord] = @TertiaryRecord,
        [TextField] = @TextField,
        [DropdownField] = @DropdownField
        WHERE [Id] = @OriginalId";
        
        /// <summary>
        /// Gets a SQL command text to delete a record from SecondaryRecords table.
        /// </summary>
        static string DELETE_COMMAND = @"DELETE FROM [SecondaryRecords] WHERE [ID] = @Id";
        
        #endregion
        
        #region Property-Column Mappings
        
        /// <summary>
        /// Gets the mapping between property names and database column names.
        /// </summary>
        internal static Dictionary<string, string> PropertyColumnMappings = new Dictionary<string, string> {
        { "ID", "S.[ID]" },
        { "TertiaryRecord", "S.[TertiaryRecord]" },
        { "TextField", "S.[TextField]" },
        { "DropdownField", "S.[DropdownField]" }};
        
        #endregion
        
        #region Subquery Mappings
        
        /// <summary>
        /// Gets the mapping between association details and required subqueries.
        /// This enables database-level execution of indirect property criteria.
        /// </summary>
        static IEnumerable<PropertySubqueryMapping> GetSubQueryMappings()
        {
            yield return new PropertySubqueryMapping("TertiaryRecord.*", "SecondaryRecords_TertiaryRecord_", TertiaryRecordDataProvider.PropertyColumnMappings)
            {
                Subquery = "SELECT [SecondaryRecords_TertiaryRecord_T].[ID] FROM [TertiaryRecords] AS SecondaryRecords_TertiaryRecord_T WHERE [SecondaryRecords_TertiaryRecord_T].[ID] = S.[TertiaryRecord]"
            };
            
            yield return new PropertySubqueryMapping("DropdownField.*", "SecondaryRecords_DropdownField_", OptionSet3DataProvider.PropertyColumnMappings)
            {
                Subquery = "SELECT [SecondaryRecords_DropdownField_O].[ID] FROM [OptionSet3s] AS SecondaryRecords_DropdownField_O WHERE [SecondaryRecords_DropdownField_O].[ID] = S.[DropdownField]"
            };
        }
        
        #endregion
        
        /// <summary>
        /// Gets the specified Secondary record instance from the database.
        /// </summary>
        public override IEntity Get(Type type, object objectID)
        {
            using (var reader = ExecuteReader(SELECT_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", objectID)))
            {
                if (reader.Read())
                {
                    return Parse(reader);
                }
                else
                {
                    throw new DataException("There is no Secondary record record with the ID of '{0}'.".FormatWith(objectID));
                }
            }
        }
        
        /// <summary>
        /// Gets the list of Secondary records that match the specified criteria.
        /// </summary>
        public override IEnumerable<IEntity> GetList(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.SecondaryRecord>(criteria, options, PropertyColumnMappings, GetSubQueryMappings);
            
            using (var reader = ExecuteReader(queryBuilder.GenerateQuery(COLUMNS_LIST, "[SecondaryRecords] AS S"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters)))
            {
                var result = new List<App.SecondaryRecord>();
                while (reader.Read())
                {
                    result.Add(Parse(reader));
                }
                
                return result;
            }
        }
        
        /// <summary>
        /// Gets a count of the Secondary records that match the specified criteria.
        /// </summary>
        public override int Count(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.SecondaryRecord>(criteria, options, PropertyColumnMappings, GetSubQueryMappings);
            
            return (int)ExecuteScalar(queryBuilder.GenerateCountQuery("[SecondaryRecords] AS S"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters));
        }
        
        /// <summary>
        /// Lazy-loads the data for the specified many-to-many relation on the specified Secondary record instance from the database.
        /// </summary>
        public override IEnumerable<string> ReadManyToManyRelation(IEntity instance, string property)
        {
            throw new ArgumentException("The property '{0}' is not supported for the instance of '{1}'".FormatWith(property, instance.GetType().ToString()));
        }
        
        /// <summary>
        /// Extracts the Secondary record instance from the current record of the specified data reader.
        /// </summary>
        internal static App.SecondaryRecord Parse(IDataReader reader)
        {
            var result = new App.SecondaryRecord();
            FillData(reader, result);
            EntityManager.SetSaved(result, reader.GetGuid(0));
            return result;
        }
        
        /// <summary>
        /// Loads the data from the specified data reader on the specified Secondary record instance.
        /// </summary>
        internal static void FillData(IDataReader reader, App.SecondaryRecord entity)
        {
            var values = new object[reader.FieldCount];
            reader.GetValues(values);
            
            entity.TertiaryRecordId = (Guid)values[1];
            
            entity.TextField = (string)values[2];
            
            if (values[3] != DBNull.Value) entity.DropdownFieldId = (Guid)values[3];
        }
        
        /// <summary>
        /// Saves the specified Secondary record instance in the database.
        /// </summary>
        public override void Save(IEntity record)
        {
            var item = record as App.SecondaryRecord;
            
            if (record.IsNew)
            {
                Insert(item);
            }
            else
            {
                Update(item);
            }
        }
        
        /// <summary>
        /// Inserts the specified new Secondary record instance into the database.
        /// </summary>
        void Insert(App.SecondaryRecord item)
        {
            ExecuteNonQuery(INSERT_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Bulk inserts a number of specified Secondary records into the database.
        /// </summary>
        public override void BulkInsert(IEntity[] entities, int batchSize)
        {
            var commands = new List<KeyValuePair<string, IDataParameter[]>>();
            
            foreach (var item in entities.Cast<App.SecondaryRecord>())
            {
                commands.Add(INSERT_COMMAND, CreateParameters(item));
            }
            
            ExecuteNonQuery(CommandType.Text, commands);
        }
        
        /// <summary>
        /// Updates the specified existing Secondary record instance in the database.
        /// </summary>
        void Update(App.SecondaryRecord item)
        {
            ExecuteNonQuery(UPDATE_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Creates parameters for Inserting or Updating Secondary record records
        /// </summary>
        IDataParameter[] CreateParameters(App.SecondaryRecord item)
        {
            var result = new List<IDataParameter>();
            
            result.Add(CreateParameter("OriginalId", item.OriginalId));
            result.Add(CreateParameter("Id", item.GetId()));
            result.Add(CreateParameter("TertiaryRecord", item.TertiaryRecordId));
            result.Add(CreateParameter("TextField", item.TextField));
            result.Add(CreateParameter("DropdownField", item.DropdownFieldId));
            
            return result.ToArray();
        }
        
        /// <summary>
        /// Deletes the specified Secondary record instance from the database.
        /// </summary>
        public override void Delete(IEntity record)
        {
            ExecuteNonQuery(DELETE_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", record.GetId()));
        }
    }
}