﻿namespace AppData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// Provides data-access facilities for Email queue items.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class EmailQueueItemDataProvider : SqlDataProvider
    {
        #region SQL Commands
        
        /// <summary>
        /// Gets a SQL command text to query a single Email queue item record
        /// </summary>
        static string SELECT_COMMAND { get { return "SELECT {0} FROM [EmailQueueItems] AS E WHERE E.[ID] = @Id".FormatWith(COLUMNS_LIST); } }
        
        /// <summary>
        /// Gets the list of columns to use for loading Email queue items.
        /// </summary>
        static string COLUMNS_LIST = @"E.[ID] AS [EmailQueueItems_Id],
        E.[.Deleted] AS [EmailQueueItems__SoftDeleted],
        E.[Body] AS [EmailQueueItems_Body],
        E.[Date] AS [EmailQueueItems_Date],
        E.[EnableSsl] AS [EmailQueueItems_EnableSsl],
        E.[Html] AS [EmailQueueItems_Html],
        E.[SenderAddress] AS [EmailQueueItems_SenderAddress],
        E.[SenderName] AS [EmailQueueItems_SenderName],
        E.[Subject] AS [EmailQueueItems_Subject],
        E.[To] AS [EmailQueueItems_To],
        E.[Attachments] AS [EmailQueueItems_Attachments],
        E.[Bcc] AS [EmailQueueItems_Bcc],
        E.[Cc] AS [EmailQueueItems_Cc],
        E.[Retries] AS [EmailQueueItems_Retries],
        E.[VCalendarView] AS [EmailQueueItems_VCalendarView],
        E.[Username] AS [EmailQueueItems_Username],
        E.[Password] AS [EmailQueueItems_Password],
        E.[SmtpHost] AS [EmailQueueItems_SmtpHost],
        E.[SmtpPort] AS [EmailQueueItems_SmtpPort],
        E.[Category] AS [EmailQueueItems_Category]";
        
        /// <summary>
        /// Gets a SQL command text to insert a record into EmailQueueItems table.
        /// </summary>
        static string INSERT_COMMAND = @"INSERT INTO [EmailQueueItems]
        ([ID], [Body], [Date], [EnableSsl], [Html], [SenderAddress], [SenderName], [Subject], [To], [Attachments], [Bcc], [Cc], [Retries], [VCalendarView], [Username], [Password], [SmtpHost], [SmtpPort], [Category], [.DELETED])
        VALUES
        (@ID, @Body, @Date, @EnableSsl, @Html, @SenderAddress, @SenderName, @Subject, @To, @Attachments, @Bcc, @Cc, @Retries, @VCalendarView, @Username, @Password, @SmtpHost, @SmtpPort, @Category, @_DELETED)";
        
        /// <summary>
        /// Gets a SQL command text to update a record in EmailQueueItems table.
        /// </summary>
        static string UPDATE_COMMAND = @"UPDATE [EmailQueueItems] SET
        [ID] = @ID,
        [Body] = @Body,
        [Date] = @Date,
        [EnableSsl] = @EnableSsl,
        [Html] = @Html,
        [SenderAddress] = @SenderAddress,
        [SenderName] = @SenderName,
        [Subject] = @Subject,
        [To] = @To,
        [Attachments] = @Attachments,
        [Bcc] = @Bcc,
        [Cc] = @Cc,
        [Retries] = @Retries,
        [VCalendarView] = @VCalendarView,
        [Username] = @Username,
        [Password] = @Password,
        [SmtpHost] = @SmtpHost,
        [SmtpPort] = @SmtpPort,
        [Category] = @Category,
        [.DELETED] = @_DELETED
        WHERE [Id] = @OriginalId";
        
        /// <summary>
        /// Gets a SQL command text to delete a record from EmailQueueItems table.
        /// </summary>
        static string DELETE_COMMAND = @"DELETE FROM [EmailQueueItems] WHERE [ID] = @Id";
        
        #endregion
        
        #region Property-Column Mappings
        
        /// <summary>
        /// Gets the mapping between property names and database column names.
        /// </summary>
        internal static Dictionary<string, string> PropertyColumnMappings = new Dictionary<string, string> {
        { "ID", "E.[ID]" },
        { "Body", "E.[Body]" },
        { "Date", "E.[Date]" },
        { "EnableSsl", "E.[EnableSsl]" },
        { "Html", "E.[Html]" },
        { "SenderAddress", "E.[SenderAddress]" },
        { "SenderName", "E.[SenderName]" },
        { "Subject", "E.[Subject]" },
        { "To", "E.[To]" },
        { "Attachments", "E.[Attachments]" },
        { "Bcc", "E.[Bcc]" },
        { "Cc", "E.[Cc]" },
        { "Retries", "E.[Retries]" },
        { "VCalendarView", "E.[VCalendarView]" },
        { "Username", "E.[Username]" },
        { "Password", "E.[Password]" },
        { "SmtpHost", "E.[SmtpHost]" },
        { "SmtpPort", "E.[SmtpPort]" },
        { "Category", "E.[Category]" },
        { "IsMarkedSoftDeleted", "E.[.DELETED]" }};
        
        #endregion
        
        /// <summary>
        /// Gets the specified Email queue item instance from the database.
        /// </summary>
        public override IEntity Get(Type type, object objectID)
        {
            var query = SELECT_COMMAND;
            
            if (!SoftDeleteAttribute.Context.ShouldByPassSoftDelete())
            {
                query += " AND E.[.DELETED] = 0";
            }
            
            using (var reader = ExecuteReader(query, System.Data.CommandType.Text, CreateParameter("Id", objectID)))
            {
                if (reader.Read())
                {
                    return Parse(reader);
                }
                else
                {
                    throw new DataException("There is no Email queue item record with the ID of '{0}'.".FormatWith(objectID));
                }
            }
        }
        
        /// <summary>
        /// Gets the list of Email queue items that match the specified criteria.
        /// </summary>
        public override IEnumerable<IEntity> GetList(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.EmailQueueItem>(criteria, options, PropertyColumnMappings);
            
            using (var reader = ExecuteReader(queryBuilder.GenerateQuery(COLUMNS_LIST, "[EmailQueueItems] AS E"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters)))
            {
                var result = new List<App.EmailQueueItem>();
                while (reader.Read())
                {
                    result.Add(Parse(reader));
                }
                
                return result;
            }
        }
        
        /// <summary>
        /// Gets a count of the Email queue items that match the specified criteria.
        /// </summary>
        public override int Count(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.EmailQueueItem>(criteria, options, PropertyColumnMappings);
            
            return (int)ExecuteScalar(queryBuilder.GenerateCountQuery("[EmailQueueItems] AS E"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters));
        }
        
        /// <summary>
        /// Lazy-loads the data for the specified many-to-many relation on the specified Email queue item instance from the database.
        /// </summary>
        public override IEnumerable<string> ReadManyToManyRelation(IEntity instance, string property)
        {
            throw new ArgumentException("The property '{0}' is not supported for the instance of '{1}'".FormatWith(property, instance.GetType().ToString()));
        }
        
        /// <summary>
        /// Extracts the Email queue item instance from the current record of the specified data reader.
        /// </summary>
        internal static App.EmailQueueItem Parse(IDataReader reader)
        {
            var result = new App.EmailQueueItem();
            FillData(reader, result);
            EntityManager.SetSaved(result, reader.GetGuid(0));
            return result;
        }
        
        /// <summary>
        /// Loads the data from the specified data reader on the specified Email queue item instance.
        /// </summary>
        internal static void FillData(IDataReader reader, App.EmailQueueItem entity)
        {
            var values = new object[reader.FieldCount];
            reader.GetValues(values);
            
            if ((bool)values[1]) // Soft Deleted
            {
                EntityManager.MarkSoftDeleted(entity);
            }
            
            if (values[2] != DBNull.Value) entity.Body = (string)values[2];
            
            entity.Date = (DateTime)values[3];
            
            entity.EnableSsl = (bool)values[4];
            
            entity.Html = (bool)values[5];
            
            if (values[6] != DBNull.Value) entity.SenderAddress = (string)values[6];
            
            if (values[7] != DBNull.Value) entity.SenderName = (string)values[7];
            
            entity.Subject = (string)values[8];
            
            if (values[9] != DBNull.Value) entity.To = (string)values[9];
            
            if (values[10] != DBNull.Value) entity.Attachments = (string)values[10];
            
            if (values[11] != DBNull.Value) entity.Bcc = (string)values[11];
            
            if (values[12] != DBNull.Value) entity.Cc = (string)values[12];
            
            entity.Retries = (int)values[13];
            
            if (values[14] != DBNull.Value) entity.VCalendarView = (string)values[14];
            
            if (values[15] != DBNull.Value) entity.Username = (string)values[15];
            
            if (values[16] != DBNull.Value) entity.Password = (string)values[16];
            
            if (values[17] != DBNull.Value) entity.SmtpHost = (string)values[17];
            
            if (values[18] != DBNull.Value) entity.SmtpPort = (int)values[18];
            
            if (values[19] != DBNull.Value) entity.Category = (string)values[19];
        }
        
        /// <summary>
        /// Saves the specified Email queue item instance in the database.
        /// </summary>
        public override void Save(IEntity record)
        {
            var item = record as App.EmailQueueItem;
            
            if (record.IsNew)
            {
                Insert(item);
            }
            else
            {
                Update(item);
            }
        }
        
        /// <summary>
        /// Inserts the specified new Email queue item instance into the database.
        /// </summary>
        void Insert(App.EmailQueueItem item)
        {
            ExecuteNonQuery(INSERT_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Bulk inserts a number of specified Email queue items into the database.
        /// </summary>
        public override void BulkInsert(IEntity[] entities, int batchSize)
        {
            var commands = new List<KeyValuePair<string, IDataParameter[]>>();
            
            foreach (var item in entities.Cast<App.EmailQueueItem>())
            {
                commands.Add(INSERT_COMMAND, CreateParameters(item));
            }
            
            ExecuteNonQuery(CommandType.Text, commands);
        }
        
        /// <summary>
        /// Updates the specified existing Email queue item instance in the database.
        /// </summary>
        void Update(App.EmailQueueItem item)
        {
            ExecuteNonQuery(UPDATE_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Creates parameters for Inserting or Updating Email queue item records
        /// </summary>
        IDataParameter[] CreateParameters(App.EmailQueueItem item)
        {
            var result = new List<IDataParameter>();
            
            result.Add(CreateParameter("OriginalId", item.OriginalId));
            result.Add(CreateParameter("Id", item.GetId()));
            result.Add(CreateParameter("Body", item.Body));
            result.Add(CreateParameter("Date", item.Date));
            result.Add(CreateParameter("EnableSsl", item.EnableSsl));
            result.Add(CreateParameter("Html", item.Html));
            result.Add(CreateParameter("SenderAddress", item.SenderAddress));
            result.Add(CreateParameter("SenderName", item.SenderName));
            result.Add(CreateParameter("Subject", item.Subject));
            result.Add(CreateParameter("To", item.To));
            result.Add(CreateParameter("Attachments", item.Attachments));
            result.Add(CreateParameter("Bcc", item.Bcc));
            result.Add(CreateParameter("Cc", item.Cc));
            result.Add(CreateParameter("Retries", item.Retries));
            result.Add(CreateParameter("VCalendarView", item.VCalendarView));
            result.Add(CreateParameter("Username", item.Username));
            result.Add(CreateParameter("Password", item.Password));
            result.Add(CreateParameter("SmtpHost", item.SmtpHost));
            result.Add(CreateParameter("SmtpPort", item.SmtpPort));
            result.Add(CreateParameter("Category", item.Category));
            result.Add(CreateParameter("_DELETED", item.IsMarkedSoftDeleted));
            
            return result.ToArray();
        }
        
        /// <summary>
        /// Deletes the specified Email queue item instance from the database.
        /// </summary>
        public override void Delete(IEntity record)
        {
            ExecuteNonQuery(DELETE_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", record.GetId()));
        }
    }
}