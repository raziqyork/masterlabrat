﻿namespace AppData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// Provides data-access facilities for Tertiary records.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TertiaryRecordDataProvider : SqlDataProvider
    {
        #region SQL Commands
        
        /// <summary>
        /// Gets a SQL command text to query a single Tertiary record record
        /// </summary>
        static string SELECT_COMMAND { get { return "SELECT {0} FROM [TertiaryRecords] AS T WHERE T.[ID] = @Id".FormatWith(COLUMNS_LIST); } }
        
        /// <summary>
        /// Gets the list of columns to use for loading Tertiary records.
        /// </summary>
        static string COLUMNS_LIST = @"T.[ID] AS [TertiaryRecords_Id],
        T.[TextField] AS [TertiaryRecords_TextField],
        T.[DateField] AS [TertiaryRecords_DateField],
        T.[OptionField] AS [TertiaryRecords_OptionField]";
        
        /// <summary>
        /// Gets a SQL command text to insert a record into TertiaryRecords table.
        /// </summary>
        static string INSERT_COMMAND = @"INSERT INTO [TertiaryRecords]
        ([ID], [TextField], [DateField], [OptionField])
        VALUES
        (@ID, @TextField, @DateField, @OptionField)";
        
        /// <summary>
        /// Gets a SQL command text to update a record in TertiaryRecords table.
        /// </summary>
        static string UPDATE_COMMAND = @"UPDATE [TertiaryRecords] SET
        [ID] = @ID,
        [TextField] = @TextField,
        [DateField] = @DateField,
        [OptionField] = @OptionField
        WHERE [Id] = @OriginalId";
        
        /// <summary>
        /// Gets a SQL command text to delete a record from TertiaryRecords table.
        /// </summary>
        static string DELETE_COMMAND = @"DELETE FROM [TertiaryRecords] WHERE [ID] = @Id";
        
        #endregion
        
        #region Property-Column Mappings
        
        /// <summary>
        /// Gets the mapping between property names and database column names.
        /// </summary>
        internal static Dictionary<string, string> PropertyColumnMappings = new Dictionary<string, string> {
        { "ID", "T.[ID]" },
        { "TextField", "T.[TextField]" },
        { "DateField", "T.[DateField]" },
        { "OptionField", "T.[OptionField]" }};
        
        #endregion
        
        #region Subquery Mappings
        
        /// <summary>
        /// Gets the mapping between association details and required subqueries.
        /// This enables database-level execution of indirect property criteria.
        /// </summary>
        static IEnumerable<PropertySubqueryMapping> GetSubQueryMappings()
        {
            yield return new PropertySubqueryMapping("OptionField.*", "TertiaryRecords_OptionField_", OptionSet1DataProvider.PropertyColumnMappings)
            {
                Subquery = "SELECT [TertiaryRecords_OptionField_O].[ID] FROM [OptionSet1s] AS TertiaryRecords_OptionField_O WHERE [TertiaryRecords_OptionField_O].[ID] = T.[OptionField]"
            };
        }
        
        #endregion
        
        /// <summary>
        /// Gets the specified Tertiary record instance from the database.
        /// </summary>
        public override IEntity Get(Type type, object objectID)
        {
            using (var reader = ExecuteReader(SELECT_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", objectID)))
            {
                if (reader.Read())
                {
                    return Parse(reader);
                }
                else
                {
                    throw new DataException("There is no Tertiary record record with the ID of '{0}'.".FormatWith(objectID));
                }
            }
        }
        
        /// <summary>
        /// Gets the list of Tertiary records that match the specified criteria.
        /// </summary>
        public override IEnumerable<IEntity> GetList(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.TertiaryRecord>(criteria, options, PropertyColumnMappings, GetSubQueryMappings);
            
            using (var reader = ExecuteReader(queryBuilder.GenerateQuery(COLUMNS_LIST, "[TertiaryRecords] AS T"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters)))
            {
                var result = new List<App.TertiaryRecord>();
                while (reader.Read())
                {
                    result.Add(Parse(reader));
                }
                
                return result;
            }
        }
        
        /// <summary>
        /// Gets a count of the Tertiary records that match the specified criteria.
        /// </summary>
        public override int Count(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.TertiaryRecord>(criteria, options, PropertyColumnMappings, GetSubQueryMappings);
            
            return (int)ExecuteScalar(queryBuilder.GenerateCountQuery("[TertiaryRecords] AS T"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters));
        }
        
        /// <summary>
        /// Lazy-loads the data for the specified many-to-many relation on the specified Tertiary record instance from the database.
        /// </summary>
        public override IEnumerable<string> ReadManyToManyRelation(IEntity instance, string property)
        {
            throw new ArgumentException("The property '{0}' is not supported for the instance of '{1}'".FormatWith(property, instance.GetType().ToString()));
        }
        
        /// <summary>
        /// Extracts the Tertiary record instance from the current record of the specified data reader.
        /// </summary>
        internal static App.TertiaryRecord Parse(IDataReader reader)
        {
            var result = new App.TertiaryRecord();
            FillData(reader, result);
            EntityManager.SetSaved(result, reader.GetGuid(0));
            return result;
        }
        
        /// <summary>
        /// Loads the data from the specified data reader on the specified Tertiary record instance.
        /// </summary>
        internal static void FillData(IDataReader reader, App.TertiaryRecord entity)
        {
            var values = new object[reader.FieldCount];
            reader.GetValues(values);
            
            if (values[1] != DBNull.Value) entity.TextField = (string)values[1];
            
            entity.DateField = ((DateTime)values[2]).Date;
            
            entity.OptionFieldId = (Guid)values[3];
        }
        
        /// <summary>
        /// Saves the specified Tertiary record instance in the database.
        /// </summary>
        public override void Save(IEntity record)
        {
            var item = record as App.TertiaryRecord;
            
            if (record.IsNew)
            {
                Insert(item);
            }
            else
            {
                Update(item);
            }
        }
        
        /// <summary>
        /// Inserts the specified new Tertiary record instance into the database.
        /// </summary>
        void Insert(App.TertiaryRecord item)
        {
            ExecuteNonQuery(INSERT_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Bulk inserts a number of specified Tertiary records into the database.
        /// </summary>
        public override void BulkInsert(IEntity[] entities, int batchSize)
        {
            var commands = new List<KeyValuePair<string, IDataParameter[]>>();
            
            foreach (var item in entities.Cast<App.TertiaryRecord>())
            {
                commands.Add(INSERT_COMMAND, CreateParameters(item));
            }
            
            ExecuteNonQuery(CommandType.Text, commands);
        }
        
        /// <summary>
        /// Updates the specified existing Tertiary record instance in the database.
        /// </summary>
        void Update(App.TertiaryRecord item)
        {
            ExecuteNonQuery(UPDATE_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Creates parameters for Inserting or Updating Tertiary record records
        /// </summary>
        IDataParameter[] CreateParameters(App.TertiaryRecord item)
        {
            var result = new List<IDataParameter>();
            
            result.Add(CreateParameter("OriginalId", item.OriginalId));
            result.Add(CreateParameter("Id", item.GetId()));
            result.Add(CreateParameter("TextField", item.TextField));
            result.Add(CreateParameter("DateField", item.DateField));
            result.Add(CreateParameter("OptionField", item.OptionFieldId));
            
            return result.ToArray();
        }
        
        /// <summary>
        /// Deletes the specified Tertiary record instance from the database.
        /// </summary>
        public override void Delete(IEntity record)
        {
            ExecuteNonQuery(DELETE_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", record.GetId()));
        }
    }
}