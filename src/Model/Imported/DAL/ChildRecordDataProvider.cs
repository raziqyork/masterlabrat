﻿namespace AppData
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Data.Ado.Net;
    
    /// <summary>
    /// Provides data-access facilities for Child records.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class ChildRecordDataProvider : SqlDataProvider
    {
        #region SQL Commands
        
        /// <summary>
        /// Gets a SQL command text to query a single Child record record
        /// </summary>
        static string SELECT_COMMAND { get { return "SELECT {0} FROM [ChildRecords] AS C WHERE C.[ID] = @Id".FormatWith(COLUMNS_LIST); } }
        
        /// <summary>
        /// Gets the list of columns to use for loading Child records.
        /// </summary>
        static string COLUMNS_LIST = @"C.[ID] AS [ChildRecords_Id],
        C.[MainRecord] AS [ChildRecords_MainRecord],
        C.[AutocompleteField] AS [ChildRecords_AutocompleteField],
        C.[DropdownField] AS [ChildRecords_DropdownField],
        C.[FileField_FileName] AS [ChildRecords_FileField_FileName]";
        
        /// <summary>
        /// Gets a SQL command text to insert a record into ChildRecords table.
        /// </summary>
        static string INSERT_COMMAND = @"INSERT INTO [ChildRecords]
        ([ID], [MainRecord], [AutocompleteField], [DropdownField], [FileField_FileName])
        VALUES
        (@ID, @MainRecord, @AutocompleteField, @DropdownField, @FileField_FileName)";
        
        /// <summary>
        /// Gets a SQL command text to update a record in ChildRecords table.
        /// </summary>
        static string UPDATE_COMMAND = @"UPDATE [ChildRecords] SET
        [ID] = @ID,
        [MainRecord] = @MainRecord,
        [AutocompleteField] = @AutocompleteField,
        [DropdownField] = @DropdownField,
        [FileField_FileName] = @FileField_FileName
        WHERE [Id] = @OriginalId";
        
        /// <summary>
        /// Gets a SQL command text to delete a record from ChildRecords table.
        /// </summary>
        static string DELETE_COMMAND = @"DELETE FROM [ChildRecords] WHERE [ID] = @Id";
        
        #endregion
        
        #region Property-Column Mappings
        
        /// <summary>
        /// Gets the mapping between property names and database column names.
        /// </summary>
        internal static Dictionary<string, string> PropertyColumnMappings = new Dictionary<string, string> {
        { "ID", "C.[ID]" },
        { "MainRecord", "C.[MainRecord]" },
        { "AutocompleteField", "C.[AutocompleteField]" },
        { "DropdownField", "C.[DropdownField]" },
        { "FileField", "C.[FileField_FileName]" }};
        
        #endregion
        
        #region Subquery Mappings
        
        /// <summary>
        /// Gets the mapping between association details and required subqueries.
        /// This enables database-level execution of indirect property criteria.
        /// </summary>
        static IEnumerable<PropertySubqueryMapping> GetSubQueryMappings()
        {
            yield return new PropertySubqueryMapping("MainRecord.*", "ChildRecords_MainRecord_", MainRecordDataProvider.PropertyColumnMappings)
            {
                Subquery = "SELECT [ChildRecords_MainRecord_M].[ID] FROM [MainRecords] AS ChildRecords_MainRecord_M WHERE [ChildRecords_MainRecord_M].[ID] = C.[MainRecord]"
            };
            
            yield return new PropertySubqueryMapping("AutocompleteField.*", "ChildRecords_AutocompleteField_", OptionSet3DataProvider.PropertyColumnMappings)
            {
                Subquery = "SELECT [ChildRecords_AutocompleteField_O].[ID] FROM [OptionSet3s] AS ChildRecords_AutocompleteField_O WHERE [ChildRecords_AutocompleteField_O].[ID] = C.[AutocompleteField]"
            };
            
            yield return new PropertySubqueryMapping("DropdownField.*", "ChildRecords_DropdownField_", OptionSet2DataProvider.PropertyColumnMappings)
            {
                Subquery = "SELECT [ChildRecords_DropdownField_O].[ID] FROM [OptionSet2s] AS ChildRecords_DropdownField_O WHERE [ChildRecords_DropdownField_O].[ID] = C.[DropdownField]"
            };
        }
        
        #endregion
        
        /// <summary>
        /// Gets the specified Child record instance from the database.
        /// </summary>
        public override IEntity Get(Type type, object objectID)
        {
            using (var reader = ExecuteReader(SELECT_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", objectID)))
            {
                if (reader.Read())
                {
                    return Parse(reader);
                }
                else
                {
                    throw new DataException("There is no Child record record with the ID of '{0}'.".FormatWith(objectID));
                }
            }
        }
        
        /// <summary>
        /// Gets the list of Child records that match the specified criteria.
        /// </summary>
        public override IEnumerable<IEntity> GetList(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.ChildRecord>(criteria, options, PropertyColumnMappings, GetSubQueryMappings);
            
            using (var reader = ExecuteReader(queryBuilder.GenerateQuery(COLUMNS_LIST, "[ChildRecords] AS C"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters)))
            {
                var result = new List<App.ChildRecord>();
                while (reader.Read())
                {
                    result.Add(Parse(reader));
                }
                
                return result;
            }
        }
        
        /// <summary>
        /// Gets a count of the Child records that match the specified criteria.
        /// </summary>
        public override int Count(Type type, IEnumerable<ICriterion> criteria, params QueryOption[] options)
        {
            var queryBuilder = new SqlQueryBuilder<App.ChildRecord>(criteria, options, PropertyColumnMappings, GetSubQueryMappings);
            
            return (int)ExecuteScalar(queryBuilder.GenerateCountQuery("[ChildRecords] AS C"), System.Data.CommandType.Text, GenerateParameters(queryBuilder.Parameters));
        }
        
        /// <summary>
        /// Lazy-loads the data for the specified many-to-many relation on the specified Child record instance from the database.
        /// </summary>
        public override IEnumerable<string> ReadManyToManyRelation(IEntity instance, string property)
        {
            throw new ArgumentException("The property '{0}' is not supported for the instance of '{1}'".FormatWith(property, instance.GetType().ToString()));
        }
        
        /// <summary>
        /// Extracts the Child record instance from the current record of the specified data reader.
        /// </summary>
        internal static App.ChildRecord Parse(IDataReader reader)
        {
            var result = new App.ChildRecord();
            FillData(reader, result);
            EntityManager.SetSaved(result, reader.GetGuid(0));
            return result;
        }
        
        /// <summary>
        /// Loads the data from the specified data reader on the specified Child record instance.
        /// </summary>
        internal static void FillData(IDataReader reader, App.ChildRecord entity)
        {
            var values = new object[reader.FieldCount];
            reader.GetValues(values);
            
            entity.MainRecordId = (Guid)values[1];
            
            entity.AutocompleteFieldId = (Guid)values[2];
            
            if (values[3] != DBNull.Value) entity.DropdownFieldId = (Guid)values[3];
            
            entity.FileField = new Document { FileName = (string)values[4] };
        }
        
        /// <summary>
        /// Saves the specified Child record instance in the database.
        /// </summary>
        public override void Save(IEntity record)
        {
            var item = record as App.ChildRecord;
            
            if (record.IsNew)
            {
                Insert(item);
            }
            else
            {
                Update(item);
            }
        }
        
        /// <summary>
        /// Inserts the specified new Child record instance into the database.
        /// </summary>
        void Insert(App.ChildRecord item)
        {
            ExecuteNonQuery(INSERT_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Bulk inserts a number of specified Child records into the database.
        /// </summary>
        public override void BulkInsert(IEntity[] entities, int batchSize)
        {
            var commands = new List<KeyValuePair<string, IDataParameter[]>>();
            
            foreach (var item in entities.Cast<App.ChildRecord>())
            {
                commands.Add(INSERT_COMMAND, CreateParameters(item));
            }
            
            ExecuteNonQuery(CommandType.Text, commands);
        }
        
        /// <summary>
        /// Updates the specified existing Child record instance in the database.
        /// </summary>
        void Update(App.ChildRecord item)
        {
            ExecuteNonQuery(UPDATE_COMMAND, CommandType.Text, CreateParameters(item));
        }
        
        /// <summary>
        /// Creates parameters for Inserting or Updating Child record records
        /// </summary>
        IDataParameter[] CreateParameters(App.ChildRecord item)
        {
            var result = new List<IDataParameter>();
            
            result.Add(CreateParameter("OriginalId", item.OriginalId));
            result.Add(CreateParameter("Id", item.GetId()));
            result.Add(CreateParameter("MainRecord", item.MainRecordId));
            result.Add(CreateParameter("AutocompleteField", item.AutocompleteFieldId));
            result.Add(CreateParameter("DropdownField", item.DropdownFieldId));
            result.Add(CreateParameter("FileField_FileName", item.FileField.FileName));
            
            return result.ToArray();
        }
        
        /// <summary>
        /// Deletes the specified Child record instance from the database.
        /// </summary>
        public override void Delete(IEntity record)
        {
            ExecuteNonQuery(DELETE_COMMAND, System.Data.CommandType.Text, CreateParameter("Id", record.GetId()));
        }
    }
}