﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    
    /// <summary>
    /// Represents an instance of Tertiary record entity type.
    /// </summary>
    
    public partial class TertiaryRecord : GuidEntity
    {
        /* -------------------------- Constructor -----------------------*/
        
        /// <summary>
        /// Initializes a new instance of the TertiaryRecord class.
        /// </summary>
        public TertiaryRecord()
        {
            this.Deleting += this.Cascade_Deleting;
        }
        
        /* -------------------------- Properties -------------------------*/
        
        #region Date field Property
        
        /// <summary>
        /// Gets or sets the value of DateField on this Tertiary record instance.
        /// </summary>
        public DateTime DateField { get; set; }
        
        #endregion
        
        #region Text field Property
        
        /// <summary>
        /// Gets or sets the value of TextField on this Tertiary record instance.
        /// </summary>
        public string TextField { get; set; }
        
        #endregion
        
        #region Option field Association
        
        /// <summary>
        /// Gets or sets the ID of the associated Option field.
        /// </summary>
        public Guid? OptionFieldId { get; set; }
        
        /// <summary>
        /// Gets or sets the value of Option field on this Tertiary record instance.
        /// </summary>
        public OptionSet1 OptionField
        {
            get
            {
                return Database.Get<OptionSet1>(this.OptionFieldId);
            }
            
            set
            {
                this.OptionFieldId = value.Get(o => o.ID);
            }
        }
        
        #endregion
        
        #region Secondary records Association
        
        /// <summary>
        /// Gets the Secondary records of this Tertiary record.
        /// </summary>
        [Calculated]
        [XmlIgnore]
        public IEnumerable<SecondaryRecord> SecondaryRecords
        {
            get
            {
                return Database.GetList<SecondaryRecord>(s => s.TertiaryRecord == this);
            }
        }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Find and returns an instance of Tertiary record from the database by its Date field.
        /// If no matching Tertiary record is found, it returns Null.
        /// </summary>
        /// <param name="dateField">The Date field of the requested Tertiary record.</param>
        /// <returns>The Tertiary record instance with the specified Date field or null if there is no Tertiary record with that Date field in the database.</returns>
        public static TertiaryRecord FindByDateField(DateTime dateField)
        {
            return Database.Find<TertiaryRecord>(t => t.DateField == dateField);
        }
        
        /// <summary>
        /// Returns a textual representation of this Tertiary record.
        /// </summary>
        /// <returns>A string value that represents this Tertiary record instance.</returns>
        public override string ToString()
        {
            return this.TextField.Or(string.Empty);
        }
        
        /// <summary>
        /// Returns a clone of this Tertiary record.
        /// </summary>
        /// <returns>
        /// A new Tertiary record object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new TertiaryRecord Clone()
        {
            return (TertiaryRecord)base.Clone();
        }
        
        /// <summary>
        /// Validates the data for the properties of this Tertiary record.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate DateField property:
            // Ensure uniqueness of DateField.
            
            if (Database.Any<TertiaryRecord>(t => t.DateField == this.DateField && t != this))
            {
                throw new ValidationException("Date field must be unique. There is an existing Tertiary record record with the provided Date field.");
            }
            
            // Validate OptionField property:
            
            if (this.OptionFieldId == null)
            {
                throw new ValidationException("Please provide a value for Option field.");
            }
            
            // Validate TextField property:
            
            if (this.TextField != null && this.TextField.Length > 200)
            {
                throw new ValidationException("The provided Text field is too long. A maximum of 200 characters is acceptable.");
            }
        }
        
        /// <summary>
        /// Handles the Deleting event of this Tertiary record.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The CancelEventArgs instance containing the event data.</param>
        private void Cascade_Deleting(object source, System.ComponentModel.CancelEventArgs e)
        {
            if (this.SecondaryRecords.Any())
            {
                throw new ValidationException("This Tertiary record cannot be deleted because of {0} dependent Secondary records.", SecondaryRecords.Count());
            }
        }
    }
}