﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Services;
    
    /// <summary>
    /// Represents an instance of Option set 2 entity type.
    /// </summary>
    
    [SmallTable]
    public partial class OptionSet2 : GuidEntity, IComparable<OptionSet2>, ISortable
    {
        /* -------------------------- Fields -------------------------*/
        
        /// <summary>
        /// Stores a cache for the Option X Option set 2 object.
        /// </summary>
        private static OptionSet2 optionX;
        
        /// <summary>
        /// Stores a cache for the Option Y Option set 2 object.
        /// </summary>
        private static OptionSet2 optionY;
        
        /// <summary>
        /// Stores a cache for the Option Z Option set 2 object.
        /// </summary>
        private static OptionSet2 optionZ;
        
        /* -------------------------- Constructor -----------------------*/
        
        /// <summary>
        /// Initializes a new instance of the OptionSet2 class.
        /// </summary>
        public OptionSet2()
        {
            this.Deleting += this.Cascade_Deleting;
            this.Saving += this.OptionSet2_Saving;
        }
        
        /* -------------------------- Properties -------------------------*/
        
        /// <summary>
        /// Gets the Option X Option set 2 object.
        /// </summary>
        public static OptionSet2 OptionX
        {
            get
            {
                if (optionX == null)
                {
                    optionX = Parse("Option X");
                    optionX.Saving += (o, e) => optionX = null;
                    optionX.Saved += (o, e) => optionX = null;
                    Database.CacheRefreshed += (o, e) => optionX = null;
                }
                
                return optionX;
            }
        }
        
        /// <summary>
        /// Gets the Option Y Option set 2 object.
        /// </summary>
        public static OptionSet2 OptionY
        {
            get
            {
                if (optionY == null)
                {
                    optionY = Parse("Option Y");
                    optionY.Saving += (o, e) => optionY = null;
                    optionY.Saved += (o, e) => optionY = null;
                    Database.CacheRefreshed += (o, e) => optionY = null;
                }
                
                return optionY;
            }
        }
        
        /// <summary>
        /// Gets the Option Z Option set 2 object.
        /// </summary>
        public static OptionSet2 OptionZ
        {
            get
            {
                if (optionZ == null)
                {
                    optionZ = Parse("Option Z");
                    optionZ.Saving += (o, e) => optionZ = null;
                    optionZ.Saved += (o, e) => optionZ = null;
                    Database.CacheRefreshed += (o, e) => optionZ = null;
                }
                
                return optionZ;
            }
        }
        
        #region Name Property
        
        /// <summary>
        /// Gets or sets the value of Name on this Option set 2 instance.
        /// </summary>
        public string Name { get; set; }
        
        #endregion
        
        #region Order Property
        
        /// <summary>
        /// Gets or sets the value of Order on this Option set 2 instance.
        /// </summary>
        public int Order { get; set; }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Find and returns an instance of Option set 2 from the database by its Name.
        /// If no matching Option set 2 is found, it returns Null.
        /// </summary>
        /// <param name="name">The Name of the requested Option set 2.</param>
        /// <returns>The Option set 2 instance with the specified Name or null if there is no Option set 2 with that Name in the database.</returns>
        public static OptionSet2 FindByName(string name)
        {
            return Database.Find<OptionSet2>(o => o.Name == name);
        }
        
        /// <summary>
        /// Returns the Option set 2 instance that is textually represented with a specified string value, or null if no such object is found.
        /// </summary>
        /// <param name="text">The text representing the Option set 2 to be retrieved from the database.</param>
        /// <returns>The Option set 2 object whose string representation matches the specified text.</returns>
        public static OptionSet2 Parse(string text)
        {
            if (text.IsEmpty())
            {
                throw new ArgumentNullException("text");
            }
            
            return Database.Find<OptionSet2>(o => o.Name == text);
        }
        
        /// <summary>
        /// Returns a textual representation of this Option set 2.
        /// </summary>
        /// <returns>A string value that represents this Option set 2 instance.</returns>
        public override string ToString()
        {
            return this.Name;
        }
        
        /// <summary>
        /// Returns a clone of this Option set 2.
        /// </summary>
        /// <returns>
        /// A new Option set 2 object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new OptionSet2 Clone()
        {
            return (OptionSet2)base.Clone();
        }
        
        /// <summary>
        /// Compares this Option set 2 with another specified Option set 2 instance.
        /// </summary>
        /// <param name="other">The other Option set 2 to compare this instance to.</param>
        /// <returns>An integer value indicating whether this instance precedes, follows, or appears in the same position as the other Option set 2 in sort orders.</returns>
        public int CompareTo(OptionSet2 other)
        {
            if (other == null)
            {
                return 1;
            }
            else
            {
                return this.Order.CompareTo(other.Order);
            }
        }
        
        /// <summary>
        /// Validates the data for the properties of this Option set 2.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate Name property:
            
            if (this.Name.IsEmpty())
            {
                throw new ValidationException("Name cannot be empty.");
            }
            
            if (this.Name.Length > 200)
            {
                throw new ValidationException("The provided Name is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Ensure uniqueness of Name.
            
            if (Database.Any<OptionSet2>(o => o.Name == this.Name && o != this))
            {
                throw new ValidationException("Name must be unique. There is an existing Option set 2 record with the provided Name.");
            }
            
            // Validate Order property:
            
            if (this.Order < 0)
            {
                throw new ValidationException("The value of Order must be 0 or more.");
            }
        }
        
        /// <summary>
        /// Handles the Deleting event of this Option set 2.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The CancelEventArgs instance containing the event data.</param>
        private void Cascade_Deleting(object source, System.ComponentModel.CancelEventArgs e)
        {
            var dependantChildRecords = Database.GetList<ChildRecord>(c => c.DropdownField == this);
            
            if (dependantChildRecords.Any())
            {
                throw new ValidationException("This Option set 2 cannot be deleted because of {0} dependent Child record record(s).", dependantChildRecords.Count());
            }
            
            var dependantMainRecords = Database.GetList<MainRecord>(m => m.RadiobuttonField == this);
            
            if (dependantMainRecords.Any())
            {
                throw new ValidationException("This Option set 2 cannot be deleted because of {0} dependent Main record record(s).", dependantMainRecords.Count());
            }
        }
        
        /// <summary>
        /// Handles the Saving event of the OptionSet2 instance.
        /// </summary>
        /// <param name="sender">The source of the event. This will be this instance itself.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void OptionSet2_Saving(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.IsNew && this.Order == 0)
            {
                // This is a new Option set 2 with unset Order value.
                // So set the Order property so that this Option set 2 goes to the end of the list:
                this.Order = Sorter.GetNewOrder((ISortable)sender);
            }
        }
    }
}