﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    using MSharp.Framework.Services;
    
    /// <summary>
    /// Represents an instance of Option set 1 entity type.
    /// </summary>
    
    [SmallTable]
    public partial class OptionSet1 : GuidEntity, IComparable<OptionSet1>, ISortable
    {
        /* -------------------------- Fields -------------------------*/
        
        /// <summary>
        /// Stores a cache for the Option A Option set 1 object.
        /// </summary>
        private static OptionSet1 optionA;
        
        /// <summary>
        /// Stores a cache for the Option B Option set 1 object.
        /// </summary>
        private static OptionSet1 optionB;
        
        /// <summary>
        /// Stores a cache for the Option C Option set 1 object.
        /// </summary>
        private static OptionSet1 optionC;
        
        /// <summary>
        /// Stores a cache for the Option D Option set 1 object.
        /// </summary>
        private static OptionSet1 optionD;
        
        /* -------------------------- Constructor -----------------------*/
        
        /// <summary>
        /// Initializes a new instance of the OptionSet1 class.
        /// </summary>
        public OptionSet1()
        {
            this.Deleting += this.Cascade_Deleting;
            this.Saving += this.OptionSet1_Saving;
        }
        
        /* -------------------------- Properties -------------------------*/
        
        /// <summary>
        /// Gets the Option A Option set 1 object.
        /// </summary>
        public static OptionSet1 OptionA
        {
            get
            {
                if (optionA == null)
                {
                    optionA = Parse("Option A");
                    optionA.Saving += (o, e) => optionA = null;
                    optionA.Saved += (o, e) => optionA = null;
                    Database.CacheRefreshed += (o, e) => optionA = null;
                }
                
                return optionA;
            }
        }
        
        /// <summary>
        /// Gets the Option B Option set 1 object.
        /// </summary>
        public static OptionSet1 OptionB
        {
            get
            {
                if (optionB == null)
                {
                    optionB = Parse("Option B");
                    optionB.Saving += (o, e) => optionB = null;
                    optionB.Saved += (o, e) => optionB = null;
                    Database.CacheRefreshed += (o, e) => optionB = null;
                }
                
                return optionB;
            }
        }
        
        /// <summary>
        /// Gets the Option C Option set 1 object.
        /// </summary>
        public static OptionSet1 OptionC
        {
            get
            {
                if (optionC == null)
                {
                    optionC = Parse("Option C");
                    optionC.Saving += (o, e) => optionC = null;
                    optionC.Saved += (o, e) => optionC = null;
                    Database.CacheRefreshed += (o, e) => optionC = null;
                }
                
                return optionC;
            }
        }
        
        /// <summary>
        /// Gets the Option D Option set 1 object.
        /// </summary>
        public static OptionSet1 OptionD
        {
            get
            {
                if (optionD == null)
                {
                    optionD = Parse("Option D");
                    optionD.Saving += (o, e) => optionD = null;
                    optionD.Saved += (o, e) => optionD = null;
                    Database.CacheRefreshed += (o, e) => optionD = null;
                }
                
                return optionD;
            }
        }
        
        #region Name Property
        
        /// <summary>
        /// Gets or sets the value of Name on this Option set 1 instance.
        /// </summary>
        public string Name { get; set; }
        
        #endregion
        
        #region Order Property
        
        /// <summary>
        /// Gets or sets the value of Order on this Option set 1 instance.
        /// </summary>
        public int Order { get; set; }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Find and returns an instance of Option set 1 from the database by its Name.
        /// If no matching Option set 1 is found, it returns Null.
        /// </summary>
        /// <param name="name">The Name of the requested Option set 1.</param>
        /// <returns>The Option set 1 instance with the specified Name or null if there is no Option set 1 with that Name in the database.</returns>
        public static OptionSet1 FindByName(string name)
        {
            return Database.Find<OptionSet1>(o => o.Name == name);
        }
        
        /// <summary>
        /// Returns the Option set 1 instance that is textually represented with a specified string value, or null if no such object is found.
        /// </summary>
        /// <param name="text">The text representing the Option set 1 to be retrieved from the database.</param>
        /// <returns>The Option set 1 object whose string representation matches the specified text.</returns>
        public static OptionSet1 Parse(string text)
        {
            if (text.IsEmpty())
            {
                throw new ArgumentNullException("text");
            }
            
            return Database.Find<OptionSet1>(o => o.Name == text);
        }
        
        /// <summary>
        /// Returns a textual representation of this Option set 1.
        /// </summary>
        /// <returns>A string value that represents this Option set 1 instance.</returns>
        public override string ToString()
        {
            return this.Name;
        }
        
        /// <summary>
        /// Returns a clone of this Option set 1.
        /// </summary>
        /// <returns>
        /// A new Option set 1 object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new OptionSet1 Clone()
        {
            return (OptionSet1)base.Clone();
        }
        
        /// <summary>
        /// Compares this Option set 1 with another specified Option set 1 instance.
        /// </summary>
        /// <param name="other">The other Option set 1 to compare this instance to.</param>
        /// <returns>An integer value indicating whether this instance precedes, follows, or appears in the same position as the other Option set 1 in sort orders.</returns>
        public int CompareTo(OptionSet1 other)
        {
            if (other == null)
            {
                return 1;
            }
            else
            {
                return this.Order.CompareTo(other.Order);
            }
        }
        
        /// <summary>
        /// Validates the data for the properties of this Option set 1.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate Name property:
            
            if (this.Name.IsEmpty())
            {
                throw new ValidationException("Name cannot be empty.");
            }
            
            if (this.Name.Length > 200)
            {
                throw new ValidationException("The provided Name is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Ensure uniqueness of Name.
            
            if (Database.Any<OptionSet1>(o => o.Name == this.Name && o != this))
            {
                throw new ValidationException("Name must be unique. There is an existing Option set 1 record with the provided Name.");
            }
            
            // Validate Order property:
            
            if (this.Order < 0)
            {
                throw new ValidationException("The value of Order must be 0 or more.");
            }
        }
        
        /// <summary>
        /// Handles the Deleting event of this Option set 1.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The CancelEventArgs instance containing the event data.</param>
        private void Cascade_Deleting(object source, System.ComponentModel.CancelEventArgs e)
        {
            var dependantTertiaryRecords = Database.GetList<TertiaryRecord>(t => t.OptionField == this);
            
            if (dependantTertiaryRecords.Any())
            {
                throw new ValidationException("This Option set 1 cannot be deleted because of {0} dependent Tertiary record record(s).", dependantTertiaryRecords.Count());
            }
        }
        
        /// <summary>
        /// Handles the Saving event of the OptionSet1 instance.
        /// </summary>
        /// <param name="sender">The source of the event. This will be this instance itself.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void OptionSet1_Saving(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.IsNew && this.Order == 0)
            {
                // This is a new Option set 1 with unset Order value.
                // So set the Order property so that this Option set 1 goes to the end of the list:
                this.Order = Sorter.GetNewOrder((ISortable)sender);
            }
        }
    }
}