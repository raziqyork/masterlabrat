﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    
    /// <summary>
    /// Represents an instance of Child record entity type.
    /// </summary>
    
    public partial class ChildRecord : GuidEntity
    {
        /* -------------------------- Fields -------------------------*/
        
        /// <summary>
        /// Stores the binary information for FileField property.
        /// </summary>
        private MSharp.Framework.Document fileField;
        
        /* -------------------------- Properties -------------------------*/
        
        #region File field Property
        
        /// <summary>
        /// Gets or sets the value of FileField on this Child record instance.
        /// </summary>
        public MSharp.Framework.Document FileField
        {
            get
            {
                if (this.fileField == null)
                {
                    this.fileField = MSharp.Framework.Document.Empty().Attach(this, "FileField");
                }
                
                return this.fileField;
            }
            
            set
            {
                if (this.fileField != null)
                {
                    // Detach the previous file, so it doesn't get updated or deleted with this Child record instance.
                    this.fileField.Detach();
                }
                
                if (value == null)
                {
                    value = MSharp.Framework.Document.Empty();
                }
                
                this.fileField = value.Attach(this, "FileField");
            }
        }
        
        #endregion
        
        #region Autocomplete field Association
        
        /// <summary>
        /// Gets or sets the ID of the associated Autocomplete field.
        /// </summary>
        public Guid? AutocompleteFieldId { get; set; }
        
        /// <summary>
        /// Gets or sets the value of Autocomplete field on this Child record instance.
        /// </summary>
        public OptionSet3 AutocompleteField
        {
            get
            {
                return Database.Get<OptionSet3>(this.AutocompleteFieldId);
            }
            
            set
            {
                this.AutocompleteFieldId = value.Get(o => o.ID);
            }
        }
        
        #endregion
        
        #region Dropdown field Association
        
        /// <summary>
        /// Gets or sets the ID of the associated Dropdown field.
        /// </summary>
        public Guid? DropdownFieldId { get; set; }
        
        /// <summary>
        /// Gets or sets the value of Dropdown field on this Child record instance.
        /// </summary>
        public OptionSet2 DropdownField
        {
            get
            {
                return Database.Get<OptionSet2>(this.DropdownFieldId);
            }
            
            set
            {
                this.DropdownFieldId = value.Get(o => o.ID);
            }
        }
        
        #endregion
        
        #region Main record Association
        
        /// <summary>
        /// Gets or sets the ID of the associated Main record.
        /// </summary>
        public Guid? MainRecordId { get; set; }
        
        /// <summary>
        /// Gets or sets the value of Main record on this Child record instance.
        /// </summary>
        public MainRecord MainRecord
        {
            get
            {
                return Database.Get<MainRecord>(this.MainRecordId);
            }
            
            set
            {
                this.MainRecordId = value.Get(m => m.ID);
            }
        }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Returns a textual representation of this Child record.
        /// </summary>
        /// <returns>A string value that represents this Child record instance.</returns>
        public override string ToString()
        {
            return "Child record (" + this.ID + ")";
        }
        
        /// <summary>
        /// Returns a clone of this Child record.
        /// </summary>
        /// <returns>
        /// A new Child record object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new ChildRecord Clone()
        {
            var clone = (ChildRecord) base.Clone();
            
            clone.FileField = this.FileField.Clone();
            return clone;
        }
        
        /// <summary>
        /// Validates the data for the properties of this Child record.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate AutocompleteField property:
            
            if (this.AutocompleteFieldId == null)
            {
                throw new ValidationException("Please provide a value for Autocomplete field.");
            }
            
            // Validate FileField property:
            
            if (this.FileField.IsEmpty())
            {
                throw new ValidationException("It is necessary to upload File field.");
            }
            
            // Ensure the file uploaded for File field is safe:
            
            if (this.FileField.HasUnsafeExtension())
            {
                throw new ValidationException("The file uploaded for File field is unsafe because of the file extension: {0}", this.FileField.FileExtension);
            }
            
            // Validate MainRecord property:
            
            if (this.MainRecordId == null)
            {
                throw new ValidationException("Please provide a value for Main record.");
            }
        }
    }
}