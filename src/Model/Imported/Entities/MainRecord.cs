﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    
    /// <summary>
    /// Represents an instance of Main record entity type.
    /// </summary>
    
    public partial class MainRecord : GuidEntity
    {
        /* -------------------------- Fields -------------------------*/
        
        /// <summary>
        /// Stores the IDs of the associated Option set 1s objects for Option set 1s property.
        /// </summary>
        private IList<Guid> checkboxListFieldIds = new List<Guid>();
        
        /// <summary>
        /// Stores the binary information for DocumentField property.
        /// </summary>
        private MSharp.Framework.Document documentField;
        
        /* -------------------------- Constructor -----------------------*/
        
        /// <summary>
        /// Initializes a new instance of the MainRecord class.
        /// </summary>
        public MainRecord()
        {
            this.DateCreated = LocalTime.Now;
            this.SliderField = 50;
            this.Deleting += this.Cascade_Deleting;
        }
        
        /* -------------------------- Properties -------------------------*/
        
        #region Checkbox field Property
        
        /// <summary>
        /// Gets or sets a value indicating whether this Main record instance Checkbox field.
        /// </summary>
        public bool CheckboxField { get; set; }
        
        #endregion
        
        #region Date created Property
        
        /// <summary>
        /// Gets or sets the value of DateCreated on this Main record instance.
        /// </summary>
        public DateTime DateCreated { get; set; }
        
        #endregion
        
        #region Date field Property
        
        /// <summary>
        /// Gets or sets the value of DateField on this Main record instance.
        /// </summary>
        public DateTime? DateField { get; set; }
        
        #endregion
        
        #region DateTime field Property
        
        /// <summary>
        /// Gets or sets the value of DateTimeField on this Main record instance.
        /// </summary>
        public DateTime? DateTimeField { get; set; }
        
        #endregion
        
        #region Disabled Checkbox field Property
        
        /// <summary>
        /// Gets or sets a value indicating whether this Main record instance Disabled Checkbox field.
        /// </summary>
        public bool DisabledCheckboxField { get; set; }
        
        #endregion
        
        #region Disabled field Property
        
        /// <summary>
        /// Gets or sets the value of DisabledField on this Main record instance.
        /// </summary>
        public string DisabledField { get; set; }
        
        #endregion
        
        #region Document field Property
        
        /// <summary>
        /// Gets or sets the value of DocumentField on this Main record instance.
        /// </summary>
        public MSharp.Framework.Document DocumentField
        {
            get
            {
                if (this.documentField == null)
                {
                    this.documentField = MSharp.Framework.Document.Empty().Attach(this, "DocumentField");
                }
                
                return this.documentField;
            }
            
            set
            {
                if (this.documentField != null)
                {
                    // Detach the previous file, so it doesn't get updated or deleted with this Main record instance.
                    this.documentField.Detach();
                }
                
                if (value == null)
                {
                    value = MSharp.Framework.Document.Empty();
                }
                
                this.documentField = value.Attach(this, "DocumentField");
            }
        }
        
        #endregion
        
        #region Number field Property
        
        /// <summary>
        /// Gets or sets the value of NumberField on this Main record instance.
        /// </summary>
        public decimal? NumberField { get; set; }
        
        #endregion
        
        #region Pattern field Property
        
        /// <summary>
        /// Gets or sets the value of PatternField on this Main record instance.
        /// </summary>
        public string PatternField { get; set; }
        
        #endregion
        
        #region Rich text field Property
        
        /// <summary>
        /// Gets or sets the value of RichTextField on this Main record instance.
        /// </summary>
        public string RichTextField { get; set; }
        
        #endregion
        
        #region Slider field Property
        
        /// <summary>
        /// Gets or sets the value of SliderField on this Main record instance.
        /// </summary>
        public int SliderField { get; set; }
        
        #endregion
        
        #region Textarea field Property
        
        /// <summary>
        /// Gets or sets the value of TextareaField on this Main record instance.
        /// </summary>
        public string TextareaField { get; set; }
        
        #endregion
        
        #region Text field Property
        
        /// <summary>
        /// Gets or sets the value of TextField on this Main record instance.
        /// </summary>
        public string TextField { get; set; }
        
        #endregion
        
        #region Time field Property
        
        /// <summary>
        /// Gets or sets the value of TimeField on this Main record instance.
        /// </summary>
        public DateTime? TimeField { get; set; }
        
        #endregion
        
        #region Checkbox list field Association
        
        /// <summary>
        /// Gets or sets the identifiers of Checkbox list field of this Main record.
        /// </summary>
        [ManyToMany]
        public virtual IList<Guid> CheckboxListFieldIds
        {
            get { return this.checkboxListFieldIds; }
            set { this.checkboxListFieldIds = value ?? new List<Guid>(); }
        }
        
        /// <summary>
        /// Gets or sets the Checkbox list field of this Main record instance.
        /// </summary>
        [CacheDependent(typeof(OptionSet1))]
        public IEnumerable<OptionSet1> CheckboxListField
        {
            get
            {
                return Database.GetList<OptionSet1>(new Criterion("ID", FilterFunction.In, this.CheckboxListFieldIds));
            }
            
            set
            {
                this.CheckboxListFieldIds.Clear();
                
                if (value != null)
                {
                    this.CheckboxListFieldIds.AddRange(value.Distinct().ExceptNull().Select(o => o.ID));
                }
            }
        }
        
        #endregion
        
        #region Child records Association
        
        /// <summary>
        /// Gets the Child records of this Main record.
        /// </summary>
        [Calculated]
        [XmlIgnore]
        public IEnumerable<ChildRecord> ChildRecords
        {
            get
            {
                return Database.GetList<ChildRecord>(c => c.MainRecord == this);
            }
        }
        
        #endregion
        
        #region Radiobutton field Association
        
        /// <summary>
        /// Gets or sets the ID of the associated Radiobutton field.
        /// </summary>
        public Guid? RadiobuttonFieldId { get; set; }
        
        /// <summary>
        /// Gets or sets the value of Radiobutton field on this Main record instance.
        /// </summary>
        public OptionSet2 RadiobuttonField
        {
            get
            {
                return Database.Get<OptionSet2>(this.RadiobuttonFieldId);
            }
            
            set
            {
                this.RadiobuttonFieldId = value.Get(o => o.ID);
            }
        }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Find and returns an instance of Main record from the database by its Text field.
        /// If no matching Main record is found, it returns Null.
        /// </summary>
        /// <param name="textField">The Text field of the requested Main record.</param>
        /// <returns>The Main record instance with the specified Text field or null if there is no Main record with that Text field in the database.</returns>
        public static MainRecord FindByTextField(string textField)
        {
            return Database.Find<MainRecord>(m => m.TextField == textField);
        }
        
        /// <summary>
        /// Returns a textual representation of this Main record.
        /// </summary>
        /// <returns>A string value that represents this Main record instance.</returns>
        public override string ToString()
        {
            return this.RichTextField.Or(string.Empty);
        }
        
        /// <summary>
        /// Returns a clone of this Main record.
        /// </summary>
        /// <returns>
        /// A new Main record object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new MainRecord Clone()
        {
            var clone = (MainRecord) base.Clone();
            
            clone.CheckboxListFieldIds = this.CheckboxListFieldIds.ToList().Clone();
            
            clone.DocumentField = this.DocumentField.Clone();
            return clone;
        }
        
        /// <summary>
        /// Adds a single Option set 1  to the Checkbox list field of this Main record.
        /// </summary>
        /// <param name="item">The instance to add to Checkbox list field of this Main record.</param>
        public virtual void AddToCheckboxListField(OptionSet1 item)
        {
            if (item != null && !this.CheckboxListFieldIds.Contains(item.ID))
            {
                this.CheckboxListFieldIds.Add(item.ID);
            }
        }
        
        /// <summary>
        /// Removes a specified Option set 1 object from the Checkbox list field of this Main record.
        /// </summary>
        /// <param name="item">The instance to remove from Checkbox list field of this Main record.</param>
        public virtual void RemoveFromCheckboxListField(OptionSet1 item)
        {
            if (item != null && this.CheckboxListFieldIds.Contains(item.ID))
            {
                this.CheckboxListFieldIds.Remove(item.ID);
            }
        }
        
        /// <summary>
        /// Validates the data for the properties of this Main record.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate DateField property:
            
            if (this.DateField < DateTime.Parse("01/01/2015"))
                {{
                
                throw new ValidationException("The provided Date field is too early.");
                }}
                
                if (this.DateField > DateTime.Parse("31/12/2015"))
                    {{
                    
                    throw new ValidationException("The provided Date field is too late.");
                    }}
                    
                    // Validate DisabledField property:
                    
                    if (this.DisabledField != null && this.DisabledField.Length > 200)
                    {
                        throw new ValidationException("The provided Disabled field is too long. A maximum of 200 characters is acceptable.");
                    }
                    
                    // Validate DocumentField property:
                    // Ensure the file uploaded for Document field is safe:
                    
                    if (this.DocumentField.HasValue() && this.DocumentField.HasUnsafeExtension())
                    {
                        throw new ValidationException("The file uploaded for Document field is unsafe because of the file extension: {0}", this.DocumentField.FileExtension);
                    }
                    
                    // Validate NumberField property:
                    
                    if (this.NumberField < 1.5m)
                    {
                        throw new ValidationException("The value of Number field must be 1.5 or more.");
                    }
                    
                    if (this.NumberField > 3400.60m)
                    {
                        throw new ValidationException("The value of Number field must be 3400.60 or less.");
                    }
                    
                    // Validate PatternField property:
                    
                    if (this.PatternField != null && this.PatternField.Length > 200)
                    {
                        throw new ValidationException("The provided Pattern field is too long. A maximum of 200 characters is acceptable.");
                    }
                    
                    // Ensure PatternField matches Email address pattern:
                    
                    if (this.PatternField.HasValue() && ! System.Text.RegularExpressions.Regex.IsMatch(this.PatternField, "\\s*\\w+([-+.'\\w])*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\\s*"))
                    {
                        throw new ValidationException("The provided Pattern field is not a valid Email address.");
                    }
                    
                    // Validate RadiobuttonField property:
                    
                    if (this.RadiobuttonFieldId == null)
                    {
                        throw new ValidationException("Please provide a value for Radiobutton field.");
                    }
                    
                    // Validate SliderField property:
                    
                    if (this.SliderField < 0)
                    {
                        throw new ValidationException("The value of Slider field must be 0 or more.");
                    }
                    
                    if (this.SliderField > 100)
                    {
                        throw new ValidationException("The value of Slider field must be 100 or less.");
                    }
                    
                    // Validate TextareaField property:
                    
                    if (this.TextareaField.IsEmpty())
                    {
                        throw new ValidationException("Textarea field cannot be empty.");
                    }
                    
                    // Validate TextField property:
                    
                    if (this.TextField.IsEmpty())
                    {
                        throw new ValidationException("Text field cannot be empty.");
                    }
                    
                    if (this.TextField.Length > 32)
                    {
                        throw new ValidationException("The provided Text field is too long. A maximum of 32 characters is acceptable.");
                    }
                    
                    // Ensure uniqueness of TextField.
                    
                    if (Database.Any<MainRecord>(m => m.TextField == this.TextField && m != this))
                    {
                        throw new ValidationException("Text field must be unique. There is an existing Main record record with the provided Text field.");
                    }
                    
                    // Validate TimeField property:
                    
                    if (this.TimeField < DateTime.Parse("07:00"))
                        {{
                        
                        throw new ValidationException("The provided Time field is too early.");
                        }}
                        
                        if (this.TimeField > DateTime.Parse("22:15"))
                            {{
                            
                            throw new ValidationException("The provided Time field is too late.");
                            }}
                        }
                        
                        /// <summary>
                        /// Handles the Deleting event of this Main record.
                        /// </summary>
                        /// <param name="source">The source of the event.</param>
                        /// <param name="e">The CancelEventArgs instance containing the event data.</param>
                        private void Cascade_Deleting(object source, System.ComponentModel.CancelEventArgs e)
                        {
                            if (this.ChildRecords.Any())
                            {
                                throw new ValidationException("This Main record cannot be deleted because of {0} dependent Child records.", ChildRecords.Count());
                            }
                        }
                    }
                }