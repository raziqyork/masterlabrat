﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    
    /// <summary>
    /// Represents an instance of Option set 3 entity type.
    /// </summary>
    
    [SmallTable]
    public partial class OptionSet3 : GuidEntity, IComparable<OptionSet3>
    {
        /* -------------------------- Constructor -----------------------*/
        
        /// <summary>
        /// Initializes a new instance of the OptionSet3 class.
        /// </summary>
        public OptionSet3()
        {
            this.Deleting += this.Cascade_Deleting;
        }
        
        /* -------------------------- Properties -------------------------*/
        
        #region Name Property
        
        /// <summary>
        /// Gets or sets the value of Name on this Option set 3 instance.
        /// </summary>
        public string Name { get; set; }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Find and returns an instance of Option set 3 from the database by its Name.
        /// If no matching Option set 3 is found, it returns Null.
        /// </summary>
        /// <param name="name">The Name of the requested Option set 3.</param>
        /// <returns>The Option set 3 instance with the specified Name or null if there is no Option set 3 with that Name in the database.</returns>
        public static OptionSet3 FindByName(string name)
        {
            return Database.Find<OptionSet3>(o => o.Name == name);
        }
        
        /// <summary>
        /// Returns a textual representation of this Option set 3.
        /// </summary>
        /// <returns>A string value that represents this Option set 3 instance.</returns>
        public override string ToString()
        {
            return this.Name;
        }
        
        /// <summary>
        /// Returns a clone of this Option set 3.
        /// </summary>
        /// <returns>
        /// A new Option set 3 object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new OptionSet3 Clone()
        {
            return (OptionSet3)base.Clone();
        }
        
        /// <summary>
        /// Compares this Option set 3 with another specified Option set 3 instance.
        /// </summary>
        /// <param name="other">The other Option set 3 to compare this instance to.</param>
        /// <returns>An integer value indicating whether this instance precedes, follows, or appears in the same position as the other Option set 3 in sort orders.</returns>
        public int CompareTo(OptionSet3 other)
        {
            if (other == null)
            {
                return 1;
            }
            else
            {
                return this.Name.CompareTo(other.Name);
            }
        }
        
        /// <summary>
        /// Validates the data for the properties of this Option set 3.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate Name property:
            
            if (this.Name.IsEmpty())
            {
                throw new ValidationException("Name cannot be empty.");
            }
            
            if (this.Name.Length > 200)
            {
                throw new ValidationException("The provided Name is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Ensure uniqueness of Name.
            
            if (Database.Any<OptionSet3>(o => o.Name == this.Name && o != this))
            {
                throw new ValidationException("Name must be unique. There is an existing Option set 3 record with the provided Name.");
            }
        }
        
        /// <summary>
        /// Handles the Deleting event of this Option set 3.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The CancelEventArgs instance containing the event data.</param>
        private void Cascade_Deleting(object source, System.ComponentModel.CancelEventArgs e)
        {
            var dependantChildRecords = Database.GetList<ChildRecord>(c => c.AutocompleteField == this);
            
            if (dependantChildRecords.Any())
            {
                throw new ValidationException("This Option set 3 cannot be deleted because of {0} dependent Child record record(s).", dependantChildRecords.Count());
            }
            
            var dependantSecondaryRecords = Database.GetList<SecondaryRecord>(s => s.DropdownField == this);
            
            if (dependantSecondaryRecords.Any())
            {
                throw new ValidationException("This Option set 3 cannot be deleted because of {0} dependent Secondary record record(s).", dependantSecondaryRecords.Count());
            }
        }
        
        /// <summary>
        /// Gets a string to uniquely identify instances of this type with.
        /// </summary>
        public virtual string GetUniqueTestInstanceName()
        {
            return this.ID.ToString();
        }
    }
}