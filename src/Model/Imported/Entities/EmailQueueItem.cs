﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    
    /// <summary>
    /// Represents an instance of Email queue item entity type.
    /// </summary>
    
    [SoftDelete]
    public partial class EmailQueueItem : GuidEntity
    {
        /* -------------------------- Constructor -----------------------*/
        
        /// <summary>
        /// Initializes a new instance of the EmailQueueItem class.
        /// </summary>
        public EmailQueueItem()
        {
            this.Date = LocalTime.Now;
        }
        
        /* -------------------------- Properties -------------------------*/
        
        #region Attachments Property
        
        /// <summary>
        /// Gets or sets the value of Attachments on this Email queue item instance.
        /// </summary>
        public string Attachments { get; set; }
        
        #endregion
        
        #region Bcc Property
        
        /// <summary>
        /// Gets or sets the value of Bcc on this Email queue item instance.
        /// </summary>
        public string Bcc { get; set; }
        
        #endregion
        
        #region Body Property
        
        /// <summary>
        /// Gets or sets the value of Body on this Email queue item instance.
        /// </summary>
        public string Body { get; set; }
        
        #endregion
        
        #region Category Property
        
        /// <summary>
        /// Gets or sets the value of Category on this Email queue item instance.
        /// </summary>
        public string Category { get; set; }
        
        #endregion
        
        #region Cc Property
        
        /// <summary>
        /// Gets or sets the value of Cc on this Email queue item instance.
        /// </summary>
        public string Cc { get; set; }
        
        #endregion
        
        #region Date Property
        
        /// <summary>
        /// Gets or sets the value of Date on this Email queue item instance.
        /// </summary>
        public DateTime Date { get; set; }
        
        #endregion
        
        #region Enable ssl Property
        
        /// <summary>
        /// Gets or sets a value indicating whether this Email queue item instance Enable ssl.
        /// </summary>
        public bool EnableSsl { get; set; }
        
        #endregion
        
        #region Html Property
        
        /// <summary>
        /// Gets or sets a value indicating whether this Email queue item instance is Html.
        /// </summary>
        public bool Html { get; set; }
        
        #endregion
        
        #region Password Property
        
        /// <summary>
        /// Gets or sets the value of Password on this Email queue item instance.
        /// </summary>
        public string Password { get; set; }
        
        #endregion
        
        #region Retries Property
        
        /// <summary>
        /// Gets or sets the value of Retries on this Email queue item instance.
        /// </summary>
        public int Retries { get; set; }
        
        #endregion
        
        #region Sender address Property
        
        /// <summary>
        /// Gets or sets the value of SenderAddress on this Email queue item instance.
        /// </summary>
        public string SenderAddress { get; set; }
        
        #endregion
        
        #region Sender name Property
        
        /// <summary>
        /// Gets or sets the value of SenderName on this Email queue item instance.
        /// </summary>
        public string SenderName { get; set; }
        
        #endregion
        
        #region Smtp host Property
        
        /// <summary>
        /// Gets or sets the value of SmtpHost on this Email queue item instance.
        /// </summary>
        public string SmtpHost { get; set; }
        
        #endregion
        
        #region Smtp port Property
        
        /// <summary>
        /// Gets or sets the value of SmtpPort on this Email queue item instance.
        /// </summary>
        public int? SmtpPort { get; set; }
        
        #endregion
        
        #region Subject Property
        
        /// <summary>
        /// Gets or sets the value of Subject on this Email queue item instance.
        /// </summary>
        public string Subject { get; set; }
        
        #endregion
        
        #region To Property
        
        /// <summary>
        /// Gets or sets the value of To on this Email queue item instance.
        /// </summary>
        public string To { get; set; }
        
        #endregion
        
        #region Username Property
        
        /// <summary>
        /// Gets or sets the value of Username on this Email queue item instance.
        /// </summary>
        public string Username { get; set; }
        
        #endregion
        
        #region VCalendar view Property
        
        /// <summary>
        /// Gets or sets the value of VCalendarView on this Email queue item instance.
        /// </summary>
        public string VCalendarView { get; set; }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Returns a textual representation of this Email queue item.
        /// </summary>
        /// <returns>A string value that represents this Email queue item instance.</returns>
        public override string ToString()
        {
            return this.Subject;
        }
        
        /// <summary>
        /// Returns a clone of this Email queue item.
        /// </summary>
        /// <returns>
        /// A new Email queue item object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new EmailQueueItem Clone()
        {
            return (EmailQueueItem)base.Clone();
        }
        
        /// <summary>
        /// Validates the data for the properties of this Email queue item.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate Attachments property:
            
            if (this.Attachments != null && this.Attachments.Length > 200)
            {
                throw new ValidationException("The provided Attachments is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate Bcc property:
            
            if (this.Bcc != null && this.Bcc.Length > 200)
            {
                throw new ValidationException("The provided Bcc is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate Category property:
            
            if (this.Category != null && this.Category.Length > 200)
            {
                throw new ValidationException("The provided Category is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate Cc property:
            
            if (this.Cc != null && this.Cc.Length > 200)
            {
                throw new ValidationException("The provided Cc is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate Password property:
            
            if (this.Password != null && this.Password.Length > 200)
            {
                throw new ValidationException("The provided Password is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate Retries property:
            
            if (this.Retries < 0)
            {
                throw new ValidationException("The value of Retries must be 0 or more.");
            }
            
            // Validate SenderAddress property:
            
            if (this.SenderAddress != null && this.SenderAddress.Length > 200)
            {
                throw new ValidationException("The provided Sender address is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate SenderName property:
            
            if (this.SenderName != null && this.SenderName.Length > 200)
            {
                throw new ValidationException("The provided Sender name is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate SmtpHost property:
            
            if (this.SmtpHost != null && this.SmtpHost.Length > 200)
            {
                throw new ValidationException("The provided Smtp host is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate SmtpPort property:
            
            if (this.SmtpPort < 0)
            {
                throw new ValidationException("The value of Smtp port must be 0 or more.");
            }
            
            // Validate Subject property:
            
            if (this.Subject.IsEmpty())
            {
                throw new ValidationException("Subject cannot be empty.");
            }
            
            if (this.Subject.Length > 200)
            {
                throw new ValidationException("The provided Subject is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate To property:
            
            if (this.To != null && this.To.Length > 200)
            {
                throw new ValidationException("The provided To is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate Username property:
            
            if (this.Username != null && this.Username.Length > 200)
            {
                throw new ValidationException("The provided Username is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Validate VCalendarView property:
            
            if (this.VCalendarView != null && this.VCalendarView.Length > 200)
            {
                throw new ValidationException("The provided VCalendar view is too long. A maximum of 200 characters is acceptable.");
            }
        }
    }
}