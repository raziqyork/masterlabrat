﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    
    /// <summary>
    /// Represents an instance of User entity type.
    /// </summary>
    
    public partial class User : GuidEntity
    {
        /* -------------------------- Properties -------------------------*/
        
        #region Email Property
        
        /// <summary>
        /// Gets or sets the value of Email on this User instance.
        /// </summary>
        public string Email { get; set; }
        
        #endregion
        
        #region Password Property
        
        /// <summary>
        /// Gets or sets the value of Password on this User instance.
        /// </summary>
        public string Password { get; set; }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Find and returns an instance of User from the database by its Email.
        /// If no matching User is found, it returns Null.
        /// </summary>
        /// <param name="email">The Email of the requested User.</param>
        /// <returns>The User instance with the specified Email or null if there is no User with that Email in the database.</returns>
        public static User FindByEmail(string email)
        {
            return Database.Find<User>(u => u.Email == email);
        }
        
        /// <summary>
        /// Returns a textual representation of this User.
        /// </summary>
        /// <returns>A string value that represents this User instance.</returns>
        public override string ToString()
        {
            return this.Email;
        }
        
        /// <summary>
        /// Returns a clone of this User.
        /// </summary>
        /// <returns>
        /// A new User object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new User Clone()
        {
            return (User)base.Clone();
        }
        
        /// <summary>
        /// Validates the data for the properties of this User.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate Email property:
            
            if (this.Email.IsEmpty())
            {
                throw new ValidationException("Email cannot be empty.");
            }
            
            if (this.Email.Length > 100)
            {
                throw new ValidationException("The provided Email is too long. A maximum of 100 characters is acceptable.");
            }
            
            // Ensure Email matches Email address pattern:
            
            if (! System.Text.RegularExpressions.Regex.IsMatch(this.Email, "\\s*\\w+([-+.'\\w])*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\\s*"))
            {
                throw new ValidationException("The provided Email is not a valid Email address.");
            }
            
            // Ensure uniqueness of Email.
            
            if (Database.Any<User>(u => u.Email == this.Email && u != this))
            {
                throw new ValidationException("Email must be unique. There is an existing User record with the provided Email.");
            }
            
            // Validate Password property:
            
            if (this.Password.IsEmpty())
            {
                throw new ValidationException("Password cannot be empty.");
            }
            
            if (this.Password.Length > 100)
            {
                throw new ValidationException("The provided Password is too long. A maximum of 100 characters is acceptable.");
            }
        }
    }
}