﻿namespace App
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using MSharp.Framework;
    using MSharp.Framework.Data;
    
    /// <summary>
    /// Represents an instance of Secondary record entity type.
    /// </summary>
    
    public partial class SecondaryRecord : GuidEntity
    {
        /* -------------------------- Properties -------------------------*/
        
        #region Text field Property
        
        /// <summary>
        /// Gets or sets the value of TextField on this Secondary record instance.
        /// </summary>
        public string TextField { get; set; }
        
        #endregion
        
        #region Dropdown field Association
        
        /// <summary>
        /// Gets or sets the ID of the associated Dropdown field.
        /// </summary>
        public Guid? DropdownFieldId { get; set; }
        
        /// <summary>
        /// Gets or sets the value of Dropdown field on this Secondary record instance.
        /// </summary>
        public OptionSet3 DropdownField
        {
            get
            {
                return Database.Get<OptionSet3>(this.DropdownFieldId);
            }
            
            set
            {
                this.DropdownFieldId = value.Get(o => o.ID);
            }
        }
        
        #endregion
        
        #region Tertiary record Association
        
        /// <summary>
        /// Gets or sets the ID of the associated Tertiary record.
        /// </summary>
        public Guid? TertiaryRecordId { get; set; }
        
        /// <summary>
        /// Gets or sets the value of Tertiary record on this Secondary record instance.
        /// </summary>
        public TertiaryRecord TertiaryRecord
        {
            get
            {
                return Database.Get<TertiaryRecord>(this.TertiaryRecordId);
            }
            
            set
            {
                this.TertiaryRecordId = value.Get(t => t.ID);
            }
        }
        
        #endregion
        
        /* -------------------------- Methods ----------------------------*/
        
        /// <summary>
        /// Find and returns an instance of Secondary record from the database by its Text field.
        /// If no matching Secondary record is found, it returns Null.
        /// </summary>
        /// <param name="textField">The Text field of the requested Secondary record.</param>
        /// <returns>The Secondary record instance with the specified Text field or null if there is no Secondary record with that Text field in the database.</returns>
        public static SecondaryRecord FindByTextField(string textField)
        {
            return Database.Find<SecondaryRecord>(s => s.TextField == textField);
        }
        
        /// <summary>
        /// Returns a textual representation of this Secondary record.
        /// </summary>
        /// <returns>A string value that represents this Secondary record instance.</returns>
        public override string ToString()
        {
            return this.TextField;
        }
        
        /// <summary>
        /// Returns a clone of this Secondary record.
        /// </summary>
        /// <returns>
        /// A new Secondary record object with the same ID of this instance and identical property values.
        /// The difference is that this instance will be unlocked, and thus can be used for updating in database.
        /// </returns>
        public new SecondaryRecord Clone()
        {
            return (SecondaryRecord)base.Clone();
        }
        
        /// <summary>
        /// Validates the data for the properties of this Secondary record.
        /// It throws a ValidationException if an error is detected.
        /// </summary>
        protected override void ValidateProperties()
        {
            // Validate TertiaryRecord property:
            
            if (this.TertiaryRecordId == null)
            {
                throw new ValidationException("Please provide a value for Tertiary record.");
            }
            
            // Validate TextField property:
            
            if (this.TextField.IsEmpty())
            {
                throw new ValidationException("Text field cannot be empty.");
            }
            
            if (this.TextField.Length > 200)
            {
                throw new ValidationException("The provided Text field is too long. A maximum of 200 characters is acceptable.");
            }
            
            // Ensure uniqueness of TextField.
            
            if (Database.Any<SecondaryRecord>(s => s.TextField == this.TextField && s != this))
            {
                throw new ValidationException("Text field must be unique. There is an existing Secondary record record with the provided Text field.");
            }
        }
    }
}