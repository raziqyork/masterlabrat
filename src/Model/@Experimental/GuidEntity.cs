﻿namespace App
{
    public abstract class GuidEntity : MSharp.Framework.GuidEntity
    {
        public void MergeChangedPropertiesFrom(GuidEntity other)
        {
        }

        ///// <summary>
        ///// Clones this instance.
        ///// </summary>
        //protected override Entity Clone()
        //{
        //    var clone = (GuidEntity)base.Clone();
        //    clone.Initialize();
        //    clone.OnCloned(this);
        //    return clone;
        //}

        ///// <summary>
        ///// Called when this instance has been cloned.
        ///// </summary>
        ///// <param name="original">The original instance that was cloned.</param>
        //protected virtual void OnCloned(GuidEntity original)
        //{
        //}
    }
}