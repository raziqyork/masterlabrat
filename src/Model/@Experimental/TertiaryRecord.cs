﻿namespace App
{
    using System;
    using System.ComponentModel.DataAnnotations;

    partial class TertiaryRecord
    {
        public void MergeChangedPropertiesFrom(TertiaryRecord other)
        {
            base.MergeChangedPropertiesFrom(other);

            if (other.DateField != default(DateTime))
                this.DateField = other.DateField;

            if (other.TextField != default(string))
                this.TextField = other.TextField;

            if (!other.OptionFieldId.HasValue)
                this.OptionFieldId = other.OptionFieldId;
        }

        [Required]
        [Display(Name = "Item title")]
        public string Title { get { return TextField; } set { TextField = value; } }

        [Required]
        [Display(Name = "Item date")]
        public DateTime Date { get { return DateField; } set { DateField = value; } }

        [Display(Name = "Is currently active?")]
        public bool IsActive
        {
            get
            {
                var a = OptionField.IsAnyOf(OptionSet1.OptionC, OptionSet1.OptionD);
                var b = (DateField.Year % 3) == 0;
                return a ^ b;
            }
        }
    }
}