﻿namespace App
{
    using MSharp.Framework;
    using System;
    using System.Dynamic;


    public class _TertiaryRecord : TertiaryRecord
    {
    }

    public class BetterEntity : SmartEntity
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }

    public abstract class SmartEntity : DynamicObject
    {
        public Guid Id { get; set; }

        public SmartEntity()
        {
        }
    
    }
}