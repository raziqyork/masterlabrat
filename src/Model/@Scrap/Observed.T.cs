﻿namespace App
{
    using System;

    public class Observed<T> where T : IEquatable<T>
    {
        private T value;
        public readonly bool IsSet;

        public Observed(T value)
        {
            this.value = value;
            //IsSet = (value != default(T));
            //Object.Equals();
        }

        public static implicit operator T(Observed<T> observer)
        {
            return observer == null ? default(T) : observer.value;
            //return observer?.value ?? default(T);
        }

        public static implicit operator Observed<T>(T value)
        {
            return new Observed<T>(value);
        }
    }
}