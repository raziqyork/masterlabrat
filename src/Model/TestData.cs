﻿namespace App
{
    using System;
    using System.Linq;
    using MSharp.Framework;
    using Ploeh.AutoFixture;

    public static class TestData
    {
        public static void InitialiseDatabaseWithTestData()
        {
            AddTertiaryRecords();
        }

        public static void AddTertiaryRecords()
        {
            var rng = new Random();
            var fixture = new Fixture();

            var options = Database.GetList<OptionSet1>().ToList();
            fixture.Register<OptionSet1>(() => options[rng.Next(options.Count)]);

            var records = fixture.Build<TertiaryRecord>()
                .Without(r => r.OptionFieldId)
                .CreateMany(200);

            Database.DeleteAll<TertiaryRecord>();
            Database.Save(records);
        }
    }
}