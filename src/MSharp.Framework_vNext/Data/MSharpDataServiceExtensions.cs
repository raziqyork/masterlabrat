﻿namespace MSharp.Framework.Data
{
	using Microsoft.Framework.ConfigurationModel;
	using Microsoft.Framework.DependencyInjection;

	public static class MSharpDataServiceExtensions
	{
		public static MSharpDataService AddMSharpData(this IServiceCollection services, IConfiguration configuration)
		{
			var dataService = new MSharpDataService(configuration);
			services.AddInstance(dataService);
			return dataService;
		}
	}
}