namespace MSharp.Framework.Data
{
	using System;
	using System.Collections.Generic;
	using System.Reflection;
	using Microsoft.Framework.ConfigurationModel;

	public class MSharpDataService
	{
		private static readonly Dictionary<Assembly, IDataProviderFactory> AssemblyProviderFactories = (Dictionary<Assembly, IDataProviderFactory>)typeof(Database).GetField("AssemblyProviderFactories", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
		private readonly IConfiguration configuration;

		public MSharpDataService(IConfiguration configuration)
		{
			this.configuration = configuration;
		}

		/// <summary>
		/// Registers a concrete IDataProviderFactory implementation.
		/// </summary>
		public MSharpDataService AddDataProvider<T>(string connectionStringKey = null) where T : IDataProviderFactory
		{
			var factoryType = typeof(T);

			if (connectionStringKey == null)
			{
				connectionStringKey = configuration.Get($"MSharp:Data:{factoryType.AssemblyQualifiedName}:ConnectionStringKey");
			}

			var config = new DataProviderFactoryInfo
			{
				Assembly = factoryType.Assembly,
				AssemblyName = factoryType.Assembly.FullName,
				ConnectionStringKey = connectionStringKey,
				ConnectionString = configuration.Get(connectionStringKey),
				ProviderFactoryType = factoryType.FullName,
			};

			var factory = (IDataProviderFactory)Activator.CreateInstance(factoryType, config);
			AssemblyProviderFactories[factoryType.Assembly] = factory;

			return this;
		}
	}
}