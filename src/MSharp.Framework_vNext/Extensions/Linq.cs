﻿namespace System.Linq
{
    using Collections.Generic;

    public static partial class SystemExtensions
    {
        public static IEnumerable<WithIndexWrapper<T>> WithIndex<T>(this IEnumerable<T> items, int startingIndex = 0)
        {
            if (items == null)
                throw new NullReferenceException();

            int i = startingIndex;
            foreach (var item in items)
            {
                yield return new WithIndexWrapper<T>(i, item);
                i++;
            }
        }

        public class WithIndexWrapper<T>
        {
            public int i => Index; // alias for brevity
            public int Index { get; }
            public T Item { get; }

            internal WithIndexWrapper(int index, T item)
            {
                Index = index;
                Item = item;
            }
        }
        /// <summary>
        /// Returns an IEnumerable containing only this value or object.
        /// </summary>
        /// <param name="item">
        /// The value.
        /// </param>
        /// <exception cref="System.NullReferenceException">
        /// Item is null
        /// </exception>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<T> ToEnumerable<T>(this T item)
        {
            if (Equals(item, null))
            {
                throw new NullReferenceException();
            }

            yield return item;
        }

        /// <summary>
        /// Returns an IEnumerable containing only this value or object.
        ///     Returns an Empty IEnumerable if this is null.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public static IEnumerable<T> ToEnumerableOrEmpty<T>(this T item)
        {
            if (!Equals(item, null))
            {
                yield return item;
            }
        }
    }
}
