﻿namespace System.Dynamic
{
	using System.Collections.Generic;

	public static class DynamicExtensions
	{
		public static ExpandoObject ToDynamic(this object obj)
		{
			return new ExpandoObject().Extend(obj);
		}

		public static ExpandoObject Extend(this ExpandoObject expando, object obj, params object[] objs)
		{
			IDictionary<string, object> dictionary = expando;

			var e = objs.GetEnumerator();
			var o = obj;
			do
			{
				foreach (var property in o.GetType().GetProperties())
					dictionary.Add(property.Name, property.GetValue(o));

				o = e.MoveNext() ? e.Current : null;
			}
			while (o != null);

			return expando;
		}
	}
}