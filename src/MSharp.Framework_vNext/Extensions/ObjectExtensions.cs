﻿namespace Microsoft.AspNet.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    public static class ObjectExtensions
	{
		public static IDictionary<string, object> ToPropertyDictionary(this object obj)
		{
			var properties = obj.GetType().GetProperties()
				.Where(p => p.GetIndexParameters().None())
				.ToDictionary(p => p.Name, p => p.GetValue(obj));

			return new ReadOnlyDictionary<string, object>(properties);
		}
	}
}