﻿namespace System
{
    public static partial class SystemExtensions
    {
        public static string If(this string text, bool predecate) => predecate ? text : null;

        public static object TryParse<T0, T1, T2>(this string text, bool orText = false)
            where T0 : struct
            where T1 : struct
            where T2 : struct
        {
            var val0 = text.TryParse<T0>();
            if (val0.HasValue) return val0.Value;

            var val1 = text.TryParse<T1>();
            if (val1.HasValue) return val1.Value;

            var val2 = text.TryParse<T2>();
            if (val2.HasValue) return val2.Value;

            return orText ? text : null;
        }

        public static object TryParse<T0, T1>(this string text, bool orText = false)
            where T0 : struct
            where T1 : struct
        {
            var val0 = text.TryParse<T0>();
            if (val0.HasValue) return val0.Value;

            var val1 = text.TryParse<T1>();
            if (val1.HasValue) return val1.Value;

            return orText ? text : null;
        }

        public static T? TryParse<T>(this string text) where T : struct
        {
            object value = null;
            if (typeof(T) == typeof(DateTime))
            {
                DateTime val;
                if (DateTime.TryParse(text, out val))
                    value = new DateTime?(val);
            }
            else if (typeof(T) == typeof(TimeSpan))
            {
                TimeSpan val;
                if (TimeSpan.TryParse(text, out val))
                    value = new TimeSpan?(val);
            }
            else if (typeof(T) == typeof(Guid))
            {
                Guid val;
                if (Guid.TryParse(text, out val))
                    value = new Guid?(val);
            }
            else if (typeof(T) == typeof(long))
            {
                long val;
                if (Int64.TryParse(text, out val))
                    value = new long?(val);
            }
            else if (typeof(T) == typeof(int))
            {
                int val;
                if (Int32.TryParse(text, out val))
                    value = new int?(val);
            }
            else if (typeof(T) == typeof(decimal))
            {
                decimal val;
                if (Decimal.TryParse(text, out val))
                    value = new decimal?(val);
            }
            else if (typeof(T) == typeof(double))
            {
                double val;
                if (Double.TryParse(text, out val))
                    value = new double?(val);
            }
            else if (typeof(T) == typeof(bool))
            {
                bool val;
                if (Boolean.TryParse(text, out val))
                    value = new bool?(val);
            }
            else
            {
                throw new NotSupportedException($"Parsing {typeof(T)} is not supported.");
            }

            return value != null ? (T?)value : null;
        }
    }
}
