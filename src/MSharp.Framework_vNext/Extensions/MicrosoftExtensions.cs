﻿using System;

namespace Microsoft.Framework.ConfigurationModel
{
    public static class ConfigurationModelExtensions
    {
        public static object GetValue(this IConfiguration configuration, string key)
        {
            var valueString = configuration.Get(key);
            var valueType = configuration.GetSubKey(key).GetType();

            var typeCode = Type.GetTypeCode(valueType);
            var obj = Convert.ChangeType(valueString, valueType);
            return obj;
        }
    }
}