﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.OptionDescriptors;
    using Microsoft.Framework.OptionsModel;
    using System;
    using System.Collections.Generic;

    public class ElementRolesServiceDescriptor : OptionDescriptor<IElementRolesService>
    {
        public ElementRolesServiceDescriptor(Type type) : base(type) { }
        public ElementRolesServiceDescriptor(IElementRolesService elementRolesService) : base(elementRolesService) { }
    }

    public class ElementRolesServiceProvider : OptionDescriptorBasedProvider<IElementRolesService>
    {
        public ElementRolesServiceProvider(
            IOptions<MSharpOptions> optionsAccessor,
            ITypeActivatorCache typeActivatorCache,
            IServiceProvider serviceProvider)
            : base(optionsAccessor.Options.ElementRolesServices, typeActivatorCache, serviceProvider)
        {
        }

        public new IEnumerable<IElementRolesService> Options => base.Options;
    }
}