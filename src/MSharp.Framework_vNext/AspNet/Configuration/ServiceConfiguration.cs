﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.Razor;
    using Microsoft.Framework.ConfigurationModel;
    using Microsoft.Framework.DependencyInjection;
    using Microsoft.Framework.OptionsModel;
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public static class ServiceConfiguration
    {
        public static IServiceCollection AddMSharpWeb(this IServiceCollection services, IConfiguration configuration)
        {
            //if (services.Any(s => s.ServiceType is Microsoft.Framework.DependencyInjection.ServiceDescriptor))
            //    throw new InvalidOperationException("Must invoke AddMvc() before AddMSharpWebConventions().");

            services.TryAdd(ConfigureMSharpOptions(configuration));

            BindDependencies(services);

            ConfigureFramework(services, configuration);

            // Ensure that Website project assembly is available for reflection
            var appInfo = new ApplicationInfo(applicationAssembly: Assembly.GetCallingAssembly());
            services.AddSingleton(p => appInfo);

            return services;
        }

        private static IEnumerable<ServiceDescriptor> ConfigureMSharpOptions(IConfiguration configuration)
        {
            var describe = new ServiceDescriber(configuration);

            // Register options and core services.
            yield return describe.Transient<IConfigureOptions<MSharpOptions>, MSharpOptionsSetup>();

            // Register options descriptor providers
            yield return describe.Transient<ElementRolesServiceProvider, ElementRolesServiceProvider>();
        }

        private static void BindDependencies(IServiceCollection services)
        {
            // Register MSharp.Web dependencies
            services.AddSingleton<IWebParametiser, DefaultWebParametiser>();
            services.AddSingleton<IActivePathDetector, DefaultActivePathDetector>();
            services.AddSingleton<IElementAttributeHelper, DefaultElementAttributeHelper>();
            //services.AddSingleton<IElementRolesService, DefaultElementRolesService>(); now done using options
            services.AddSingleton<ParamHelperService>();

            services.AddScoped<IElementIdentityGenerator, DefaultElementIdentityGenerator>();
            services.AddScoped<LinkHelperService>();
            services.AddScoped<EasyBinder>();

            services.AddTransient<IRouteHelper, DefaultRouteHelper>();
            services.AddTransient<LinkInfo>();
        }

        private static void ConfigureFramework(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<MvcOptions>(options =>
            {
                // Ensures that M# multi-level page nesting is supported
                options.ViewEngines.Clear();
                options.ViewEngines.Add(typeof(MSharpRazorViewEngine));

                //options.ValueProviderFactories.Clear();
                //options.ValueProviderFactories.Add(new RouteValueValueProviderFactory());
                //options.ValueProviderFactories.Add(new QueryStringValueProviderFactory());
                //options.ValueProviderFactories.Add(new EnhancedFormValueProviderFactory());

                //var factory = options.ValueProviderFactories.FirstOrDefault(f => f.OptionType == typeof(FormValueProviderFactory));
                //if (factory != null) options.ValueProviderFactories.Remove(factory);
                //options.ValueProviderFactories.Add(new App.EnhancedFormValueProviderFactory());

            });

            services.Configure<RazorViewEngineOptions>(options =>
            {
                // Ensures that M# module view locations are supported
                options.ViewLocationExpanders.Add(new ModuleViewLocationExpander(configuration));
            });
        }
    }
}