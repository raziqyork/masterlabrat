﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.Framework.OptionsModel;

    public class MSharpOptionsSetup : ConfigureOptions<MSharpOptions>
    {
        public MSharpOptionsSetup() : base(ConfigureDefaultOptions)
        {
            Order = 1; /*User code should order at bigger than 0 or smaller than -2000.*/
        }

        public static void ConfigureDefaultOptions(MSharpOptions options)
        {
            options.ElementRolesServices.AddType(typeof(DefaultElementRolesService));
        }
    }
}