﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System.Reflection;

    public class ApplicationInfo
    {
        public Assembly ApplicationAssembly { get; }

        public ApplicationInfo(Assembly applicationAssembly)
        {
            ApplicationAssembly = applicationAssembly;
        }
    }
}