﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System.Collections.Generic;

    public class MSharpOptions
    {
        public IList<ElementRolesServiceDescriptor> ElementRolesServices { get; }

        public MSharpOptions()
        {
            ElementRolesServices = new List<ElementRolesServiceDescriptor>();
        }
    }
}