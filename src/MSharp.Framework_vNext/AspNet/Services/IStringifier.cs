﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;

    public interface IStringifier
    {
        Type TargetType { get; }
        string ToString(object obj);
        object FromString(string text, object obj = null);
    }
}