﻿namespace MSharp.Framework.AspNet.Mvc
{
	public interface IParamHelper
	{
		IParamHelper Frag(string key);

		IParamHelper Frag(string key, object value);

		IParamHelper Qry(string key, object value);
	}
}