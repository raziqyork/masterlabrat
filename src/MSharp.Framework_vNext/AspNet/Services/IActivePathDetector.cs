﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Http;

    public interface IActivePathDetector
    {
        bool CalculateIsActive(HttpRequest currentRequest, LinkInfo linkInfo, params object[] arguments);
    }
}