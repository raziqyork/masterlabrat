﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;

    public class LinkHelperService
    {
        private readonly IServiceProvider services;

        public LinkHelperService(IServiceProvider services)
        {
            this.services = services;
        }

        public LinkInfo To(string href, object parameters = null)
        {
            var link = services.GetService(typeof(LinkInfo)) as LinkInfo;

            link.SetRouteOrUrl(href);

            link.Parameters.SetParams(parameters);

            return link;
        }

        public LinkInfo To(string action, string controller, string area = null, object routeValues = null)
        {
            var link = services.GetService(typeof(LinkInfo)) as LinkInfo;

            link.SetRouteOrUrl(action, controller, area, routeValues);

            return link;
        }

        //[Obsolete("This looks horrible when used.", true)]
        //public LinkInfo To(string action, string controller, string area = null, params object[] routeValuePairs)
        //{
        //    var link = To(action, controller, area, routeValues: null);

        //    for (int i = 0; i < routeValuePairs.Length; i += 2)
        //    {
        //        var key = (string)routeValuePairs[i];
        //        var value = routeValuePairs.ElementAtOrDefault(i + 1);
        //        link.Parameters.SetParam(key, value);
        //    }

        //    return link;
        //}

        //public GoInfoScope Scope()
        //{
        //    return new GoInfoScope(this);
        //}
    }
}