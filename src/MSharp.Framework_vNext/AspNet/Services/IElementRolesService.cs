﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IElementRolesService
    {
        Task<IEnumerable<ElementRole>> GetElementRolesAsync(IEnumerable<string> names);
    }
}