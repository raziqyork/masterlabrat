﻿namespace MSharp.Framework.AspNet.Mvc
{
    public interface IElementAttributeHelper
    {
        string MergeAttributeValues(string attribute, string existingValue, string newValue);
    }
}