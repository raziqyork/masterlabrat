﻿namespace MSharp.Framework.AspNet.Mvc
{
	using System.Collections.Generic;

	public interface IWebParametiser
	{
		IDictionary<string, object> SplitQueryString(string queryString);
		IDictionary<string, object> SplitFragment(string fragment);
		string JoinQueryString(IDictionary<string, object> pairs);
		string JoinFragments(IDictionary<string, object> pairs);
		bool QueryStringKeysCaseSensitive { get; }
		string QueryStringPrefix { get; }
		string QueryStringPairDelim { get; }
		string QueryStringKeyValueDelim { get; }
		string FragmentPrefix { get; }
		bool FragmentKeysCaseSensitive { get; }
		string FragmentPairDelim { get; }
		string FragmentKeyValueDelim { get; }
	}
}