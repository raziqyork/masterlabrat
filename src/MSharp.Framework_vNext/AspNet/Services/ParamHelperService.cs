﻿namespace MSharp.Framework.AspNet.Mvc
{
	public class ParamHelperService : IParamHelper
	{
		public IParamHelper Frag(string key)
		{
			return (new ParamSet() as IParamHelper).Frag(key);
		}

		public IParamHelper Frag(string key, object value)
		{
			return (new ParamSet() as IParamHelper).Frag(key, value);
		}

		public IParamHelper Qry(string key, object value)
		{
			return (new ParamSet() as IParamHelper).Qry(key, value);
		}
	}
}