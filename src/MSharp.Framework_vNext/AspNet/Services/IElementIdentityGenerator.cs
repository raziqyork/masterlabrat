﻿namespace MSharp.Framework.AspNet.Mvc
{
    public interface IElementIdentityGenerator
    {
        string NextModuleId();

        /// <summary>
        /// This returns a new module Id where a module has been added to the control tree at postback.
        /// It ensures no duplicate module Id is assigned at postback.
        /// </summary>
        string NextSafeModuleId();
    }
}