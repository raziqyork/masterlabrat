namespace MSharp.Framework.AspNet.HtmlHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNet.Mvc.Rendering;

    public class CssClassList
    {
        private readonly List<string> classes = new List<string>();

        public CssClassList(params string[] cssClasses)
        {
            Add(cssClasses);
        }

        public CssClassList Add(params string[] cssClasses)
        {
            if (cssClasses == null)
                throw new ArgumentNullException(nameof(cssClasses));

            foreach (var css in cssClasses)
            {
                foreach (var sub in (css ?? "").Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (!classes.Contains(sub))
                    {
                        classes.Add(sub);
                    }
                }
            }

            return this;
        }

        public CssClassList AddIf(bool predecate, params string[] cssClasses)
        {
            if (predecate)
                Add(cssClasses);

            return this;
        }

        public CssClassList Toggle(bool predecate, string trueCssClass, string falseCssClass)
        {
            Add(predecate ? trueCssClass : falseCssClass);

            return this;
        }

        /// <remarks>
        /// Reason: Avoids empty class attributes by rendering nothing if no css classes are defined.
        /// New: Depricate asap, replace with tag helper to drop empty attributes
        /// </remarks>
        public HtmlString Attr
        {
            get
            {
                return new HtmlString(classes.Any() ? $"class=\"{this}\"" : String.Empty);
            }
        }

        /// <remarks>
        /// In theory this should really return an HtmlString
        /// </remarks>
        public override string ToString()
        {
            return String.Join(" ", classes);
        }
    }
}