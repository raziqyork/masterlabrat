﻿namespace Microsoft.AspNet.Mvc.Rendering
{
    using MSharp.Framework.AspNet.HtmlHelpers;

    public static partial class HtmlExtensions
    {
        public static CssClassList Css(this IHtmlHelper html, params string[] cssClasses)
        {
            return new CssClassList(cssClasses);
        }

        public static CssClassList CssIf(this IHtmlHelper html, bool predecate, params string[] cssClasses)
        {
            return new CssClassList().AddIf(predecate, cssClasses);
        }

        public static CssClassList CssToggle(this IHtmlHelper html, bool predecate, string trueCssClass, string falseCssClass)
        {
            return new CssClassList().Toggle(predecate, trueCssClass, falseCssClass);
        }
    }
}