﻿namespace MSharp.Framework.AspNet.TagHelpers
{
    using Microsoft.AspNet.Razor.Runtime.TagHelpers;
    using System;
    using System.Linq;

    [HtmlElementName("div", "table", "tr", "td", "th", "button", "input", "a", "ul", "li", "i", "span")]
    public class EmptyAttributeTagHelper : TagHelper
    {
        public override int Order => 9999;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);

            foreach (var key in output.Attributes.Keys.ToList())
            {
                var value = output.Attributes[key];
                if (value == null || value == String.Empty)
                {
                    output.Attributes.Remove(key);
                }
            }
        }
    }
}