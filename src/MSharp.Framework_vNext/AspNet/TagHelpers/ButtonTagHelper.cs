﻿namespace MSharp.Framework.AspNet.TagHelpers
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.Rendering;
    using Microsoft.AspNet.Mvc.TagHelpers;
    using Microsoft.AspNet.Razor.Runtime.TagHelpers;
    using MSharp.Framework.AspNet.Mvc;
    using System;

    [HtmlElementName("button")]
    public class ButtonTagHelper : TagHelper
    {
        private const string DefaultCommandNamePrefix = "module";
        private const string CommandNamePart = "Command.Stringified";
        private const string CommandAttributeName = "asp-command";
        private const string CommandRefAttributeName = "asp-command-ref";
        private const string CommandIndexAttributeName = "asp-command-index";
        private const string CommandArgsAttributeName = "asp-command-args";
        private const string BindingPrefixAttributeName = "asp-prefix";

        [Activate]
        protected internal IHtmlGenerator Generator { get; set; }

        [Activate]
        protected internal ViewContext ViewContext { get; set; }

        [HtmlAttributeName(BindingPrefixAttributeName)]
        public string BindingPrefix { get; set; }

        [HtmlAttributeName(CommandAttributeName)]
        public string CommandName { get; set; }

        [HtmlAttributeName(CommandRefAttributeName)]
        public string CommandRef { get; set; }

        [HtmlAttributeName(CommandIndexAttributeName)]
        public int? CommandIndex { get; set; }

        [HtmlAttributeName(CommandArgsAttributeName)]
        public string CommandArgs { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var fullName = (String.IsNullOrEmpty(BindingPrefix)
                ? DefaultCommandNamePrefix
                : BindingPrefix)
                + "." + CommandNamePart;

            var info = new CommandInfo
            {
                Name = CommandName,
                Id = CommandRef,
                RowIndex = CommandIndex,
                Args = CommandArgs
            };

            var tagBuilder = new TagBuilder("button");
            tagBuilder.GenerateId(fullName, Generator.IdAttributeDotReplacement);
            tagBuilder.MergeAttribute("name", fullName);
            tagBuilder.MergeAttribute("value", info.Stringified);
            output.MergeAttributes(tagBuilder);
        }
    }
}