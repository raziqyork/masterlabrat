﻿namespace MSharp.Framework.AspNet.TagHelpers
{
    using System.Threading.Tasks;
	using Microsoft.AspNet.Mvc;
	using Microsoft.AspNet.Mvc.Rendering;
	using Microsoft.AspNet.Razor.Runtime.TagHelpers;

	[HtmlElementName("AutoComplete")]
	public class AutoCompleteTagHelper : TagHelper
	{
		[Activate]
		protected internal IHtmlGenerator Generator { get; set; }
		[Activate]
		protected internal ViewContext ViewContext { get; set; }

		[HtmlAttributeName("asp-autoexpand")]
		public bool AutoExpand { get; set; }

		[HtmlAttributeName("for")]
		public bool For { get; set; }

		public override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
		{
			return base.ProcessAsync(context, output);
		}

		public override void Process(TagHelperContext context, TagHelperOutput output)
		{
			var textField = Generator.GenerateTextBox(ViewContext, null, "myName", "lorem ipsum", "{0}",
				new
				{
					//id = context.UniqueId,
					@data_api = "/api/xxx",
					@data_auto_expand = AutoExpand,
				});

			output.SuppressOutput();

            output.Content.SetContent(textField.ToHtmlString(TagRenderMode.SelfClosing).ToString());

            //output.SelfClosing = true;
        }
	}
}