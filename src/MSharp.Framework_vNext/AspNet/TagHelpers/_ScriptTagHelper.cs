﻿namespace MSharp.Framework.AspNet.TagHelpers
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.Rendering;
    using Microsoft.AspNet.Razor.Runtime.TagHelpers;

    [HtmlElementName("script")]
    public class ScriptTagHelper : TagHelper
    {
        [Activate]
        protected internal IHtmlGenerator Generator { get; set; }
        [Activate]
        protected internal ViewContext ViewContext { get; set; }

        [HtmlAttributeName("type")]
        public string Type { get; set; }
        [HtmlAttributeName("src")]
        public string Src { get; set; }
        [HtmlAttributeName("async")]
        public object Async { get; set; }
        [HtmlAttributeName("defer")]
        public object Defer { get; set; }
        [HtmlAttributeName("charset")]
        public string Charset { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            //Microsoft.AspNet.Mvc.TagHelpers.InputTagHelper;
            //Microsoft.AspNet.Mvc.TagHelpers.AnchorTagHelper;

            //output.SuppressOutput();
            //output.Content = textField.ToHtmlString(TagRenderMode.SelfClosing).ToString();
            //output.SelfClosing = true;

            if (Src == null)
            {
                output.PreContent.SetContent("/*XXXXXXXXX*/");
                output.PostContent.SetContent("/*YYYYYYYYY*/");
            }

            foreach (var attr in context.AllAttributes)
                output.Attributes.Add(attr.Key, attr.Value?.ToString());
        }
    }
}