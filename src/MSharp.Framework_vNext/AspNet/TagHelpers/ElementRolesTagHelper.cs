﻿namespace MSharp.Framework.AspNet.TagHelpers
{
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Razor.Runtime.TagHelpers;
    using MSharp.Framework.AspNet.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    [HtmlElementName("body", "form", "div", "table", "tr", "td", "th", "button", "input", "a", "ul", "li", "i", "span")]
    public class ElementRolesTagHelper : TagHelper
    {
        public override int Order => 9998;

        [HtmlAttributeName("asp-role")]
        public string Roles { get; set; }

        [Activate]
        protected internal IElementAttributeHelper AttributeHelper { get; set; }

        [Activate]
        protected internal ElementRolesServiceProvider ServiceProvider { get; set; }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (Roles == null)
                return;

            var names = Roles.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Distinct();

            bool? isRendered = null;
            string newTag = null;
            var attributeValues = new Dictionary<string, string>();

            foreach (var service in ServiceProvider.Options)
            {
                foreach (var role in await service.GetElementRolesAsync(names))
                {
                    // For each attribute in each Role linked to this HtmlElement
                    foreach (var attribute in role.Attributes())
                    {
                        // Try to get the current attribute value, or use the original value from the HtmlElement
                        string currentValue;
                        if (!attributeValues.TryGetValue(attribute.Key, out currentValue))
                        {
                            string originalValue;
                            if (output.Attributes.TryGetValue(attribute.Key, out originalValue))
                            {
                                output.Attributes.Remove(attribute.Key);
                            }
                            currentValue = originalValue ?? String.Empty;
                        }

                        // Merge current attribute value with attribute value defined in the Role
                        var newValue = AttributeHelper.MergeAttributeValues(attribute.Key, currentValue, attribute.Value);

                        attributeValues[attribute.Key] = newValue;
                    }

                    // Detect if Role defines this attribute as non-rendered.
                    isRendered = isRendered.GetValueOrDefault() || role.IsRendered;

                    if (role.NewTag != null)
                    {
                        if (newTag != null && !newTag.Equals(role.NewTag, StringComparison.OrdinalIgnoreCase))
                            throw new InvalidOperationException($"ElementRoles on element {output.TagName} are trying to change the tag to both {role.NewTag} and {newTag}.");

                        newTag = role.NewTag;
                    }
                }
            }

            if (isRendered == false)
            {
                output.TagName = null; // omits start and end tags
            }
            else
            {
                if (newTag != null)
                {
                    output.TagName = newTag;
                }

                foreach (var attribute in attributeValues)
                {
                    var value = attribute.Value;

                    if (!String.IsNullOrWhiteSpace(value))
                    {
                        output.Attributes[attribute.Key] = value != "{}" ? value : null;
                    }
                    else if (output.Attributes.ContainsKey(attribute.Key))
                    {
                        output.Attributes.Remove(attribute.Key);
                    }
                }
            }
        }
    }
}