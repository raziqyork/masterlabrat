﻿namespace MSharp.Framework.AspNet.Mvc
{
	using System;
	using System.Collections.Generic;
	using System.Threading;

	/// <summary>
	/// Used to help synchronise async properties/methods in LinkInfo.
	/// </summary>
	public class GoInfoScope : IDisposable
	{
		private readonly LinkHelperService helper;
		private readonly List<LinkInfo> infos = new List<LinkInfo>();
		private readonly SemaphoreSlim sem = new SemaphoreSlim(1);

		public GoInfoScope(LinkHelperService helper)
		{
			this.helper = helper;
		}

		public void Dispose()
		{
			// todo: Release semaphore to complete all LinkInfo.IsActiveAsyc instructions
		}

		public LinkInfo To(string action, string controller, string area = null, object routeValues = null)
		{
			var info = helper.To(action, controller, area, routeValues);
			infos.Add(info);
			return info;
		}
	}
}