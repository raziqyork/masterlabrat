﻿namespace MSharp.Framework.AspNet.TagHelpers
{
    using System;
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.Rendering;
    using Microsoft.AspNet.Razor.Runtime.TagHelpers;
    using Mvc;

    [HtmlElementName("go", "go-scope")]
    public class GoScopeTagHelper : TagHelper
    {
        [Activate]
        protected internal IHtmlGenerator Generator { get; set; }
        [Activate]
        protected internal ViewContext ViewContext { get; set; }
        [Activate]
        protected internal LinkInfo Link { get; set; }

        [HtmlAttributeName("key")]
        public string Key { get; set; }

        [HtmlAttributeName("action")]
        public string Action { get; set; }

        [HtmlAttributeName("controller")]
        public string Controller { get; set; }

        [HtmlAttributeName("area")]
        public string Area { get; set; }

        [HtmlAttributeName("url")]
        public string Url { get; set; }

        //[HtmlAttributeName("type")]
        //public string Type { get; set; }
        //[HtmlAttributeName("src")]
        //public string Src { get; set; }
        //[HtmlAttributeName("async")]
        //public object Async { get; set; }
        //[HtmlAttributeName("defer")]
        //public object Defer { get; set; }
        //[HtmlAttributeName("charset")]
        //public string Charset { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            // Always strip the outer tag name as we never want <go> to render
            output.TagName = null;

            output.PreContent.Append($"<!--{Action}-->");
            output.PreContent.Append($"<!--{Controller}-->");
            output.PreContent.Append($"<!--{Area}-->");
            output.PreContent.Append($"<!--{Url}-->");

            if (Url != null)
            {
                Link.SetRouteOrUrl(Url);
            }
            else if (Action != null)
            {
                Link.SetRouteOrUrl(Action, Controller, Area, null);
            }
            else
            {
                throw new NotSupportedException();
            }

            var suffix = Key?.Trim() ?? String.Empty;
            ViewContext.ViewData[$"GoTo{suffix}"] = Link;

            //output.SuppressOutput();
            //output.Content = textField.ToHtmlString(TagRenderMode.SelfClosing).ToString();
            //output.SelfClosing = true;
            //output.PreContent  += "/*XXXXXXXXX*/";
            //output.PostContent += "/*YYYYYYYYY*/";
            //foreach (var attr in context.AllAttributes)
            //	output.Attributes.Add(attr.Key, attr.Value?.ToString());
        }
    }
}