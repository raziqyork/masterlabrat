﻿namespace MSharp.Framework.AspNet.TagHelpers
{
    using Microsoft.AspNet.Razor.Runtime.TagHelpers;

    [HtmlElementName("a")]
    public class AnchorTagHelper : Microsoft.AspNet.Mvc.TagHelpers.AnchorTagHelper
    {
        [HtmlAttributeName("asp-area")]
        public string Area { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.Add("xxxx", Area);

            base.Process(context, output);
        }
    }
}