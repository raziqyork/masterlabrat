﻿namespace MSharp.Framework.AspNet.Mvc
{
	public interface IRouteHelper
	{
		int? GetDistance(IUrlSource source, string fromPath);

        void RegisterUrlHost(IUrlSource source);

        IUrlSource CalculateActiveUrlHost(string fromPath);
	}
}