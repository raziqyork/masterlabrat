﻿namespace MSharp.Framework.AspNet.Mvc
{
	using Microsoft.AspNet.Mvc.Razor;

	public abstract class MyCustomRazorPage : RazorPage
	{
	}

	public abstract class MyCustomRazorPage<TModel> : RazorPage<TModel>
	{
	}
}