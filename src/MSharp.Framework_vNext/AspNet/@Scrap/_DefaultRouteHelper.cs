﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNet.Mvc;

    public class DefaultRouteHelper : IRouteHelper
    {
        private readonly HashSet<IUrlSource> urlHosts = new HashSet<IUrlSource>();
        private readonly IUrlHelper urlHelper;

        public DefaultRouteHelper(IUrlHelper urlHelper)
        {
            this.urlHelper = urlHelper;
        }

        public int? GetDistance(IUrlSource source, string fromPath)
        {
            //var url0 = urlHelper.ParseUrlOrPath(source.Url);
            //var url1 = urlHelper.ParseUrlOrPath(fromPath);

            //if (url0.Authority != url1.Authority) return null;
            //if (url0.Port != url1.Port) return null; /* remove for apps with mixed https and http */
            //if (url0.Scheme != url1.Scheme) return null; /* remove for apps with mixed https and http */

            //for (int i0 = 0, i1 = 0; i0 < url0.Segments.Length || i1 < url1.Segments.Length; i0++, i1++)
            //{
            //	var seg0 = url0.Segments.ElementAtOrDefault(i0);
            //	var seg1 = url1.Segments.ElementAtOrDefault(i1);

            //	if (seg0 != null && seg1 != null && seg0 != seg1) break;
            //}
            return null;
        }

        public void RegisterUrlHost(IUrlSource source)
        {
            urlHosts.Add(source);
        }

        public IUrlSource CalculateActiveUrlHost(string fromPath)
        {
            var active = urlHosts.WithMin(source => GetDistance(source, fromPath) ?? Int32.MaxValue);
            return active;
        }
    }
}