﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.ModelBinding;
    using Microsoft.AspNet.Mvc.ModelBinding.Metadata;
    using Microsoft.Framework.DependencyInjection;

    public class EasyBinder
    {
        private readonly ActionBindingContext bindingContext;
        private readonly IModelMetadataProvider modelMetadataProvider;
        private readonly ModelStateDictionary modelState;
        private readonly OperationBindingContext operationBindingContext;

        public EasyBinder(
            IScopedInstance<ActionContext> actionContext,
            IScopedInstance<ActionBindingContext> bindingContext,
            IModelMetadataProvider modelMetadataProvider)
        {
            this.bindingContext = bindingContext.Value;
            this.modelState = actionContext.Value?.ModelState;
            this.modelMetadataProvider = modelMetadataProvider;
            this.operationBindingContext = new OperationBindingContext
            {
                ModelBinder = this.bindingContext.ModelBinder,
                ValidatorProvider = this.bindingContext.ValidatorProvider,
                MetadataProvider = this.modelMetadataProvider,
                HttpContext = actionContext.Value?.HttpContext,
                ValueProvider = this.bindingContext.ValueProvider
            };
        }

        public async Task<object> BindModelAsync(Type modelType, string propertyName = null)
        {
            var identity = ModelMetadataIdentity.ForProperty(modelType, propertyName, null);

            //var modelMetadata = new ModelMetadata(modelMetadataProvider, null, modelType, propertyName);
            ModelMetadata modelMetadata = null;
            //var x = ModelMetadataIdentity.ForType(modelType);
            ////var modelMetadata = new ModelMetadata(modelMetadataProvider,  null, modelType, propertyName);
            //var modelMetadata = new DefaultModelMetadata(modelMetadataProvider, null, modelType, propertyName);

            var modelBindingContext = new ModelBindingContext
            {
                ModelName = modelMetadata.BinderModelName ?? modelMetadata.PropertyName,
                ModelMetadata = modelMetadata,
                ModelState = modelState,

                // Fallback only if there is no explicit model name set.
                FallbackToEmptyPrefix = modelMetadata.BinderModelName == null,
                ValueProvider = operationBindingContext.ValueProvider,
                OperationBindingContext = operationBindingContext,
            };

            var modelBindingResult = await bindingContext.ModelBinder.BindModelAsync(modelBindingContext);

            return modelBindingResult.Model;
        }

        public async Task<T> BindModelAsync<T>(string propertyName = null) where T : class, new()
        {
            return (T)(await BindModelAsync(typeof(T), propertyName));
        }

        public object BindModel(Type modelType, string propertyName = null)
        {
            return BindModelAsync(modelType, propertyName).Result;
        }

        public T BindModel<T>(string propertyName = null) where T : class, new()
        {
            return BindModelAsync<T>(propertyName).Result;
        }
    }
}