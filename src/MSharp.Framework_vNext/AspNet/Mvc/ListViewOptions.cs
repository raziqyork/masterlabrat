﻿namespace MSharp.Framework.AspNet
{
    using System;

    public class ListViewOptions
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Sort { get; set; }
        public Func<object, IComparable> SortFunc { get; set; }
        public bool IsComplexSort => SortFunc != null;
    }
}