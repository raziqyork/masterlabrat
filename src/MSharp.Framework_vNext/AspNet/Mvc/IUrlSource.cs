﻿namespace MSharp.Framework.AspNet.Mvc
{
	public interface IUrlSource
	{
		string Url { get; }
	}
}