﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using System.Linq;

    public class DefaultElementAttributeHelper : IElementAttributeHelper
    {
        public string MergeAttributeValues(string attribute, string existingValue, string newValue)
        {
            // Todo: It appears duplciate sequencial attribute values aren't being removed.
            // e.g.: action="post post"

            if (Is(attribute, "class", "role"))
            {
                return ConcatUniqueValues(existingValue, newValue);
            }

            if (StartsWith(attribute, "On"))
            {
                return Join("; ", existingValue, newValue);
            }

            return Join(" ", existingValue, newValue);
        }

        private static string Join(string spearator, params string[] values)
        {
            return String.Join(spearator, values.Where(o => !String.IsNullOrWhiteSpace(o)));
        }

        protected static string ConcatUniqueValues(string value0, string value1)
        {
            var parts = String.Concat(value0, " ", value1).Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                  .Distinct(StringComparer.OrdinalIgnoreCase);

            return String.Join(" ", parts);
        }

        private static bool Is(string attribute, string name, params string[] moreNames)
        {
            return String.Equals(attribute, name, StringComparison.OrdinalIgnoreCase)
                || (moreNames != null && moreNames.Any(n => Is(attribute, n)));
        }

        private static bool StartsWith(string attribute, string prefix)
        {
            return attribute.StartsWith(prefix, StringComparison.OrdinalIgnoreCase);
        }
    }
}