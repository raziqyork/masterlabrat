﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc.Razor;
    using Microsoft.Framework.ConfigurationModel;
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    //[Obsolete("Only needed when using View Components for Modules.")]
    public class ModuleViewLocationExpander : IViewLocationExpander
    {
        private readonly string modulesDir;

        public ModuleViewLocationExpander(IConfiguration configuration)
        {
            modulesDir = configuration.Get("MSharp:Mvc:GeneratedModulesDir") ?? "_Modules";
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
        }

        private static readonly Regex ComponentViewPattern = new Regex(@"^Components/(.+)/Default$",
            RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            var componentView = ComponentViewPattern.Match(context.ViewName);
            if (componentView.Success)
            {
                return ExpandComponentViewLocations(context, componentView, viewLocations);
            }

            return viewLocations;
        }

        public IEnumerable<string> ExpandComponentViewLocations(ViewLocationExpanderContext context, Match componentView, IEnumerable<string> viewLocations)
        {
            var componentName = componentView.Groups[1].Value;

            yield return $"/Views/{modulesDir}/{componentName}/Default.cshtml";

            foreach (var location in viewLocations)
                yield return location;
        }
    }
}