﻿namespace MSharp.Framework.AspNet.Mvc
{
	using System;
	using System.Collections.Generic;

	public class ParamSet : IParamHelper
	{
		private const string IdentifierFragmentPrefix = "frag_";
		private const string IdentifierEscapedDot = "dot_";

		private readonly Dictionary<string, object> queries = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
		private readonly Dictionary<string, object> frags = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

		public IDictionary<string, object> Queries => queries;
		public IDictionary<string, object> Fragments => frags;

		public void MergeParamSet(ParamSet otherSet)
		{
			MergeQueries(otherSet.Queries);
			MergeFragments(otherSet.Fragments);
		}

		public void MergeQueries(IDictionary<string, object> queryDictionary, bool unescapeNames = false)
		{
			foreach (var pair in queryDictionary)
			{
				SetQuery(pair.Key, pair.Value, unescapeNames);
			}
		}

		public void MergeFragments(IDictionary<string, object> fragDictionary, bool unescapeNames = false)
		{
			foreach (var pair in fragDictionary)
			{
				SetFragment(pair.Key, pair.Value, unescapeNames);
			}
		}

		public void SetParams(object parameters)
		{
			if (parameters == null)
			{
				return;
			}

			if (parameters is ParamSet)
			{
				MergeParamSet(parameters as ParamSet);
				return;
			}

			foreach (var prop in parameters.GetType().GetProperties())
			{
				SetParam(prop.Name, prop.GetValue(parameters));
			}
		}

		public void SetParams(IDictionary<string, object> parameters)
		{
			foreach (var prop in parameters)
			{
				SetParam(prop.Key, prop.Value);
			}
		}

		public void SetParam(string name, object value)
		{
			if (IsFragmentParam(name))
			{
				SetFragment(name, value);
			}
			else
			{
				SetQuery(name, value);
			}
		}

		public void SetFragment(string name, object value, bool unescapeName = false)
		{
			frags[unescapeName ? UnescapeParamName(name) : name] = value;
		}

		public void SetQuery(string name, object value, bool unescapeName = false)
		{
			queries[unescapeName ? UnescapeParamName(name) : name] = value;
		}

		private static bool IsFragmentParam(string parameterName)
		{
			return parameterName.StartsWith(IdentifierFragmentPrefix, StringComparison.OrdinalIgnoreCase);
		}

		public static string UnescapeParamName(string name)
		{
			if (IsFragmentParam(name))
			{
				name = name.Substring(IdentifierFragmentPrefix.Length);
			}

			if (name.StartsWith(IdentifierEscapedDot, StringComparison.OrdinalIgnoreCase))
			{
				name = '.' + name.Substring(IdentifierEscapedDot.Length);
			}

			return name;
		}

		IParamHelper IParamHelper.Frag(string key)
		{
			frags[key] = null;
			return this;
		}

		IParamHelper IParamHelper.Frag(string key, object value)
		{
			frags[key] = value;
			return this;
		}

		IParamHelper IParamHelper.Qry(string key, object value)
		{
			queries[key] = value;
			return this;
		}
	}
}