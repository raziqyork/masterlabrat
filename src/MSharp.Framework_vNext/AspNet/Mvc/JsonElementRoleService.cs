﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.Framework.ConfigurationModel;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// Reads element role values from MSharp:Mvc:UX:ElementRoles hive in IConfiguration.
    /// </summary>
    public class JsonElementRoleService : IElementRolesService
    {
        private readonly IServiceProvider services;
        private readonly Dictionary<string, List<ElementRole>> rules;
        private IConfiguration configuration;
        private bool isInitialised;

        public JsonElementRoleService(IServiceProvider services, IConfiguration configuration)
        {
            this.services = services;
            this.configuration = configuration;
            this.rules = new Dictionary<string, List<ElementRole>>(StringComparer.OrdinalIgnoreCase);
        }

        public async Task<IEnumerable<ElementRole>> GetElementRolesAsync(IEnumerable<string> names)
        {
            if (!isInitialised)
            {
                await Task.Run(() => Initialise());
            }

            List<ElementRole> allRoles = new List<ElementRole>();

            if (rules.Count > 0)
            {
                foreach (var name in names)
                {
                    List<ElementRole> roles;
                    if (rules.TryGetValue(name, out roles))
                        allRoles.AddRange(roles);
                }
            }

            return allRoles;
        }

        public void Initialise()
        {
            var uxConfig = configuration.GetSubKey("MSharp:Mvc:UX:ElementRoles");

            if (uxConfig != null)
            {
                foreach (var roleData in uxConfig.GetSubKeys())
                {
                    var name = roleData.Key;

                    List<ElementRole> namedRoles;
                    if (!rules.TryGetValue(name, out namedRoles))
                    {
                        rules[name] = namedRoles = new List<ElementRole>();
                    }

                    namedRoles.Add(new JsonElementRole(roleData.Value));
                }
            }

            isInitialised = true;
        }
    }
}