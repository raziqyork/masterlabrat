﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.AspNet.Mvc.Razor;
    using Microsoft.AspNet.Mvc.Razor.OptionDescriptors;
    using Microsoft.Framework.ConfigurationModel;
    using System.Collections.Generic;
    using System.Linq;

    public class MSharpRazorViewEngine : RazorViewEngine
    {
        private readonly string pagesDir;
        private readonly string modulesDir;
        private IList<string> viewLocationFormats;
        private IList<string> areaViewLocationFormats;

        public MSharpRazorViewEngine(
            IRazorPageFactory pageFactory,
            IRazorViewFactory viewFactory,
            IViewLocationExpanderProvider viewLocationExpanderProvider,
            IViewLocationCache viewLocationCache,
            IConfiguration configuration)
            : base(pageFactory, viewFactory, viewLocationExpanderProvider, viewLocationCache)
        {
            pagesDir = configuration.Get("MSharp:Mvc:GeneratedPagesDir") ?? "_Pages";
            modulesDir = configuration.Get("MSharp:Mvc:GeneratedModulesDir") ?? "_Modules";
        }

        public override IEnumerable<string> ViewLocationFormats
        {
            get
            {
                if (viewLocationFormats == null)
                {
                    viewLocationFormats = base.ViewLocationFormats.ToList();

                    /* The order here is important as it may lead to stack-overflow */
                    /* Always add to end of list */
                    viewLocationFormats.Add($"/Views/{pagesDir}/{{1}}/{{0}}.cshtml"); // ~/Views/_Pages/{controller}/{action}.cshtml
                    viewLocationFormats.Add($"/Views/{pagesDir}/{{1}}.cshtml"); // ~/Views/_Pages/{controller}.cshtml
                    viewLocationFormats.Add($"/Views/{pagesDir}/{{0}}.cshtml"); // ~/Views/_Pages/{action}.cshtml
                    viewLocationFormats.Add($"/Views/{modulesDir}/{{1}}/{{0}}.cshtml"); // ~/Views/_Modules/{controller}/{action}.cshtml
                }

                return viewLocationFormats;
            }
        }

        public override IEnumerable<string> AreaViewLocationFormats
        {
            get
            {
                if (areaViewLocationFormats == null)
                {
                    areaViewLocationFormats = base.AreaViewLocationFormats.ToList();

                    /* The order here is important as it may lead to stack-overflow */
                    /* Always add to end of list */
                    areaViewLocationFormats.Add($"/Views/{pagesDir}/{{2}}/{{1}}/{{0}}.cshtml"); // ~/Views/_Pages/{area}/{controller}/{action}.cshtml
                    areaViewLocationFormats.Add($"/Views/{pagesDir}/{{2}}/{{1}}.cshtml"); // ~/Views/_Pages/{area}/{controller}.cshtml
                    areaViewLocationFormats.Add($"/Views/{pagesDir}/{{2}}/{{0}}.cshtml"); // ~/Views/_Pages/{area}/{action}.cshtml
                }

                return areaViewLocationFormats;
            }
        }
    }
}
