﻿namespace MSharp.Framework.AspNet.Mvc
{
	using System;
	using System.Linq;
	using System.Collections.Generic;

	public class DefaultWebParametiser : IWebParametiser
	{
		public IDictionary<string, object> SplitQueryString(string queryString)
		{
			return SplitParams(queryString, QueryStringPrefix, QueryStringPairDelim, QueryStringKeyValueDelim, QueryStringKeysCaseSensitive);
		}

		public IDictionary<string, object> SplitFragment(string fragment)
		{
			return SplitParams(fragment, FragmentPrefix, FragmentPairDelim, FragmentKeyValueDelim, FragmentKeysCaseSensitive);
		}

		public string JoinQueryString(IDictionary<string, object> pairs)
		{
			return JoinParams(pairs, QueryStringPrefix, QueryStringPairDelim, QueryStringKeyValueDelim);
		}

		public string JoinFragments(IDictionary<string, object> pairs)
		{
			//return JoinParams(pairs, FragmentPrefix, Uri.EscapeDataString(FragmentPairDelim), FragmentKeyValueDelim);
			return JoinParams(pairs, FragmentPrefix, FragmentPairDelim, FragmentKeyValueDelim);
		}

		private static string JoinParams(IDictionary<string, object> pairs, string prefix, string pairDelim, string keyValDelim)
		{
			var query =
				from p in pairs
				let left = Uri.EscapeDataString(p.Key)
				let val = p.Value?.ToString()
				let right = val != null ? keyValDelim + Uri.EscapeDataString(val) : ""
				select left + right;

			var joined = String.Join(pairDelim, query);

			return joined.Length > 0 ? prefix + joined : joined;
		}

		private static IDictionary<string, object> SplitParams(string text, string prefix, string pairDelim, string keyValDelim, bool caseSensitiveKeys)
		{
			var dictionary = caseSensitiveKeys
				? new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
				: new Dictionary<string, object>();

			foreach (var pair in text.Split(new[] { prefix, pairDelim }, StringSplitOptions.RemoveEmptyEntries))
			{
				var parts = pair.Split(new[] { keyValDelim }, StringSplitOptions.None);
				var key = parts.ElementAtOrDefault(0);

				if (key != null)
				{
					var value = parts.ElementAtOrDefault(1);
					value = value != null ? Uri.UnescapeDataString(value) : null;
					dictionary[Uri.UnescapeDataString(key)] = value;
				}
			}

			return dictionary;
		}

		public bool QueryStringKeysCaseSensitive => false;

		public string QueryStringPrefix => "?";

		public string QueryStringPairDelim => "&";

		public string QueryStringKeyValueDelim => "=";

		public bool FragmentKeysCaseSensitive => false;

		public string FragmentPrefix => "#";

		public string FragmentPairDelim => "&";

		public string FragmentKeyValueDelim => "=";
	}
}