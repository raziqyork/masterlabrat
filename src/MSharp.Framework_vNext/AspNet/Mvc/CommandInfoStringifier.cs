﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using System.Text.RegularExpressions;

    public class CommandInfoStringifier : IStringifier
    {
        Type IStringifier.TargetType => typeof(CommandInfo);

        public string ToString(object obj)
        {
            var info = (CommandInfo)obj;
            return String.Concat(info.Name, "|", info.RowIndex, "|", info.Id, "|", info.Args);
        }

        public object FromString(string text, object obj = null)
        {
            if (text == null) return obj;

            var match = Pattern.Match(text);

            if (!match.Success)
                throw new InvalidOperationException($"{GetType()} cannot deserialise '{text}'.");

            var info = ((CommandInfo)obj) ?? new CommandInfo();

            info.Name = match.Groups[1].Value;
            info.RowIndex = match.Groups[2].Value.TryParse<int>();
            info.Id = match.Groups[3].Value.TryParse<int, Guid>(orText: true);
            info.Args = match.Groups[4].Value;

            return info;
        }

        private static readonly Regex Pattern = new Regex(@"^(\w+)\|(\d+)?\|([^|]+)?\|(.+)?$",
            RegexOptions.Compiled | RegexOptions.Singleline);
    }
}