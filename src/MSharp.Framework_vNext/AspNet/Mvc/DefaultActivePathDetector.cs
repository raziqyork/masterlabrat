﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Http;
    using MSharp.Collections;

    public class DefaultActivePathDetector : IActivePathDetector
    {
        private const string PathToken = "{path}";
        private const string IdQuery = "id";

        public bool CalculateIsActive(HttpRequest request, LinkInfo linkInfo, params object[] dependencies)
        {
            // Set default arguments if none supplied
            if (dependencies?.None() ?? true)
            {
                dependencies = new object[] { PathToken, IdQuery };
            }

            // Build path list to use with binary tree

            var path0 = new List<string>();
            var path1 = new List<string>();

            foreach (string dependency in dependencies)
            {
                if (dependency.Equals(PathToken, StringComparison.OrdinalIgnoreCase))
                {
                    path0.AddRange((request.PathBase.ToUriComponent() + request.Path.ToUriComponent()).Split('/'));
                    path1.AddRange(linkInfo.RouteOrUrl.Split('/'));
                }
                else
                {
                    if (request.Query.ContainsKey(dependency))
                    {
                        var val = request.Query[dependency];
                        if (val != null)
                        {
                            path0.Add(val);
                        }
                    }

                    if (linkInfo.Parameters.Queries.ContainsKey(dependency))
                    {
                        var val = linkInfo.Parameters.Queries[dependency]?.ToString();
                        if (val != null)
                        {
                            path1.Add(val);
                        }
                    }
                }
            }

            // use binary tree to calculate distance
            var tree = new BTree<string>();
            var leaf0 = tree.AddPath(path0);
            var leaf1 = tree.AddPath(path1);
            var distance = leaf0.GetLineage(leaf1);

            var isActive = distance.GetValueOrDefault(-1) >= 0;

            return isActive;
        }

        public async Task<bool> CalculateIsActiveAsync(HttpRequest request, LinkInfo linkInfo, params object[] arguments)
        {
            await Task.Delay(0);
            throw new NotImplementedException();
            //await Task.Delay(2000); return true;
        }
    }
}
