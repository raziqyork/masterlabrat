﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.Framework.ConfigurationModel;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public sealed class JsonElementRole : ElementRole
    {
        private readonly IDictionary<string, string> attributes;

        private const char StringDelim = '\'';

        public JsonElementRole(IConfiguration roleData)
        {
            attributes = roleData.GetSubKeys().ToDictionary(p => p.Key, p => GetValue(roleData, p.Key));

            IsRendered = true;

            string value;
            if (attributes.TryGetValue("rendered", out value))
            {
                bool isRendered;
                if (Boolean.TryParse(value, out isRendered))
                {
                    IsRendered = isRendered;
                }
                attributes.Remove("rendered");
            }

            if (attributes.TryGetValue("newTag", out value))
            {
                NewTag = value;
                attributes.Remove("newTag");
            }
        }

        public override bool IsRendered { get; }

        public override string NewTag { get; }

        public override void Config(IDictionary<string, string> attributes)
        {
            foreach (var pair in this.attributes)
            {
                attributes[pair.Key] = pair.Value?.Trim();
            }
        }

        private static string GetValue(IConfiguration roleData, string key)
        {
            var value = roleData.Get(key);
            if (value != null)
                return value;

            // If value is a hive of sub keys, then JSONify it
            var subkeys = roleData.GetSubKeys(key);
            if (subkeys != null) // && subkeys.Any())
            {
                var values = new StringBuilder();
                foreach (var pair in subkeys)
                {
                    values.Append(StringDelim).Append(pair.Key).Append(StringDelim).Append(":");

                    var subValue = GetValue(roleData, key + ":" + pair.Key)?.Trim() ?? "null";

                    var isLiteral = !subValue.StartsWith("{") && subValue != "null";

                    if (isLiteral) values.Append(StringDelim);

                    values.Append(subValue);

                    if (isLiteral) values.Append(StringDelim);

                    values.Append(",");
                }

                if (values.Length > 0)
                    return "{" + values.ToString().Trim(' ', ',') + "}";
            }

            return null;
        }
    }
}