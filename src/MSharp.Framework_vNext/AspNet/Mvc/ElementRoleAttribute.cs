﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class ElementRoleAttribute : Attribute
    {
        public string Name { get; }

        public ElementRoleAttribute(string name)
        {
            Name = name;
        }
    }
}