﻿namespace MSharp.Framework.AspNet.Mvc
{
    using Microsoft.Framework.DependencyInjection;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Threading.Tasks;

    /// <summary>
    /// Uses reflection to find all concrete ElementRole types in entry assembly.
    /// </summary>
    public class DefaultElementRolesService : IElementRolesService
    {
        private readonly IServiceProvider services;
        private readonly ApplicationInfo appInfo;
        private readonly Dictionary<string, List<ElementRole>> rules;
        private bool isInitialised;

        public DefaultElementRolesService(IServiceProvider services, ApplicationInfo appInfo)
        {
            this.services = services;
            this.appInfo = appInfo;
            this.rules = new Dictionary<string, List<ElementRole>>(StringComparer.OrdinalIgnoreCase);
        }

        public async Task<IEnumerable<ElementRole>> GetElementRolesAsync(IEnumerable<string> names)
        {
            if (!isInitialised)
            {
                await Task.Run(() => Initialise());
            }

            List<ElementRole> allRoles = new List<ElementRole>();

            if (rules.Count > 0)
            {
                foreach (var name in names)
                {
                    List<ElementRole> roles;
                    if (rules.TryGetValue(name, out roles))
                        allRoles.AddRange(roles);
                }
            }

            return allRoles;
        }

        public void Initialise()
        {
            var assembly = appInfo.ApplicationAssembly ?? Assembly.GetCallingAssembly();

            foreach (var type in assembly.DefinedTypes)
            {
                if (type.IsClass &&
                    !type.IsGenericType &&
                    !type.IsAbstract &&
                    typeof(ElementRole).IsAssignableFrom(type))
                {
                    var role = (ElementRole)ActivatorUtilities.CreateInstance(services, type);

                    foreach (var name in GetRoleNames(type))
                    {
                        List<ElementRole> namedRoles;
                        if (!rules.TryGetValue(name, out namedRoles))
                        {
                            rules[name] = namedRoles = new List<ElementRole>();
                        }

                        namedRoles.Add(role);
                    }
                }
            }

            isInitialised = true;
        }

        private static IEnumerable<string> GetRoleNames(Type ruleType)
        {
            var attributesDefined = false;
            foreach (var attribute in ruleType.GetCustomAttributes<ElementRoleAttribute>(false))
            {
                attributesDefined = true;
                yield return attribute.Name;
            }

            if (!attributesDefined)
            {
                var name = ruleType.Name.ToLower();
                if (ruleType.Name.EndsWith("ElementRole"))
                {
                    name = name.Substring(0, name.Length - 11);
                }
                else if (ruleType.Name.EndsWith("Role"))
                {
                    name = name.Substring(0, name.Length - 4);
                }

                yield return name;
            }
        }
    }
}