﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using Microsoft.AspNet.Mvc;
    using Microsoft.AspNet.Mvc.Rendering;
    using Microsoft.Framework.DependencyInjection;

    /// <summary>
    /// M# Equivalent: Page.GetPageUrl(string resourceKey) 
    /// </summary>
    public class LinkInfo : IUrlSource, IDisposable
    {
        private readonly ActionContext actionContext;
        private readonly IUrlHelper urlHelper;
        private readonly IWebParametiser parametiser;
        private readonly ParamSet parameters;
        private bool initialised;

        public LinkInfo(
            IScopedInstance<ActionContext> contextAccessor,
            IUrlHelper urlHelper,
            IWebParametiser parametiser,
            IRouteHelper routeHelper,
            IActivePathDetector activePathDetector
            )
        {
            this.parameters = new ParamSet();
            this.actionContext = contextAccessor.Value;
            this.urlHelper = urlHelper;
            this.parametiser = parametiser;
            this.ActivePathDetector = activePathDetector;
        }

        void IDisposable.Dispose() { }

        public IActivePathDetector ActivePathDetector { get; set; }
        public string RouteOrUrl { get; set; }

        public ParamSet Parameters => parameters;
        public HtmlString Url => new HtmlString(BuildUrl());
        string IUrlSource.Url => BuildUrl();

        public bool IsActive => IsActiveOn();

        public bool IsActiveOn(params object[] arguments)
        {
            return ActivePathDetector.CalculateIsActive(actionContext.HttpContext.Request, this, arguments);
        }

        public bool IsActiveOn<T>(params object[] arguments) where T : class, IActivePathDetector, new()
        {
            return new T().CalculateIsActive(actionContext.HttpContext.Request, this, arguments);
        }

        public void SetRouteOrUrl(string href)
        {
            Uri uri;
            if (Uri.TryCreate(href, UriKind.Absolute, out uri))
            {
                RouteOrUrl = uri.GetLeftPart(UriPartial.Authority) + uri.LocalPath;
            }
            else if (urlHelper.IsLocalUrl(href) && Uri.TryCreate("http://0.0.0.0" + urlHelper.Content(href), UriKind.Absolute, out uri))
            {
                RouteOrUrl = uri.LocalPath;
            }
            else
            {
                throw new InvalidOperationException($"'{href}' is not a valid URL.");
            }

            parameters.MergeQueries(parametiser.SplitQueryString(uri.Query), unescapeNames: true);

            parameters.MergeFragments(parametiser.SplitFragment(uri.Fragment), unescapeNames: true);

            initialised = true;
        }

        public void SetRouteOrUrl(string action, string controller, string area, object routeValues)
        {
            if (routeValues is ParamSet)
            {
                // Merge all parameter from routeValues if it's a ParamSet
                parameters.MergeParamSet(routeValues as ParamSet);
            }

            // Resolve action route using IUrlHelper, then treat as regular Href
            SetRouteOrUrl(urlHelper.Action(action, controller, area, routeValues));
        }

        //private string _BuildUrl;

        public string BuildUrl()
        {
            if (RouteOrUrl == null || !initialised)
                throw new NotSupportedException("Either an HRef or an Action must be supplied (using the SetRouteOrUrl method).");

            return //_BuildUrl ?? (_BuildUrl =
                RouteOrUrl +
                    parametiser.JoinQueryString(parameters.Queries) +
                    parametiser.JoinFragments(parameters.Fragments)
            //)
            ;
        }
    }
}