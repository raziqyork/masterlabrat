namespace MSharp.Framework.AspNet.Mvc
{
    public class CommandInfo
    {
        public object Id { get; set; }
        public string Name { get; set; }
        public string Args { get; set; }
        public int? RowIndex { get; set; }

        public string Stringified
        {
            get
            {
                return Stringifier.ToString(this);
            }
            set
            {
                Stringifier.FromString(value, this);
            }
        }

        private static readonly IStringifier Stringifier = new CommandInfoStringifier();
    }
}