﻿using System;

namespace MSharp.Framework.AspNet.Mvc
{
    public class DefaultElementIdentityGenerator : IElementIdentityGenerator
    {
        private volatile int generalIdSeed = 0;
        private volatile int moduleIdSeed = 0;

        public string NextModuleId() => $"ui{(moduleIdSeed++):d3}";

        public string NextSafeModuleId() => $"ui_u_{(generalIdSeed++):d3}";
    }
}