﻿namespace MSharp.Framework.AspNet.Mvc
{
    using System;
    using System.Collections.Generic;

    public abstract class ElementRole
    {
        public virtual bool IsRendered => true;
        public virtual string NewTag => null;
        public virtual string Css => null;
        public virtual string Role => null;

        public virtual void Config(IDictionary<string, string> attributes) { }

        private void ConfigDefaults(IDictionary<string, string> attributes)
        {
            if (!String.IsNullOrEmpty(Css))
                attributes["class"] = Css;

            if (!String.IsNullOrEmpty(Role))
                attributes["role"] = Role;
        }

        /// <summary>
        /// Returns a new Dictionary containing all defined attributes.
        /// </summary>
        public IDictionary<string, string> Attributes()
        {
            var otherAttributes = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            Config(otherAttributes);
            ConfigDefaults(otherAttributes);
            return otherAttributes;
        }

        public class Hidden : ElementRole
        {
            public override string NewTag => null;
            public sealed override string Css => String.Empty;
            public sealed override string Role => String.Empty;
            public sealed override bool IsRendered => false;
            public sealed override void Config(IDictionary<string, string> attributes) { }
        }
    }
}