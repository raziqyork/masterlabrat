﻿namespace Microsoft.AspNet.Http
{
	using System;

	public static class RequestExtensions
	{
		public static Uri Url(this HttpRequest request)
		{
			var url = $"{request.Scheme}://{request.Host}{request.PathBase.ToUriComponent()}{request.Path.ToUriComponent()}{request.QueryString}";
			Uri uri;
			return Uri.TryCreate(url, UriKind.Absolute, out uri) ? uri : null;
		}
	}
}