﻿namespace Microsoft.AspNet.Mvc
{
    using System;
    using System.Collections.Generic;
    using MSharp.Framework.AspNet.Mvc;

    public static class UrlExtensions
    {
        /// <summary>
        /// More than just Uri.TryCreate(), but does the same sort of thing. 
        /// </summary>
        public static Uri ParseUrlOrPath(this IUrlHelper urlHelper, string urlOrPath)
        {
            Uri uri;
            if (Uri.TryCreate(urlOrPath, UriKind.Absolute, out uri) ||
                (urlHelper.IsLocalUrl(urlOrPath) &&
                Uri.TryCreate("http://0.0.0.0" + urlHelper.Content(urlOrPath), UriKind.Absolute, out uri)))
            {
                return uri;
            }

            return null;
        }

        public static string Action(this IUrlHelper urlHelper, string action, string controller, string area, object routeValues)
        {
            if (routeValues is ParamSet)
            {
                // IUrlHelper.Action() will accept IDictionary
                routeValues = ((ParamSet)routeValues).Queries;
            }

            if (!String.IsNullOrEmpty(area))
            {
                if (routeValues == null)
                {
                    routeValues = new { area };
                }
                else
                {
                    var dictionary = routeValues as IDictionary<string, object> ?? routeValues.ToPropertyDictionary();

                    routeValues = new Dictionary<string, object>(dictionary, StringComparer.OrdinalIgnoreCase) {["area"] = area };

                    //routeValues = routeValues.ToDynamic().Extend(new { area }); /* potentially very slow */
                }
            }

            var route = urlHelper.Action(action, controller, routeValues);

            return route;
        }
    }
}