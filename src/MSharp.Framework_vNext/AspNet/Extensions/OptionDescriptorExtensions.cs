﻿namespace Microsoft.AspNet.Mvc
{
    using System;
    using System.Collections.Generic;

    public static class OptionDescriptorExtensions
    {
        public static TService AddType<TService>(this IList<TService> descriptors, Type viewEngineType)
        {
            var descriptor = (TService)Activator.CreateInstance(typeof(TService), viewEngineType);
            descriptors.Add(descriptor);
            return descriptor;
        }

        public static TService AddInstance<TService>(this IList<TService> descriptors, object instance)
        {
            var descriptor = (TService)Activator.CreateInstance(typeof(TService), instance);
            descriptors.Add(descriptor);
            return descriptor;
        }
    }
}