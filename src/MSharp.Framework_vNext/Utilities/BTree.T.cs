﻿namespace MSharp.Collections
{
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// Light-weight Binary Tree to help resolve path hierarchy for Uris.
	/// </summary>
	public class BTree<T> where T : IEquatable<T>
	{
		public Node Root;

		public Node AddPath(IEnumerable<T> path)
		{
			var e = path.GetEnumerator();
			e.MoveNext();

			if (Root == null)
			{
				Root = Node.CreateRoot(e.Current);
			}
			else if (!Root.Value.Equals(e.Current))
			{
				throw new InvalidOperationException("Root node value doesn't match first value in path.");
			}

			var parent = Root;
			while (e.MoveNext())
			{
				parent = parent.LinkChild(e.Current);
			}

			return parent;
		}

		public class Node
		{
			public T Value { get; private set; }
			public Node Parent { get; private set; }
			public Node Left { get; private set; }
			public Node Right { get; private set; }

			internal Node LinkChild(T value)
			{
				if (Left != null)
				{
					if (Left.Value.Equals(value))
					{
						return Left;
					}
					else
					{
						if (Right != null)
						{
							if (Right.Value.Equals(value))
							{
								return Right;
							}
							else
							{
								throw new InvalidOperationException("Both Left and Right nodes already assigned.");
							}
						}
						else
						{
							return Right = new Node { Parent = this, Value = value };
						}
					}
				}
				else
				{
					return Left = new Node { Parent = this, Value = value };
				}
			}

			internal static Node CreateRoot(T value)
			{
				return new Node { Value = value };
			}

			public int? GetDistance(Node node)
			{
				/*
					Todo:
					Dist(n1, n2) = Dist(root, n1) + Dist(root, n2) - 2*Dist(root, lca) 
					'n1' and 'n2' are the two given keys
					'root' is root of given Binary Tree.
					'lca' is lowest common ancestor of n1 and n2
					Dist(n1, n2) is the distance between n1 and n2.
				*/
				throw new NotImplementedException();
			}

			/// <summary>
			/// Returns the distance in generations between this node and the given
			/// node if they're a direct decendant or ancestor. <para />Returns null for all
			/// other relationships.
			/// </summary>
			public int? GetLineage(Node node)
			{
				int i = 0;
				foreach (var n in Parts)
				{
					if (n == node) return i;
					i++;
				}

				i = 0;
				foreach (var n in node.Parts)
				{
					if (n == this) return i;
					i--;
				}

				return null;
			}

			private IEnumerable<Node> Parts
			{
				get
				{
					var node = this;
					while (node != null)
					{
						yield return node;
						node = node.Parent;
					}
				}
			}

			public override string ToString()
			{
				return $"{Value} [{Path}]";
			}

			public string Path => Parent == null ? $"{Value}" : $"{Parent.Path}|{Value}";
		}
	}
}