# README #

This outlines the steps needed to get the M# MVC 6 Master Lab Rat up and running.

### What is this repository for? ###

* This project defines the model M# project which will be build using M# MVC.
* Once ratified, all code, mark-up and patterns from this web application will be used as templates for the next version of M#.

### How do I get set up? ###

* Install [Visual Studio 2015 CTP 6](http://www.visualstudio.com/en-us/downloads/visual-studio-2015-ctp-vs) (or latest) (also here: \\\\RAZIQ\InstallImages\vs2015.ctp_ult_enu.iso).
* [Update nuget repositories](https://github.com/aspnet/Home/wiki/Configuring-the-feed-used-by-kpm-to-restore-packages) to include [aspnetvnext](https://www.myget.org/gallery/aspnetvnext).
* Pull the [dev branch of KVM](https://github.com/aspnet/home) and [upgrade the KRE](https://github.com/aspnet/Home/wiki/version-manager).
* [Fix the locations the KRE has been installed to](https://github.com/aspnet/Home/issues/292#issuecomment-73422144). You need to make a directory junction from /.k to /.kre, then clone runtime dir with "-win" removed from its name; finally copy dir and renamed clone to .k\packages.
* Open the project in VS 2015 and edit project properties for the Website project. On the first tab, you can select kre version. You should be able to see the latest one you installed with kvm.
* Update the self hosting server url in project.json.
* Restore MLR.Data.bak to SQL server and update the connection string in config.json.
* It can be easier to work from the command line, so go to the project directory in PowerShell and run: 
    - kpm restore
    - kpm build
    - k web
* If you cannot build, you may need to rename the following directory: \.k\packages\Microsoft.AspNet.Mvc.Common\6.0.0-beta4-12857\shared (e.g. stick a ! before the name).
* If you get errors restoring packages, then try deleting the project.lock.json files and try again.

### Contribution guidelines ###

* Comments and suggestions are needed. We're improving this all the time, so need feedback from other developers. 

### Who do I talk to? ###

* [Raziq York](mailto:raziq.york@msharp.co), Geeks Ltd. UK

![yay](http://25.media.tumblr.com/tumblr_m6cng8RJlD1rq7gxco1_500.gif).