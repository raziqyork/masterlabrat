﻿namespace Tests
{
    using System;
    using MSharp.Collections;
    using Xunit;

    public class UriDistanceTests
    {
        private Uri url0 = new Uri("http://0/a/b/c");
        private Uri url1 = new Uri("http://0/a/b/c/d/e");
        private Uri url2 = new Uri("http://0/b/f");
        private Uri url3 = new Uri("http://0/c/f");

        [Fact]
        public void BTree_Test_0()
        {
            var tree = new BTree<string>();

            var leaf0 = tree.AddPath(url0.LocalPath.Split('/'));
            var leaf1 = tree.AddPath(url1.LocalPath.Split('/'));
            var leaf2 = tree.AddPath(url2.LocalPath.Split('/'));

            var distanceA = leaf0.GetLineage(leaf1);
            var distanceB = leaf1.GetLineage(leaf0);
            var distanceC = leaf1.GetLineage(leaf1);
            var distanceD = leaf1.GetLineage(leaf2);

            Assert.Equal(-2, distanceA);
            Assert.Equal(2, distanceB);
            Assert.Equal(0, distanceC);
            Assert.Null(distanceD);
        }

        [Fact]
        public void BTree_Test_1()
        {
            var tree = new BTree<string>();

            var leaf0 = tree.AddPath(url0.LocalPath.Split('/'));
            var leaf1 = tree.AddPath(url1.LocalPath.Split('/'));
            var leaf2 = tree.AddPath(url2.LocalPath.Split('/'));

            Assert.Throws<InvalidOperationException>(() =>
                tree.AddPath(url3.LocalPath.Split('/')));
        }

        [Fact]
        public void BTree_Test_2()
        {

        }
    }
}